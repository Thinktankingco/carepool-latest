// Let's register Template7 helper so we can pass json string in links
Template7.registerHelper('json_stringify', function (context) {
    return JSON.stringify(context);
});

// Initialize your app
var myApp = new Framework7({
    animateNavBackIcon: true,
    // Enable templates auto precompilation
    precompileTemplates: true,
    // Enabled pages rendering using Template7
    template7Pages: true,
    // Specify Template7 data for pages

});

// Export selectors engine
var $$ = Dom7;



// Add main View
var mainView = myApp.addView('.view-main', {
    // Enable dynamic Navbar
    dynamicNavbar: true,
});

// Init slider and store its instance in mySwiper variable
var mySwiper = myApp.swiper('.swiper-container', {
    pagination: '.swiper-pagination',
    //enable hash navigation
    hashnav: true
});

var yesturday = new Date().setDate(new Date().getDate() -1);
//alert(weekLater);
var calendarDefault = myApp.calendar({
    input: '#calendar-default',
    dateFormat: 'dd MM yyyy',
    disabled: {
     to: yesturday
    }
});

function close_now() {
    calendarDefault.close()
}

// Pull to refresh content
var ptrContent = $$('.pull-to-refresh-content');
 
// Add 'refresh' listener on it
ptrContent.on('refresh', function (e) {
    // Emulate 2s loading
    setTimeout(function () {
       
       emtpybefore();
        // When loading done, we need to reset it
        myApp.pullToRefreshDone();
    }, 2000);
});

// Messages
// Conversation flag
var conversationStarted = false;
 
// Init Messages
var myMessages = myApp.messages('.messages', {
  autoLayout:true
});
 
// Init Messagebar
var myMessagebar = myApp.messagebar('.messagebar');
 
// Handle message
$$('.messagebar .link').on('click', function () {
  // Message text
//  var messageText = myMessagebar.value().trim();
//  // Exit if empy message
//  if (messageText.length === 0) return;
// 
//  // Empty messagebar
//  myMessagebar.clear()
// 
//  // Random message type
//  var messageType = (['sent', 'received'])[Math.round(Math.random())];
// 
//  // Avatar and name for received message
//  var avatar, name;
//  if(messageType === 'received') {
//    avatar = 'http://lorempixel.com/output/people-q-c-100-100-9.jpg';
//    name = 'Kate';
//  }
//  // Add message
//  myMessages.addMessage({
//    // Message text
//    text: messageText,
//    // Random message type
//    type: messageType,
//    // Avatar and name:
//    avatar: avatar,
//    name: name,
//    // Day
//    day: !conversationStarted ? 'Today' : false,
//    time: !conversationStarted ? (new Date()).getHours() + ':' + (new Date()).getMinutes() : false
//  })
// 
//  // Update conversation flag
  //conversationStarted = true;
});                