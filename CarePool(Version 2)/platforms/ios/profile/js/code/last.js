var counter = 0;
function validateEmail(email) {
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}

function validate(email) {

    if (validateEmail(email)) {
        return true;
    } else {
        return false;
    }
    return false;
}
$(function () {
    Parse.User.logOut();
    $('#signup').on('submit', function () {
        if ($('#full_name').val() === '') {
            config.Msg('Please Provide Full Name in order to proceed', 'Hmm, something’s wrong', 'OK');
            return false;
        } else {
            myApp.showIndicator();

            var user = new Parse.User();
            user.set("username", localStorage.getItem('mobilenumber'));
            user.set("country_code", localStorage.getItem('country_code'));
            if (validate($('#email').val())) {
                user.set("email", $('#email').val());
            }
            user.set("password", '789');
            user.set("full_name", $('#full_name').val());
            user.set("player_id", localStorage.getItem('player_id'));
            user.set("push_notes", 'Yes');
            user.set("type", 'Client');

            //return false;
            user.signUp(null, {
                success: function (user) {
                    if (validate($('#email').val())) {

                        $.ajax({
                            type: "post",
                            url: 'http://www.carepool.co/carepool_emails/welcome.php',
                            data: {email: $('#email').val(), name: $('#full_name').val()},
                            crossDomain: true,
                            dataType: "json",
                            cache: false,
                            success: function (data) {
                                // console.log();
                                if (data == 'sent') {
                                    sum();
                                } else {
                                    myApp.hideIndicator();
                                    config.Msg("That didn’t really go to plan. We’re not sure why. Please reload CarePool.", 'Hmm, something’s wrong', 'OK');
                                }
                            },
                            error: function () {
                                //console.log();
                                myApp.hideIndicator();
                                config.Msg("That didn’t really go to plan. We’re not sure why. Please reload CarePool.", 'Hmm, something’s wrong', 'OK');
                            }

                        });
                    } else {
                        sum();
                    }


                    myApp.showIndicator();
                    //change name in user friends 
                    var Life = Parse.Object.extend("user_friends");
                    var query = new Parse.Query(Life);
                    //updaintg user friends table
                    query.equalTo("email", localStorage.getItem('mobilenumber'));
                    //query.equalTo("email", 'ahsan.dev.drc@gmail.com');
                    query.find({
                        success: function (results) {
                            myApp.showIndicator();
                            //console.log(results);
                            if (results.length) {
                                var lifeArray = [];
                                for (var i = 0; i < results.length; i++) {
                                    var update_name = new Life();
                                    var object = results[i];
                                    update_name.set("objectId", object.id);
                                    update_name.set("full_name", $('#full_name').val());
                                    lifeArray.push(update_name);
                                }
                                Parse.Object.saveAll(lifeArray, {
                                    success: function (objs) {
                                        // objects have been saved...
                                        // config.Msg('success');
                                        //alert('changed all the names');
                                        //myApp.hideIndicator();
                                        sum();
                                    },
                                    error: function (error) {
                                        // an error occurred...

                                        config.Msg("That didn’t really go to plan. We’re not sure why. Please reload CarePool.", 'Hmm, something’s wrong', 'OK');
                                        myApp.hideIndicator();
                                        return false;
                                    }
                                });
                            } else {
                                sum();
                            }
                            // results is an array of AgentReleases
                            myApp.hideIndicator();
                        },
                        error: function (error) {
                            alert("Error: " + error.code + " " + error.message);
                            myApp.hideIndicator();
                        }
                    });
                    //updating user_offers providers name first
                    myApp.showIndicator();
                    var Provider = Parse.Object.extend("user_offers");
                    var query = new Parse.Query(Provider);
                    query.equalTo("care_provider", localStorage.getItem('mobilenumber'));
                    query.find({
                        success: function (results) {
                            myApp.showIndicator();
                            //console.log(results);
                            if (results.length) {
                                var lifeArray = [];
                                for (var i = 0; i < results.length; i++) {
                                    var update_name = new Provider();
                                    var object = results[i];
                                    update_name.set("objectId", object.id);
                                    update_name.set("care_provider_name", $('#full_name').val());
                                    lifeArray.push(update_name);
                                }
                                Parse.Object.saveAll(lifeArray, {
                                    success: function (objs) {
                                        // objects have been saved...
                                        // config.Msg('success');
                                        //alert('changed all the receivers');
                                        sum();
                                        //myApp.hideIndicator();
                                    },
                                    error: function (error) {
                                        // an error occurred...
                                        //config.Msg(error);
                                        config.Msg("That didn’t really go to plan. We’re not sure why. Please reload CarePool.", 'Hmm, something’s wrong', 'OK');
                                        myApp.hideIndicator();
                                        return false;
                                    }
                                });
                                //console.log(results);
                            } else {
                                sum();
                            }
                            // results is an array of AgentReleases
                            myApp.hideIndicator();
                        },
                        error: function (error) {
                            alert("Error: " + error.code + " " + error.message);
                        }
                    });
                    myApp.showIndicator();
                    //updating user_offers receivers name first
                    query.equalTo("care_receiver", localStorage.getItem('mobilenumber'));
                    query.find({
                        success: function (results) {
                            //console.log(results);
                            myApp.showIndicator();
                            if (results.length) {
                                var lifeArray = [];
                                for (var i = 0; i < results.length; i++) {
                                    var update_name = new Provider();
                                    var object = results[i];
                                    update_name.set("objectId", object.id);
                                    update_name.set("care_receiver_name", $('#full_name').val());
                                    lifeArray.push(update_name);
                                }
                                Parse.Object.saveAll(lifeArray, {
                                    success: function (objs) {
                                        // objects have been saved...
                                        // config.Msg('success');
                                        //alert('changed all the providers');
                                        sum();
                                        //myApp.hideIndicator();
                                    },
                                    error: function (error) {
                                        // an error occurred...

                                        config.Msg("That didn’t really go to plan. We’re not sure why. Please reload CarePool.", 'Hmm, something’s wrong', 'OK');
                                        myApp.hideIndicator();
                                        return false;
                                    }
                                });
                                // console.log(results);
                            } else {
                                sum();
                            }
                            // results is an array of AgentReleases
                            myApp.hideIndicator();
                        },
                        error: function (error) {
                            alert("Error: " + error.code + " " + error.message);
                            myApp.hideIndicator();
                        }
                    });
                },
                error: function (user, error) {
                    //alert('190-'+error.code);
                    var code_array = [203, 202];
                    //alert(code_array.indexOf(error.code));
                    if (!code_array.includes(error.code)) {
                        //alert(192);
                        // myApp.hideIndicator();
                        // Show the error message somewhere and let the user try again.
                        myApp.hideIndicator();
                        config.Msg("Please Fill all the required fields.", 'Hmm, something’s wrong', 'OK');
                    } else {
                        //alert(194);
                        myApp.hideIndicator();
//                        config.Msg("Ooops, that number is already connected to a CarePool account. \n\
//Recover your password on the Sign In screen or use another email address", 'CarePool', 'OK');
                        //do signin code starts here

                        var musername = localStorage.getItem('mobilenumber');
                        var mpassword = '789';

                        myApp.showIndicator();

                        Parse.User.logIn(musername, mpassword, {
                            // If the username and password matches
                            success: function (user) {
                                //config.Msg('Welcome! ' + Parse.User.current().get('username'));

                                if (Parse.User.current().get('type') !== 'Admin') {

                                    //update player ID code starts here

                                    var TestObject = Parse.Object.extend("User");
                                    var testObject = new TestObject();
                                    var uid = Parse.User.current().id;

                                    testObject.set("objectId", uid);

                                    testObject.set("player_id", localStorage.getItem('player_id'));
                                    if (validate($('#email').val())) {
                                        testObject.set("email", $('#email').val());
                                    }
                                    testObject.save(null, {
                                        success: function (object) {

                                            myApp.hideIndicator();
                                            window.location = 'dashboard.html';

                                        },
                                        error: function (model, error)
                                        {

                                            // config.Msg('error');
                                            config.Msg("That didn’t really go to plan. We’re not sure why. Please reload CarePool.", 'Hmm, something’s wrong', 'OK');
                                            myApp.hideIndicator();
                                        }
                                    });


                                    //update player ID code ends here


                                } else {
                                    config.Msg('Only Admin can login to this section', 'Hmm, something’s wrong', 'OK');
                                    window.location = 'signup.html';
                                }

                            },
                            // If there is an error
                            error: function (user, error) {
                                //alert(error.code);
                                myApp.hideIndicator();
                                if (error.code !== 101) {
                                    // Show the error message somewhere and let the user try again.
                                    config.Msg("That didn’t really go to plan. We’re not sure why. Please reload CarePool.", 'Hmm, something’s wrong', 'OK');
                                } else {
                                    //config.Msg("Oops, that was the wrong email/password combination");
                                    config.Msg("Ooops, that was the wrong email / password combination!", 'Try another', 'OK');

                                }
                            }
                        });
                        return false;

                        //do signin code ends here

                    }

                }
            });
            return false;
        }
    });
    //api_num();
});
//function api_num() {
//    alert(1);
//    
//    var url = "http://api.clickatell.com/http/sendmsg?user=bilalkirmani&password=IGFEbcPfePSEYC&api_id=3620448&to=923028005003&text=Message";
//    
//    $.post(url, function(output){
//        alert(output);
//    });
//}

function sum() {
    ++counter;
    //alert(counter);
    if (counter === 4) {
        myApp.hideIndicator();
        window.location = 'dashboard.html';
    }
}