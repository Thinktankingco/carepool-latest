$(function () {

    // get page height
    ttlheight = $(window).height() - 180;
    $('.page-content').css('height', ttlheight);

    //get offers categories
    myApp.showIndicator();
    var off_cat = Parse.Object.extend("offer_Category");
    var off = new Parse.Query(off_cat);
    off.equalTo("category", 'help');
    off.find({
        success: function (results) {
            for (var i = 0; i < results.length; i++) {
                myApp.showIndicator();
                var object = results[i];


                $('#offers').append('<li class="bg_orange"><div class="item-content"><a onClick="makesession(' +"'"+ object.id +"'"+ ')"><div class="item-media">\n\
<img src="' + object.get('simple_image').url() + '" width="60" height="60"></div></a><div class="item-inner"><a style="margin-left:10px" onClick="makesession(' +"'"+ object.id +"'"+ ')"><div class=""><strong>\n\
<font class="default_font" id="text_new">' + object.get('title') + '</font><br> <font class="default_sub_font" id="text_new_sb">' + object.get('start_time') + '-' + object.get('end_time') + '</font></strong> </div></a><div class="item-after"  id="offer_set">\n\
<img alt="' + object.get('info') + '" id="o' + i + '" onclick="get_hello(' + i + ')" data-popover=".popover-about" style="margin-top:-5px;" width="30" height="30" src="img/icons/info.png" class="link open-popover hand" ></div></div></div><div class="sortable-handler"></div></li>');
            myApp.hideIndicator();
            }
            myApp.hideIndicator();
        },
        error: function (error) {
            config.Msg("That didn’t really go to plan. We’re not sure why. Please reload CarePool.",'Hmm, something’s wrong','OK');
            myApp.hideIndicator();
        }
    });

});
function makesession(object){
   
    localStorage.setItem('creation_start',new Date());
    window.location.href = 'create.html?id=' + object+ '&cat=ask';
   
}
function get_hello(id) {
    var n = 'o' + id;
    var m = $('#' + n).attr('alt');
    $('#message').html(m);
    $('#message').toggle('fast');
}