function getUrlVars() {
    var vars = {};
    var parts = window.location.href.replace(
            /[?&]+([^=&]+)=([^&]*)/gi, function (m, key, value) {
                vars[key] = value;
            });
    return vars;
}
$(function () {

    ttlheight = $(window).height() - 250;
    myApp.showIndicator();
    
    $('.page-content').css('height', ttlheight);
    $('.page-content').css('background-color', 'white');

    myApp.hideIndicator();

});


 var vote;
$('#img_hover1').click(function () {
    $('#check_box1').trigger('click');
    vote = $('#check_box1').val();
    $('#img_hover1').css({
            'box-shadow' : '4px 4px #ccc',
            'cursor' : 'pointer',
            'border-radius' : '50px'
    });
    $('#img_hover2').css({
            'box-shadow' : '0px 0px #ccc',
            'cursor' : 'pointer',
            'border-radius' : '50px'
    });
    $('#img_hover3').css({
            'box-shadow' : '0px 0px #ccc',
            'cursor' : 'pointer',
            'border-radius' : '0px'
    });
    
});

$('#img_hover2').click(function () {
    $('#check_box1').trigger('click');
   vote = $('#check_box2').val();
    $('#img_hover2').css({
            'box-shadow' : '4px 4px #ccc',
            'cursor' : 'pointer',
            'border-radius' : '50px'
    });
    $('#img_hover1').css({
            'box-shadow' : '0px 0px #ccc',
            'cursor' : 'pointer',
            'border-radius' : '50px'
    });
    $('#img_hover3').css({
            'box-shadow' : '0px 0px #ccc',
            'cursor' : 'pointer',
            'border-radius' : '0px'
    });
});

$('#img_hover3').click(function () {
    $('#check_box1').trigger('click');
    vote = $('#check_box3').val();
    $('#img_hover3').css({
            'box-shadow' : '4px 4px #ccc',
            'cursor' : 'pointer',
            'border-radius' : '50px'
    });
    $('#img_hover1').css({
            'box-shadow' : '0px 0px #ccc',
            'cursor' : 'pointer',
            'border-radius' : '50px'
    });
    $('#img_hover2').css({
            'box-shadow' : '0px 0px #ccc',
            'cursor' : 'pointer',
            'border-radius' : '0px'
    });
});

$('#click_me').click(
        function () {
            $(this).closest('#send_message').trigger('submit');
            
        });

$("#send_message").on('submit', function() {
    
    
    var email = Parse.User.current().get('email');
    var fullname = Parse.User.current().get('full_name');
    
    var comment = $("#comment").val();
    
    if(vote===''){
        //config.Msg('Select emotion');
        config.Msg("Select Emotion.",'Hmm, something’s wrong','OK');
        return false
    }
    
    if(comment===''){
        //config.Msg('Type comment');
        config.Msg("Type Comment",'Hmm, something’s wrong','OK');
        return false;
    }
    myApp.showIndicator();
    var msg = 'Feeling: ' +vote+ '\n Actual Message: ' +comment;
   // config.Msg(email);config.Msg(fullname);config.Msg(msg);return false;
    $.ajax({
            type: "post",
            url: 'http://www.carepool.co/carepool_emails/support.php',
            data: {email:email, name:fullname, message:msg},
            crossDomain: true,
            dataType: "json",
            cache: false,
            success: function (data) {
              // console.log();
                if(data=='sent'){
                   myApp.hideIndicator();
                   
                    config.Msg("Email sent to the Administrator",'Hurrah','OK');
                    window.location.href = 'more.html';
                }else{
                    myApp.hideIndicator();
                    //config.Msg("An eror occured while sending email");
                    config.Msg("That didn’t really go to plan. We’re not sure why. Please reload CarePool.",'Hmm, something’s wrong','OK');
                }
            },
            error: function () {
                //console.log();
                config.Msg("That didn’t really go to plan. We’re not sure why. Please reload CarePool.",'Hmm, something’s wrong','OK');
            }

        });
        
        return false;
    
});