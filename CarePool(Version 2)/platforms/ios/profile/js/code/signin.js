
$(function () {

    ttlheight = $(window).height() - 165;
    $('.page-content').css('height', ttlheight);

    Parse.User.logOut();
    //login form
    $('#login_form').submit(function () {

        var musername = $('#musername').val();
        var mpassword = $('#mpassword').val();

        myApp.showIndicator();

        Parse.User.logIn(musername, mpassword, {
            // If the username and password matches
            success: function (user) {
                //config.Msg('Welcome! ' + Parse.User.current().get('username'));



                if (Parse.User.current().get('type') !== 'Admin') {

                    //update player ID code starts here

                    var TestObject = Parse.Object.extend("User");
                    var testObject = new TestObject();
                    var uid = Parse.User.current().id;

                    testObject.set("objectId", uid);

                    testObject.set("player_id", localStorage.getItem('player_id'));

                    testObject.save(null, {
                        success: function (object) {

                            myApp.hideIndicator();
                            window.location = 'dashboard.html';

                        },
                        error: function (model, error)
                        {

                            // config.Msg('error');
                            config.Msg("That didn’t really go to plan. We’re not sure why. Please reload CarePool.", 'Hmm, something’s wrong', 'OK');
                            myApp.hideIndicator();
                        }
                    });


                    //update player ID code ends here


                } else {
                    config.Msg('Only Admin can login to this section', 'Hmm, something’s wrong', 'OK');
                    window.location = 'signin.html';
                }

            },
            // If there is an error
            error: function (user, error) {
                //alert(error.code);
                myApp.hideIndicator();
                if (error.code !== 101) {
                    // Show the error message somewhere and let the user try again.
                    config.Msg("That didn’t really go to plan. We’re not sure why. Please reload CarePool.", 'Hmm, something’s wrong', 'OK');
                } else {
                    //config.Msg("Oops, that was the wrong email/password combination");
                    config.Msg("Ooops, that was the wrong email / password combination!", 'Try another', 'OK');

                }
            }
        });
        return false;
    });

});

function login() {
    myApp.showIndicator();
    openFB.init({appId: '1035425336490081'});
    openFB.login(
            function (response) {
                if (response.status === 'connected') {
//alert(37);return false;
                    //alert('Facebook login succeeded, got access token: ' + response.authResponse.accessToken);
                    myApp.showIndicator();
                    $.ajax({
                        type: "get",
                        url: 'https://graph.facebook.com/me?access_token=' + response.authResponse.accessToken + '&fields=email',
                        //data: {email:email, name:fullname, message:msg},
                        crossDomain: true,
                        dataType: "json",
                        cache: false,
                        success: function (data) {
                            localStorage.setItem('mytoken', response.authResponse.accessToken);
                            Parse.User.logIn(data.email, '789', {
                                // If the username and password matches
                                success: function (user) {
                                    //config.Msg('Welcome! ' + Parse.User.current().get('username'));

                                    if (Parse.User.current().get('type') !== 'Admin') {


                                        //update player ID code starts here

                                        var TestObject = Parse.Object.extend("User");
                                        var testObject = new TestObject();
                                        var uid = Parse.User.current().id;

                                        testObject.set("objectId", uid);

                                        testObject.set("player_id", localStorage.getItem('player_id'));

                                        testObject.save(null, {
                                            success: function (object) {

                                                myApp.hideIndicator();
                                                window.location = 'dashboard.html';

                                            },
                                            error: function (model, error)
                                            {

                                                // config.Msg('error');
                                                config.Msg("That didn’t really go to plan. We’re not sure why. Please reload CarePool.", 'Hmm, something’s wrong', 'OK');
                                                myApp.hideIndicator();
                                            }
                                        });


                                        //update player ID code ends here
                                    } else {
                                        alert('Only Admin can login to this section');
                                        window.location = 'signin.html';
                                    }
                                },
                                // If there is an error
                                error: function (user, error) {
                                    myApp.hideIndicator();
                                    //config.Msg(error.message);
                                    config.Msg("That didn’t really go to plan. We’re not sure why. Please reload CarePool.", 'Hmm, something’s wrong', 'OK');
                                }
                            });

                            myApp.hideIndicator();
                        },
                        error: function () {
                            //console.log();
                            config.Msg("That didn’t really go to plan. We’re not sure why. Please reload CarePool.", 'Hmm, something’s wrong', 'OK');
                            myApp.hideIndicator();
                        }

                    });

                } else {

                    config.Msg("That didn’t really go to plan. We’re not sure why. Please reload CarePool.", 'Hmm, something’s wrong', 'OK');
                    myApp.hideIndicator();
                }
                myApp.hideIndicator();
            }, {scope: 'email,public_profile,user_friends'});
}