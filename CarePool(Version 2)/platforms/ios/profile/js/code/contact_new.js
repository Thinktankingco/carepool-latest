$(function () {
    ttlheight = $(window).height() - 180;
    myApp.showIndicator();
    $('.page-content').css('height', ttlheight);
    
   var Life = Parse.Object.extend("user_friends");
    var mylife = new Parse.Query(Life);
    mylife.equalTo("user_id", Parse.User.current().id);
    mylife.find({
        success: function (results) {
            if(results.length){
            for (var i = 0; i < results.length; i++) {
                var object = results[i];
                //myfriends.push(object.get('full_name'));
                
$('#contactlist').append('<li class="swipeout"><div class="item-content"><div class="item-media">\n\
<img src="img/profile.png" width="30" height="30"></div><div class="item-inner"><div class="item-title">'+object.get('full_name')+'</div></div>\n\
<div class="item-media"><img src="img/icons/icon7.jpg" width="20" height="18"></div></div><div class="swipeout-actions-right">\n\
<a href="#" class="swipeout-delete" data-confirm="Are you sure want to delete this contact?" data-confirm-title="Delete?" data-close-on-cancel="false">Delete</a></div></li>');
                
            }
        }else{
            $('#contactlist').append('<li><div class="item-content"><div class="item-media">\n\
<div class="item-media"><div class="item-inner"><div class="item-title">You dont have any contacts yet</div></div></div></div></li>');
        }
           
            myApp.hideIndicator();
          
        },
        error: function (error) {
            myApp.hideIndicator();
            config.Msg("That didn’t really go to plan. We’re not sure why. Please reload CarePool.",'Hmm, something’s wrong','OK');
        }
    })


});