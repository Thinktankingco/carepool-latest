var counter = 0;
$(function () {
    $('#signup').on('submit', function () {
        if ($('#number').val() === '') {
            config.Msg('Please provide Access Code in order to be proceeded.', 'Hmm, something’s wrong', 'OK');
            return false;
        } else {
            if ($('#number').val() === localStorage.getItem('comparetonextpage')) {
                //config.Msg('valid', 'Wow', 'OK');

                window.location = 'last.html';
                return false;
            } else {
                //alert(localStorage.getItem('comparetonextpage'));
                config.Msg('Invalid Access Code provided, please provide the valid access code in order to be proceeded.', 'Hmm, something’s wrong', 'OK');
                return false;
            }
        }
        return false;
    });
});