function getUrlVars() {
    var vars = {};
    var parts = window.location.href.replace(
            /[?&]+([^=&]+)=([^&]*)/gi, function (m, key, value) {
                vars[key] = value;
            });
    return vars;
}
$(function () {
    ttlheight = $(window).height() - 180;
    myApp.showIndicator();
    $('.page-content').css('height', ttlheight);
    $('#auth_user').html(Parse.User.current().get('full_name'));
    $('#email_user').html(Parse.User.current().get('username'));

    if (Parse.User.current().get('image')) {

        var pic = Parse.User.current().get('image').url();
        $("#prof_pic").attr('src', pic);

    } else

    if (Parse.User.current().get('facebook_image')) {
        var pic = Parse.User.current().get('facebook_image');
        $("#prof_pic").attr('src', pic);
    } else {
        $("#prof_pic").attr('src', 'img/generic-profile.png');

    }
    myApp.hideIndicator();
    var k = 0;
    $('#changename').on('click', function () {
//        alert(31);
        if (k === 0) {
            $("#changename").attr('src', 'img/icons/Close-icon.png');
            $('#myfield').show();
            k = 1;
        } else {
            $("#changename").attr('src', 'img/icons/edit.png');
            $('#myfield').hide();
            k = 0;
        }

    });
    $('#myfield').on('submit', function () {
        if ($('#full_name').val().trim()) {
            if ($('#full_name').val().trim() === Parse.User.current().get('full_name')) {

                config.Msg("New name should be different then that exist one", 'Hmm, something’s wrong', 'OK');
            } else {
                //change name in users table
                myApp.showIndicator();
                var TestObject = Parse.Object.extend("User");
                var testObject = new TestObject();
                var uid = Parse.User.current().id;

                testObject.set("objectId", uid);

                testObject.set("full_name", $('#full_name').val());

                testObject.save(null, {
                    success: function (object) {

                        $('#auth_user').html($('#full_name').val());
                        var newname = $('#full_name').val();
                        $('#full_name').val('');
                        $("#changename").attr('src', 'img/icons/edit.png');
                        $('#myfield').hide();
                        k = 0;
                        myApp.showIndicator();
                        //change name in user friends 
                        var Life = Parse.Object.extend("user_friends");
                        var query = new Parse.Query(Life);

                        //updaintg user friends table
                        query.equalTo("email", Parse.User.current().get('username'));
                        //query.equalTo("email", 'ahsan.dev.drc@gmail.com');
                        query.find({
                            success: function (results) {
                                myApp.showIndicator();
                                //console.log(results);
                                if (results.length) {
                                    var lifeArray = [];
                                    for (var i = 0; i < results.length; i++) {
                                        var update_name = new Life();
                                        var object = results[i];
                                        update_name.set("objectId", object.id);
                                        update_name.set("full_name", newname);
                                        lifeArray.push(update_name);
                                    }
                                    Parse.Object.saveAll(lifeArray, {
                                        success: function (objs) {
                                            // objects have been saved...
                                            // config.Msg('success');
                                            //alert('changed all the names');
                                            myApp.hideIndicator();
                                        },
                                        error: function (error) {
                                            // an error occurred...
                                            //config.Msg(error);
                                            config.Msg("That didn’t really go to plan. We’re not sure why. Please reload CarePool.", 'Hmm, something’s wrong', 'OK');
                                            myApp.hideIndicator();
                                            return false;
                                        }
                                    });

                                }
                                // results is an array of AgentReleases
                                myApp.hideIndicator();
                            },
                            error: function (error) {
                                alert("Error: " + error.code + " " + error.message);
                                myApp.hideIndicator();
                            }
                        });
                        //updating user_offers providers name first
                        myApp.showIndicator();
                        var Provider = Parse.Object.extend("user_offers");
                        var query = new Parse.Query(Provider);
                        query.equalTo("care_provider", Parse.User.current().get('username'));
                        query.find({
                            success: function (results) {
                                myApp.showIndicator();
                                //console.log(results);
                                if (results.length) {
                                    var lifeArray = [];
                                    for (var i = 0; i < results.length; i++) {
                                        var update_name = new Provider();
                                        var object = results[i];
                                        update_name.set("objectId", object.id);
                                        update_name.set("care_provider_name", newname);
                                        lifeArray.push(update_name);
                                    }
                                    Parse.Object.saveAll(lifeArray, {
                                        success: function (objs) {
                                            // objects have been saved...
                                            // config.Msg('success');
                                            //alert('changed all the providers');
                                            myApp.hideIndicator();
                                        },
                                        error: function (error) {
                                            // an error occurred...
                                            //config.Msg(error);
                                            config.Msg("That didn’t really go to plan. We’re not sure why. Please reload CarePool.", 'Hmm, something’s wrong', 'OK');
                                            myApp.hideIndicator();
                                            return false;
                                        }
                                    });
                                    //console.log(results);
                                }
                                // results is an array of AgentReleases
                                myApp.hideIndicator();
                            },
                            error: function (error) {
                                alert("Error: " + error.code + " " + error.message);
                            }
                        });
                        myApp.showIndicator();
                        //updating user_offers receivers name first
                        var query2 = new Parse.Query(Provider);
                        query2.equalTo("care_receiver", Parse.User.current().get('username'));
                        query2.find({
                            success: function (results) {
                                //console.log(results);
                                myApp.showIndicator();
                                if (results.length) {
                                    var lifeArray = [];
                                    for (var i = 0; i < results.length; i++) {
                                        var update_name = new Provider();
                                        var object = results[i];
                                        update_name.set("objectId", object.id);
                                        update_name.set("care_receiver_name", newname);
                                        lifeArray.push(update_name);
                                    }
                                    Parse.Object.saveAll(lifeArray, {
                                        success: function (objs) {
                                            // objects have been saved...
                                            // config.Msg('success');
                                            //alert('changed all the providers');
                                            myApp.hideIndicator();
                                        },
                                        error: function (error) {
                                            // an error occurred...
                                            //config.Msg(error);
                                            config.Msg("That didn’t really go to plan. We’re not sure why. Please reload CarePool.", 'Hmm, something’s wrong', 'OK');
                                            myApp.hideIndicator();
                                            return false;
                                        }
                                    });
                                    // console.log(results);
                                }
                                // results is an array of AgentReleases
                                myApp.hideIndicator();
                            },
                            error: function (error) {
                                alert("Error: " + error.code + " " + error.message);
                                myApp.hideIndicator();
                            }
                        });


                    },
                    error: function (model, error)
                    {

                        // config.Msg('error');
                        config.Msg("That didn’t really go to plan. We’re not sure why. Please reload CarePool.", 'Hmm, something’s wrong', 'OK');
                        myApp.hideIndicator();
                    }
                });
            }
            return false;
        } else {

            config.Msg("Enter your name", 'Hmm, something’s wrong', 'OK');
            return false;
        }
    });



    $('#btn_pp').click(function () {
        
        navigator.camera.getPicture(gotPic, failHandler,
                {quality: 50, destinationType: navigator.camera.DestinationType.DATA_URL,
                    sourceType: navigator.camera.PictureSourceType.PHOTOLIBRARY});

    });

    function gotPic(data) {
//        alert(229);
       
        var parseFile = new Parse.File("mypic.jpg", {base64:data});
        myApp.showIndicator();
        //saving
        parseFile.save().then(function () {
            // The file has been saved to Parse..

            //alert(237);
            var TestObject = Parse.Object.extend("User");
            var testObject = new TestObject();
            //config.Msg('Image Uploaded Successfully');


            var uid = Parse.User.current().id;

            testObject.set("objectId", uid);

            testObject.set("image", parseFile);


            testObject.save(null, {
                success: function (object) {

                    //config.Msg('Done');
                    myApp.hideIndicator();
                    window.location = 'profile.html';
                    localStorage.setItem('token', object.sessionToken);

                },
                error: function (model, error)
                {
                    //$(".error").show();
                    //config.Msg('error');
                    config.Msg("That didn’t really go to plan. We’re not sure why. Please reload CarePool.", 'Hmm, something’s wrong', 'OK');
                    myApp.hideIndicator();
                }
            });

        }, function (error) {
            alert(JSON.stringify(error));
            // The file either could not be read, or could not be saved to Parse.
            //config.Msg('unable to save file');
            config.Msg("That didn’t really go to plan. We’re not sure why. Please reload CarePool.", 'Hmm, something’s wrong', 'OK');
            myApp.hideIndicator();
        });
    }

    function failHandler(e) {
//        alert("ErrorFromC");
//        alert(e);
        //console.log(e.toString());
    }
});