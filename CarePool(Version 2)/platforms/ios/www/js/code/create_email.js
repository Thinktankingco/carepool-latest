
var myfriends = new Array();
var Life = Parse.Object.extend("user_offers");
var MLife = Parse.Object.extend("user_friends");
var arr_r = new Array(); // {} will create an object
var player_ids = {};
var send_players = [];
function getUrlVars() {
    var vars = {};
    var parts = window.location.href.replace(
            /[?&]+([^=&]+)=([^&]*)/gi, function (m, key, value) {
                vars[key] = value;
            });
    return vars;
}

$(function () {

//config.Msg(getUrlVars()["cat"]);
    if (getUrlVars()["cat"] == 'ask') {
        var bodyid = document.getElementById("change_me");
        bodyid.setAttribute("class", "body_orange");
    }

});

function send_accomplishments(url) {


    $.ajax({
        type: "post",
        url: url,
        data: {email: Parse.User.current().get('email'), name: Parse.User.current().get('full_name')},
        crossDomain: true,
        dataType: "json",
        cache: false,
        success: function (data) {
            backto_dashboard();
        },
        error: function (error) {
            //console.log();
            //alert(JSON.stringify(data));
            config.Msg("That didn’t really go to plan. We’re not sure why. Please reload CarePool.", 'Hmm, something’s wrong', 'OK');
        }
    });

}

var email_array = []; // {} will create an object
var email_name = [];
function addrecepients(id) {

//config.Msg($('#'+ id).attr('myemail'));
    showind();
    var email = $('#' + id).attr('myemail');
    var name = $('#' + id).attr('myname');
    var index = email_array.indexOf(email);
    //config.Msg(index);
    if (index > -1) {
        email_array.splice(index, 1);
    }
    var name_index = email_name.indexOf(' ' + name);
    //config.Msg(index);
    if (name_index > -1) {
        email_name.splice(name_index, 1);
    }
    showind();
    if ($('#' + id).is(':checked')) {
        // Do stuff
        email_array.push(email);
        email_name.push(' ' + name);
    }

    myApp.hideIndicator();
}
function displaymyfriends() {
    //getting myfriends
    showind();
    var Life = Parse.Object.extend("user_friends");
    var mylife = new Parse.Query(Life);
    mylife.equalTo("user_id", Parse.User.current().id);
    mylife.ascending("full_name");
    mylife.find({
        success: function (results) {
            $('#contactlist').empty();
            if (results.length) {
                var j = 1;
                for (var i = 0; i < results.length; i++) {
                    var object = results[i];
                    myfriends.push(object.get('email'));

                    $('#contactlist').append('<li><div class="item-content"><div class="item-inner"><div class="item-title">' + object.get('full_name') + '</div>\n\
<label class="item-content col-20"><input  myemail="' + object.get('email') + '" myname="' + object.get('full_name') + '" type="checkbox"  id="' + j + '" onChange="addrecepients(' + j + ')"><div class="item-media">\n\
<i class="icon icon-form-checkbox"></i></div></div></div></li>');
                    j++;
                }
                $('#right-buttons').show();
            } else {
                $('#contactlist').append('<li><div class="item-content"><div class="">\n\
<div class="item-media"><div class=""><div class="default_font" style="color:black;">Add people to your CarePool to be able to \n\
send them offers or requests. To add new contacts, click on Add More below.</div></div></div></div></li>');
                $('#right-buttons').hide();
            }
            //config.Msg(90);
            myApp.hideIndicator();
            device_contacts();
        },
        error: function (error) {
            myApp.hideIndicator();
            config.Msg("That didn’t really go to plan. We’re not sure why. Please reload CarePool.", 'Hmm, something’s wrong', 'OK');
        }
    });
}
$(function () {
    //submit users
    $('.submit-recepients').on('click', function () {
        if (email_array.length > 0) {
            // config.Msg(email_array);
            //$('.panel').hide('fast');
            myApp.closePanel();
            //alert(email_name.toString());
            $('#recipient').val('');
            $('#recipient').empty();
            $('#recipient').val(email_name.toString());
        } else {
            config.Msg('Add Contacts using the checkboxes on the right. If you don’t want to add any right now, just hit Close', 'Adding Contacts', 'GOT IT');
        }
        //
    });
    //back


    displaymyfriends();
    myApp.hideIndicator();


    ttlheight = $(window).height() - 250;
    $('.page-content').css('height', ttlheight);

    ttlheight = $(window).height() - 200;
    $('#shortmylength').css('height', ttlheight);

    ttlheight = $(window).height() - 195;
    $('#mycarepool').css('height', ttlheight);
    //$('.page-content2').css('max-height', ttlheight);

    $('.page-content').css('background-color', 'white');


    // check for null id
    if (getUrlVars()["id"] == null && getUrlVars()["o_id"] == null) {
        window.location.href = 'offer.html';
    }

    if (getUrlVars()["cat"] == 'offer') {
        var link = document.getElementById("back_link");
        link.setAttribute("href", "offer.html");
    } else if (getUrlVars()["cat"] == 'ask') {
        var link = document.getElementById("back_link");
        link.setAttribute("href", "askforhelp.html");
    }

    //get offers categories
    if (getUrlVars()["id"]) {
        get_related_offer(getUrlVars()["id"]);
    } else if (getUrlVars()["o_id"]) {
        showind();
        var off_cat = Parse.Object.extend("offers");
        var off = new Parse.Query(off_cat);
        off.equalTo("objectId", getUrlVars()["o_id"]);
        off.find({
            success: function (results) {
                console.log(results);
                Date.prototype.toLocaleFormat = Date.prototype.toLocaleFormat || function (pattern) {
                    var year = this.getFullYear(), month = this.getMonth() + 1, day = this.getDate();
                    if (month < 10)
                        month = '0' + month;
                    if (day < 10)
                        day = '0' + day;
                    return pattern.replace(/%Y/g, year).replace(/%m/g, month).replace(/%d/g, day);
                };
                for (var i = 0; i < results.length; i++) {
                    var object = results[i];
                    $('#objectId').val(object.id);
                    get_related_offer(object.get('offer_Category_id')); // target parent class
                    getrecepients(object.id);

                    var today = new Date(object.get('date'));

                    //var mydate = today.toLocaleFormat('%Y-%m-%d');
                    var mydate = today.toLocaleFormat('%d %m %Y');
                    mydate = mydate.split(' ');
                    switch (mydate[1]) {
                        case '01':
                            cmonth = 'January';
                            break;
                        case '02':
                            cmonth = 'February';
                            break;
                        case '03':
                            cmonth = 'March';
                            break;
                        case '04':
                            cmonth = 'April';
                            break;
                        case '05':
                            cmonth = 'May';
                            break;
                        case '06':
                            cmonth = 'June';
                            break;
                        case '07':
                            cmonth = 'July';
                            break;
                        case '08':
                            cmonth = 'August';
                            break;
                        case '09':
                            cmonth = 'September';
                            break;
                        case '10':
                            cmonth = 'October';
                            break;
                        case '11':
                            cmonth = 'November';
                            break;
                        case '12':
                            cmonth = 'December';
                            break;
                    }
                    //alert(mydate);
                    mydate = mydate[0] + ' ' + cmonth + ' ' + mydate[2];
                    $('#calendar-default').val(mydate);

                    // Bilal Code
                    var t1 = object.get('start_time');
                    $('#time_picker1').val(t1);

                    $('#time_picker2').val(object.get('end_time'));
                    $('#location').val(object.get('location'));
                    $('#msg').val(object.get('message'));
                    $('#title').val(object.get('title'));
                    getme(t1, object.get('end_time'));
                    //alert(174);
                }

                myApp.hideIndicator();
            },
            error: function (error) {
                config.Msg("That didn’t really go to plan. We’re not sure why. Please reload CarePool.", 'Hmm, something’s wrong', 'OK');
                myApp.hideIndicator();
            }
        });
        //get selected friends as well


    }
    $('#send').on('click', function () {
        $('#sent').val('1');
    });
    $('#preview').on('click', function () {
        $('#sent').val('0');
    });
    //submit form
    $("#offer_form").submit(function () {

        var location = $('#location').val();

        var date1_check = $('#time_picker1').val();
        var date2_check = $('#time_picker2').val();
        var msg_check = $('#msg').val();
        var savedate = $('#calendar-default').val();

        if (email_array.length === 0) {
            $('#recipient').val('');
        }
        var recipient = $('#recipient').val();
        if (location == '' || date1_check == '' || date2_check == '' || savedate == '' || msg_check == '' || recipient == '') {
            config.Msg('Please make sure you have completed all fields (date, time, recipient etc) and try sending again :)', 'We need some more info', 'OK');
            return false;
        }
        // alert(email_array.length);alert(email_name);
        // config.Msg(email_array);
        // return false;
        showind();
        //config.Msg($('#sent').val());
        if (Parse.User.current()) {

            var GameScore = Parse.Object.extend("offers");
            var query = new GameScore();


            var time1 = '';

            if ($('#time_picker1').val().indexOf('PM') > -1) {
                var res = $('#time_picker1').val().split(" ");
                res = res[0].split(":");
                if (parseInt(res[0]) < 12) {
                    res2 = parseInt(res[0]) + 12;
                } else {
                    res2 = parseInt(res[0]);
                }

                time1 = res2 + ':' + res[1]


            } else if ($('#time_picker1').val().indexOf('AM') > -1) {
                var res = $('#time_picker1').val().split(" ");
                time1 = res[0];

            } else {
                time1 = $('#time_picker1').val();
            }

            var time2 = '';
            if ($('#time_picker2').val().indexOf('PM') > -1) {
                var res = $('#time_picker2').val().split(" ");
                res = res[0].split(":");
                if (parseInt(res[0]) < 12) {
                    res2 = parseInt(res[0]) + 12;
                } else {
                    res2 = parseInt(res[0]);
                }
                time2 = res2 + ':' + res[1]

            } else if ($('#time_picker2').val().indexOf('AM') > -1) {
                var res = $('#time_picker2').val().split(" ");
                time2 = res[0];

            } else {
                time2 = $('#time_picker2').val();
            }
            //alert(savedate);
            //savedate = savedate.split(' ');
            //alert(savedate);
//            switch (savedate[1]) {
//                case 'January':
//                    cmonth = '01';
//                    break;
//                case 'February':
//                    cmonth = '02';
//                    break;
//                case 'March':
//                    cmonth = '03';
//                    break;
//                case 'April':
//                    cmonth = '04';
//                    break;
//                case 'May':
//                    cmonth = '05';
//                    break;
//                case 'June':
//                    cmonth = '06';
//                    break;
//                case 'July':
//                    cmonth = '07';
//                    break;
//                case 'August':
//                    cmonth = '08';
//                    break;
//                case 'September':
//                    cmonth = '09';
//                    break;
//                case 'October':
//                    cmonth = '10';
//                    break;
//                case 'November':
//                    cmonth = '11';
//                    break;
//                case 'December':
//                    cmonth = '12';
//                    break;
//            }
            //alert(cmonth);
            savedate = savedate + ' ' + time1;
            //alert(savedate);

            savedate = new Date(savedate);
            //alert(savedate);
            //return false;
            if ($('#objectId').val()) {
                query.set("objectId", $('#objectId').val());
            }

            // alert(time1);alert(time2);return false;
            query.set("created_by", Parse.User.current().id);
            query.set("date", savedate);
            query.set("location", $('#location').val());
            query.set("offer_Category_id", getUrlVars()["id"]);
            query.set("start_time", time1);
            query.set("end_time", time2);
            query.set("message", $('#msg').val());
            query.set("sent", parseInt($('#sent').val()));
            query.set("title", $('#title').val());
            query.set("category", getUrlVars()["cat"]);
            query.set("creator_viewed", 1);

            query.save(null,
                    {
                        success: function (object) {
                            // console.log(266);
                            //config.Msg();
                            //console.log(object);

                            //first delete recepients if found any
                            var query = new Parse.Query('user_offers');
                            query.equalTo("offer_id", object.id);
                            query.find().then(function (users) {
                                //alert(users.length);
                                var mycounter = 1;
                                //What do I do HERE to delete the posts?
                                if (users.length) {
                                    users.forEach(function (user) {
                                        user.destroy({
                                            success: function () {
                                                // SUCCESS CODE HERE, IF YOU WANT
                                                //console.log(280);

                                                if (users.length === mycounter) {
                                                    saveuser_offers(object.id, savedate);
                                                }
                                                mycounter++;
                                            },
                                            error: function () {
                                                // ERROR CODE HERE, IF YOU WANT
                                            }
                                        });
                                    });
                                } else {
                                    saveuser_offers(object.id, savedate);
                                }

                            }, function (error) {
                                config.Msg(error);
                                return false;
                            });
                            //config.Msg(email_array);config.Msg(email_name);return false;



                            //  config.Msg('getting offers after adding new offer');

                        },
                        error: function (model, error)
                        {
                            myApp.hideIndicator();
                            //$(".error").show();
                            config.Msg("That didn’t really go to plan. We’re not sure why. Please reload CarePool.", 'Hmm, something’s wrong', 'OK');
                        }
                    });
        }
        return false;
    });
    $('.movetolist').click(function () {

        if (email_contacts.length > 0) {

            showind();
            //add to the user_friends table

            // this will store the rows for use with Parse.Object.saveAll
            var lifeArray = [];

            // create a few objects, with a random state 0 or 1.
            for (var i = 0; i < email_contacts.length; i++) {
                var newLife = new MLife();
                newLife.set("user_id", Parse.User.current().id);
                newLife.set("email", email_contacts[i]);
                newLife.set("full_name", contacts_name[i]);

                lifeArray.push(newLife);
            }
            //config.Msg(JSON.stringify(lifeArray));
            // save all the newly created objects
            Parse.Object.saveAll(lifeArray, {
                success: function (objs) {
                    // objects have been saved...
                    // config.Msg('success');
                    email_contacts = [];
                    contacts_name = [];
                    redirectuser();
                },
                error: function (error) {
                    // an error occurred...
                    config.Msg("That didn’t really go to plan. We’re not sure why. Please reload CarePool.", 'Hmm, something’s wrong', 'OK');
                    return false;
                }
            });


            myApp.hideIndicator();
            //find in existing users


        } else {
            config.Msg('Add Contacts using the checkboxes on the right. If you don’t want to add any right now, just hit Close', 'Adding Contacts', 'GOT IT');
            return false;
        }


    });
});
function redirectuser() {

    displaymyfriends();
    myApp.openPanel('right');

}
function saveuser_offers(objectid, savedate) {
    // alert('315');
    //initialize the user_offers table
    showind();

    // this will store the rows for use with Parse.Object.saveAll
    var lifeArray = [];


    for (var i = 0; i < email_array.length; i++) {
        var newLife = new Life();
        newLife.set("offer_id", objectid);
        var care_provider_name, care_provider, care_receiver_name, care_receiver = '';
        if (getUrlVars()["cat"] == 'ask') {
            care_provider_name = email_name[i];
            care_provider = email_array[i];
            care_receiver_name = Parse.User.current().get('full_name');
            care_receiver = Parse.User.current().get('username');
        } else {
            care_provider_name = Parse.User.current().get('full_name');
            care_provider = Parse.User.current().get('username');
            care_receiver_name = email_name[i];
            care_receiver = email_array[i];
        }
        newLife.set("care_provider_name", care_provider_name);
        newLife.set("care_provider", care_provider);
        newLife.set("care_receiver", care_receiver);
        newLife.set("care_receiver_name", care_receiver_name);
        newLife.set("event_date", savedate);
        newLife.set("status", 'Pending');

        lifeArray.push(newLife);
    }

    // save all the newly created objects
    Parse.Object.saveAll(lifeArray, {
        success: function (objs) {
            // objects have been saved...

            if ($('#sent').val() === '1') {

                //save creation time and finishing time starts here

                //get offer id to update offers table

                var GameScore = Parse.Object.extend("offers");
                var query = new GameScore();

                //config.Msg(savedate);
                //return false;
                query.save({
                    objectId: objectid,
                    creation_start: new Date(localStorage.getItem('creation_start')),
                    creation_ends: new Date()
                },
                        {
                            success: function (object) {

                            },
                            error: function (model, error)
                            {
                                //$(".error").show();
                                // config.Msg('error');
                                config.Msg("That didn’t really go to plan. We’re not sure why. Please reload CarePool.", 'Hmm, something’s wrong', 'OK');
                                myApp.hideIndicator();
                            }
                        });


                //save create time and finishing time ends here

                //send notification to receiving parties
                var mycounter;
                for (mycounter = 0; mycounter < email_array.length; mycounter++) {

                    if (player_ids[email_array[mycounter]]) {
                        //alert(474);
                        send_players.push(player_ids[email_array[mycounter]]);

                    }
                    if (mycounter === email_array.length - 1) {
                        //alert('filtered players: ' + send_players);
                        //alert(476);
                        if (send_players.length > 0) {
                            // alert(477);
                            var titleforpush = '';
                            var message = '';
                            if (getUrlVars()["cat"] === 'offer') {
                                titleforpush = 'Yippee!';
                                message = "You have received a new offer of care from " + Parse.User.current().get('full_name') + ". Please refresh your Dashboard for details.";
                            } else {
                                titleforpush = 'Someone needs you!';
                                message = "You have received a new request for help from " + Parse.User.current().get('full_name') + ". Please refresh your Dashboard for details.";

                            }

                            $.ajax({
                                type: "post",
                                url: 'https://onesignal.com/api/v1/notifications',
                                data: {app_id: '8c1378a8-a783-400e-94da-e9505801a0c1',
                                    include_player_ids: send_players,
                                    //include_player_ids: ['07e84db6-2f92-4bc3-ba31-9a20c66ad0e8', '943cdab1-364e-47a8-b28b-fcf614e8bad4'],
                                    //contents: {"en": "A new " + getUrlVars()["cat"] + " has been created for you, please check your dashboard"},
                                    contents: {"en": message},
                                    headings: {'en': titleforpush},
                                    android_group: true,
                                    android_group_message: {"en": "You have $[notif_count] new messages"},
                                    ios_badgeType: 'Increase',
                                    ios_badgeCount: 1
                                },
                                crossDomain: true,
                                dataType: "json",
                                cache: false,
                                success: function (data) {
                                    //alert(JSON.stringify(data));
                                    backto_dashboard();
                                },
                                error: function (error) {
                                    //console.log();
                                    //alert(JSON.stringify(data));
                                    config.Msg("An eror occured while sending push request");
                                }
                            });
                        } else {
                            //alert(499);
                            backto_dashboard();
                        }

                    }
                }
                showind();

                //check here whether subscribed or not starts here
                myApp.showIndicator();
                var off_cat = Parse.Object.extend("subscription");
                var off = new Parse.Query(off_cat);
                off.equalTo("user_id", Parse.User.current().get('username'));
                off.count({
                    success: function (results) {

                        if (results === 0) {

                            if (getUrlVars()["cat"] === "ask") {
                                //getting count of ask till now
                                var off_cat = Parse.Object.extend("offers");
                                var off = new Parse.Query(off_cat);
                                off.equalTo("created_by", Parse.User.current().id);
                                off.equalTo("category", 'ask');
                                off.count({
                                    success: function (results_count) {
                                        if (results_count === 10) {

                                            send_accomplishments('http://www.carepool.co/carepool_emails/user_accomp_10.php');

                                        } else {
                                            backto_dashboard()
                                        }
                                        //myApp.hideIndicator();
                                    },
                                    error: function (error) {
                                        config.Msg("That didn’t really go to plan. We’re not sure why. Please reload CarePool.", 'Hmm, something’s wrong', 'OK');
                                        myApp.hideIndicator();
                                    }
                                });
                            } else {
                                //getting count of offers till now
                                var off_cat = Parse.Object.extend("offers");
                                var off = new Parse.Query(off_cat);
                                off.equalTo("created_by", Parse.User.current().id);
                                off.equalTo("category", 'offer');
                                off.count({
                                    success: function (results_offer) {
                                        if (results_offer === 5) {
                                            send_accomplishments('http://www.carepool.co/carepool_emails/user_accomp_5.php');
                                        } else if (results_offer === 20) {
                                            send_accomplishments('http://www.carepool.co/carepool_emails/user_accomp_20.php');
                                        } else {
                                            backto_dashboard();
                                        }

                                        //myApp.hideIndicator();
                                    },
                                    error: function (error) {
                                        config.Msg("That didn’t really go to plan. We’re not sure why. Please reload CarePool.", 'Hmm, something’s wrong', 'OK');
                                        myApp.hideIndicator();
                                    }
                                });
                            }

                        }//paste it over here

                        myApp.hideIndicator();
                    },
                    error: function (error) {
                        config.Msg("That didn’t really go to plan. We’re not sure why. Please reload CarePool.", 'Hmm, something’s wrong', 'OK');
                        myApp.hideIndicator();
                    }
                });
                //check whether subscribed or not ends here
                //
                //check recepients 
                var array_to_dismiss = [];
                var name_to_dismiss = [];
                var count_for_me = email_array.length;

                for (i = 0; i < count_for_me; i++) {

                    var index = arr_r.indexOf(email_array[i]);

                    if (index > -1) {

                        array_to_dismiss.push(email_array[i]);
                        name_to_dismiss.push(email_name[i]);
                        // email_array.splice(email_array.indexOf(email_array[i]), 1);
                        // email_name.splice(email_name.indexOf(email_name[i]), 1);
                    }

                }
                //alert(array_to_dismiss);
                //remove recepients
                var count_for_me = email_array.length;
                for (i = 0; i < count_for_me; i++) {

                    var index = email_array.indexOf(array_to_dismiss[i]);

                    if (index > -1) {

                        email_array.splice(email_array.indexOf(array_to_dismiss[i]), 1);
                        email_name.splice(email_name.indexOf(name_to_dismiss[i]), 1);
                    }

                }

                // alert('731-' + email_array);
                // alert('731-' + email_name);
                //return false;
                //send invitation to non-app user list
                if (email_array.length > 0) {

                    showind();
                    $.ajax({
                        type: "post",
                        url: 'http://www.carepool.co/carepool_emails/invite.php',
                        data: {name: Parse.User.current().get('full_name'),
                            email_list: email_array,
                            email_name: email_name},
                        crossDomain: true,
                        dataType: "json",
                        cache: false,
                        success: function (data) {
                            // console.log();

                            if (data === 'sent') {
                                backto_dashboard();
                            } else {

                                config.Msg("That didn’t really go to plan. We’re not sure why. Please reload CarePool.", 'Hmm, something’s wrong', 'OK');
                                return false;
                            }
                            //myApp.hideIndicator();
                        },
                        error: function (error) {

                            //console.log();
                            config.Msg("That didn’t really go to plan. We’re not sure why. Please reload CarePool.", 'Hmm, something’s wrong', 'OK');
                            myApp.hideIndicator();
                            return false;
                        }

                    });
                } else {
                    backto_dashboard();
                }
            } else {
                window.location = 'preview.html?id=' + objectid + '&cat=' + getUrlVars()["cat"];
            }
        },
        error: function (error) {
            // an error occurred...

            config.Msg("That didn’t really go to plan. We’re not sure why. Please reload CarePool.", 'Hmm, something’s wrong', 'OK');
            return false;
        }
    });
}
var docount = 0;
function backto_dashboard() {
    docount++;
    if (docount === 3) {
        myApp.hideIndicator();
        window.location = 'dashboard.html';
    }

}

function getme(start_time, end_time) {
    //alert(331);
    var t2 = start_time;
    var hours = Number(t2.match(/^(\d+)/)[1]);
    var minutes = Number(t2.match(/:(\d+)/)[1]);
    //alert(minutes);
    var AMPM = t2.match(/\s(.*)$/)[1];
    if (AMPM == "PM" && hours < 12)
        hours = hours;
    //alert(hours);
    if (AMPM == "AM" && hours == 12)
        hours = hours;

    var sHours1 = hours.toString();
    var sMinutes1 = minutes.toString();



    if (hours < 10)
        sHours1 = sHours1;
    //alert(sHours1);
    if (minutes < 10)
        sMinutes1 = '0' + sMinutes1;
    //alert(sMinutes1);
    //var timetodisplay = sHours + ":" + sMinutes;

    //alert(AMPM);
    //alert(sHours1);

    var today123 = new Date();



    var time_picker1 = myApp.picker({
        input: '#time_picker1',
        toolbar: true,
        rotateEffect: true,
        value: [sHours1, sMinutes1, AMPM],
        onChange: function (picker, values, displayValues) {
            var daysInMonth = new Date(picker.value[2], picker.value[0] * 1 + 1, 0).getDate();
            if (values[1] > daysInMonth) {
                picker.cols[1].setValue(daysInMonth);
            }
        },
        formatValue: function (p, values, displayValues) {
            //return timetodisplay;
            return values[0] + ':' + values[1] + ' ' + values[2];
        },
        cols: [
            // Hours
            {
                values: (function () {
                    var arr = [];
                    for (var i = 1; i <= 12; i++) {
                        arr.push(i);
                    }
                    return arr;
                })(),
            },
            // Divider
            {
                divider: true,
                content: ':'
            },
            // Minutes
            {
                values: (function () {
                    var arr = [];
                    for (var i = 0; i <= 59; i++) {
                        arr.push(i < 10 ? '0' + i : i);
                    }
                    return arr;
                })(),
            },
            {
                values: ('AM PM').split(' ')
            }
        ]
    });

    var t3 = end_time;

    var hours = Number(t3.match(/^(\d+)/)[1]);
    var minutes = Number(t3.match(/:(\d+)/)[1]);
    //alert(minutes);
    var AMPM1 = t3.match(/\s(.*)$/)[1];
    if (AMPM1 == "PM" && hours < 12)
        hours = hours;
    if (AMPM1 == "AM" && hours == 12)
        hours = hours - 12;
    var sHours = hours.toString();
    var sMinutes = minutes.toString();
    if (hours < 10)
        sHours = sHours;
    if (minutes < 10)
        sMinutes = "0" + sMinutes;
    var timetodisplay = sHours + ":" + sMinutes;

    var today1234 = new Date();



    var time_picker2 = myApp.picker({
        input: '#time_picker2',
        toolbar: true,
        rotateEffect: true,
        value: [sHours, sMinutes, AMPM1],
        onChange: function (picker, values, displayValues) {
            var daysInMonth = new Date(picker.value[2], picker.value[0] * 1 + 1, 0).getDate();
            if (values[1] > daysInMonth) {
                picker.cols[1].setValue(daysInMonth);
            }
        },
        formatValue: function (p, values, displayValues) {
            //return timetodisplay;
            return values[0] + ':' + values[1] + ' ' + values[2];
        },
        cols: [
            // Hours
            {
                values: (function () {
                    var arr = [];
                    for (var i = 1; i <= 12; i++) {
                        arr.push(i);
                    }
                    return arr;
                })(),
            },
            // Divider
            {
                divider: true,
                content: ':'
            },
            // Minutes
            {
                values: (function () {
                    var arr = [];
                    for (var i = 0; i <= 59; i++) {
                        arr.push(i < 10 ? '0' + i : i);
                    }
                    return arr;
                })(),
            },
            {
                values: ('AM PM').split(' ')
            }
        ]
    });

}


function getrecepients(offer_id) {

    showind();
    var off_cat = Parse.Object.extend("user_offers");
    var off = new Parse.Query(off_cat);
    off.equalTo("offer_id", offer_id);
    off.find({
        success: function (results) {
            console.log(results);
            for (var i = 0; i < results.length; i++) {
                var object = results[i];
                if (getUrlVars()["cat"] !== 'ask') {
                    email_array.push(object.get('care_receiver'));
                    email_name.push(object.get('care_receiver_name'));
                    $('input:checkbox[myemail="' + object.get('care_receiver') + '"]').prop('checked', true);
                } else {
                    email_array.push(object.get('care_provider'));
                    email_name.push(object.get('care_provider_name'));
                    $('input:checkbox[myemail="' + object.get('care_provider') + '"]').prop('checked', true);
                }

            }

            $('#recipient').val(email_name);
            myApp.hideIndicator();
        },
        error: function (error) {
            config.Msg("That didn’t really go to plan. We’re not sure why. Please reload CarePool.", 'Hmm, something’s wrong', 'OK');
            myApp.hideIndicator();
        }
    });

}

function get_related_offer(offer_id) {
    showind();
    var off_cat = Parse.Object.extend("offer_Category");
    var off = new Parse.Query(off_cat);
    off.equalTo("objectId", offer_id);
    off.find({
        success: function (results) {
//            console.log(results);
            for (var i = 0; i < results.length; i++) {

                var object = results[i];

                $("#offer_image")[0].src = object.get('simple_image').url();
                if ($("#title").val() === '') {
                    $("#title").val(object.get('title'));
                }


                if ($("#msg").val() == '') {
                    var message = object.get('event_message');

                    var newmsg = message.replace('[sender name]', Parse.User.current().get('full_name'));
                    if (newmsg) {
                        $("#msg").val(newmsg);
                    }
                }
                if ($("#time_picker1").val() === '') {
                    $("#time_picker1").val(object.get('start_time'));
                }

                if ($("#time_picker2").val() === '') {
                    $("#time_picker2").val(object.get('end_time'));
                }

                $("#location").val(object.get('location'));
                //(497);
                getme(object.get('start_time'), object.get('end_time'));

            }

            myApp.hideIndicator();
        },
        error: function (error) {
            config.Msg("That didn’t really go to plan. We’re not sure why. Please reload CarePool.", 'Hmm, something’s wrong', 'OK');
            myApp.hideIndicator();
        }
    });
}

function device_contacts() {
    //alert(myfriends);
    showind();
    //getting all users and their player_id for push
    var off_cat = Parse.Object.extend("User");
    var off = new Parse.Query(off_cat);
    off.find({
        success: function (results) {
            standarresult = results;
            for (var i = 0; i < results.length; i++) {
                var object = results[i];

                arr_r.push(object.get('username'));

                if (object.get('player_id') !== undefined && object.get('push_notes') === 'Yes') {
                    player_ids[object.get('username')] = object.get('player_id');
                    //alert(JSON.stringify(player_ids));
                }

            }
            //console.log(arr);

            myApp.hideIndicator();


        },
        error: function (error) {
            myApp.hideIndicator();
            config.Msg("That didn’t really go to plan. We’re not sure why. Please reload CarePool.", 'Hmm, something’s wrong', 'OK');
        }
    });
    //onDeviceReady();
}
function onDeviceReady() {
    var email_contacts = []; // {} will create an object
    var contacts_name = [];

    showind();
    // find all contacts
    var options = new ContactFindOptions();

    options.filter = "";
    options.multiple = true;
    options.hasPhoneNumber = true;
    var filter = ["displayName", "addresses"];
    navigator.contacts.find(filter, onSuccess, onError, options);
    showind();
}
var cSort = function (a, b)
{

    aName = a.name.formatted;
    bName = b.name.formatted;
    //alert(aName);
    //alert(bName);
    return aName < bName ? -1 : (aName == bName ? 0 : 1);
};
function onSuccess(contacts) {
    showind();
    var k = 1;
    contacts = contacts.sort(cSort);
    showind();
    $('#newcontactlist').empty();
    for (var i = 0; i < contacts.length; i++) {

        data = JSON.stringify(contacts);

        showind();
        var display_name = contacts[i].name.formatted;
        if (contacts[i].emails) {
            var z = 0;
            for (; z < contacts[i].emails.length; z++) {

                showind();
                var index = myfriends.indexOf(contacts[i].emails[z].value);


                if (display_name === undefined) {
                    display_name = contacts[i].emails[z].value;
                }
                var display_email = contacts[i].emails[z].value;
                if (display_email !== Parse.User.current().get('username') && index === -1) {
                    showind();
                    //alert(contacts[i].emails[0].value);alert(contacts[i].name.formatted);
                    $('#newcontactlist').append('<li><div class="item-content" style="height:65px !important;"><div class="item-media">\n\
<img src="img/profile.png" width="30" height="30"></div><div class="item-inner"><div class="item-title">' + display_name + '<br/>Email: ' + display_email + '</div>\n\
<label class="item-content col-20">\n\
<input style="width:20px" myname="' + display_name + '" myemail="' + display_email + '" type="checkbox" id="el_' + k + '" onChange="sendemail(' + k + ')">\n\
<div class="item-media"> <i class="icon icon-form-checkbox"></i></div></div></div></li>');
                    //
                    k++;

                } else {

                    $('#newcontactlist').append('<li style="background-color: lightgreen"><div class="item-content"><div class="item-media">\n\
<img src="img/profile.png" width="30" height="30"></div><div class="item-inner"><div class="item-title">' + display_name + '<br/>Email: ' + display_email + '<br/>(Already Added)</div>\n\
<label class="item-content col-20">\n\
<input style="width:20px" myname="' + display_name + '" disabled type="checkbox" >\n\
<div class="item-media"> <i class="icon icon-form-checkbox"></i></div></div></div></li>');
                }
            }
        } else {
//           / alert(238);

            $('#newcontactlist').append('<li style="background-color: #eee"><div class="item-content"><div class="item-media">\n\
<img src="img/profile.png" width="30" height="30"></div><div class="item-inner"><div class="item-title">' + display_name + '</div>\n\
<label class="item-content col-20">\n\
<input style="width:20px" myname="' + display_name + '" disabled type="checkbox" >\n\
<div class="item-media"> <i class="icon icon-form-checkbox"></i></div></div></div></li>');
        }
    }
    myApp.hideIndicator();
}
;

function onError(contactError) {

    config.Msg("That didn’t really go to plan. We’re not sure why. Please reload CarePool.", 'Hmm, something’s wrong', 'OK');
    myApp.hideIndicator();
}
;
var email_contacts = []; // {} will create an object
var contacts_name = [];
function sendemail(id) {

    var email = $('#el_' + id).attr('myemail');
    var name = $('#el_' + id).attr('myname');
//    alert(email);
//    alert(name);
    showind();
    if ($('#el_' + id).is(':checked')) {

        email_contacts.push(email);
        contacts_name.push(name);
    } else {

        var index = email_contacts.indexOf(email);
        //config.Msg(index);
        if (index > -1) {
            email_contacts.splice(index, 1);
        }

        var name_index = contacts_name.indexOf(name);

        if (name_index > -1) {
            contacts_name.splice(name_index, 1);
        }

    }
    myApp.hideIndicator();
}

function showind() {
    myApp.showIndicator();
}