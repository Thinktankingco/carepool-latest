//backup of i button
//<div class="item-after"><a class="external" href="offerdetail.html?id=' + object.id + '&color=' + classi + '"><img width="30" height="30" src="img/icons/' + icon_image + '" style="margin-top:-5px;float:right;"></a></div>

//code for displaying date data
Date.prototype.toLocaleFormat = Date.prototype.toLocaleFormat || function (pattern) {
    var year = this.getFullYear(), month = this.getMonth() + 1, day = this.getDate();
    if (month < 10)
        month = '0' + month;
    if (day < 10)
        day = '0' + day;
    return pattern.replace(/%Y/g, year).replace(/%m/g, month).replace(/%d/g, day);
};

var monthNames = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'
];
var d = new Date();
//d.setDate(d.getDate() - 1);
d.setHours(0, 0, 0, 0);
//alert(d);
//code for displaying date data
var tome_array = [];//for the whole data
var check_status = {};//for pending/approved/deleted
var check_status_from_me = [];
var main_category_array = [];//for the whole data
var cats_array = [];//only for category primary key
var ttlheight = 0;
var user_offer_upcoming = []; //my sent/received offers
var user_offer_past = []; //my sent/received offers
var cn = '';
$(function () {

    // get page height
    ttlheight = $(window).height() - 160;
    $('.page-content').css('height', ttlheight);


    myApp.showIndicator();

    //get offers categories

    var off_cat = Parse.Object.extend("offer_Category");
    var off = new Parse.Query(off_cat);
    off.find({
        success: function (results) {
            main_category_array = results;
            for (var i = 0; i < results.length; i++) {
                myApp.showIndicator();
                var object = results[i];

                cats_array.push(object.id);
                myApp.hideIndicator();
            }
            //config.Msg(cats_array);
            user_offers();

            myApp.hideIndicator();
            // config.Msg(cats_array);
        },
        error: function (error) {
            myApp.hideIndicator();
            config.Msg("That didn’t really go to plan. We’re not sure why. Please reload CarePool.", 'Hmm, something’s wrong', 'OK');
        }
    });

    //setTimeout(func, 500);

});
///window.setInterval(emtpybefore, 60000);

function emtpybefore() {
    $(".mypending").hide();
    $("#pending").empty();
    $("#offers").empty();
    $("#poffers").empty();
    user_offers();
}

function user_offers() {
    // alert(65);
    myApp.showIndicator();
    var GameScore = Parse.Object.extend("user_offers");
    var query = new Parse.Query(GameScore);

    var from_me = new Parse.Query("user_offers");
    from_me.equalTo("care_provider", Parse.User.current().get('username'));

    var to_me = new Parse.Query("user_offers");
    to_me.equalTo("care_receiver", Parse.User.current().get('username'));
    //upcomg
    var mainQuery = Parse.Query.or(from_me, to_me);
    mainQuery.greaterThanOrEqualTo("event_date", d);
    mainQuery.find({
        success: function (results) {
            // console.log(results);
            for (var i = 0; i < results.length; i++) {
                var object = results[i];
                myApp.showIndicator();
//                alert(object.get('offer_id'));
//                alert(object.get('status'));
                check_status[object.get('offer_id')] = object.get('status');


                check_status_from_me.push({
                    offer_id: object.get('offer_id'),
                    status: object.get('status')
                });


                if (object.get('care_receiver') == Parse.User.current().get('username')) {
                    //config.Msg(object.get('care_receiver'));
                    tome_array.push(object.get('offer_id'));
                }
                var name_index = user_offer_upcoming.indexOf(object.get('offer_id'));
                //config.Msg(index);
                if (name_index > -1) {
                    user_offer_upcoming.splice(name_index, 1);
                }
                user_offer_upcoming.push(object.get('offer_id'));
                myApp.hideIndicator();
            }
            //alert(JSON.stringify(check_status));
            // alert(JSON.stringify(check_status_from_me));
            //config.Msg(tome_array);
            //config.Msg(user_offer_upcoming);
            //user_offer_upcoming
            func_upcoming(user_offer_upcoming);
            myApp.hideIndicator();
        },
        error: function (error) {
            myApp.hideIndicator();
            config.Msg("That didn’t really go to plan. We’re not sure why. Please reload CarePool.", 'Hmm, something’s wrong', 'OK');
        }
    });
    myApp.showIndicator();
    //past
    var mainQuery2 = Parse.Query.or(from_me, to_me);
    mainQuery2.lessThanOrEqualTo("event_date", d);
    mainQuery2.find({
        success: function (results) {
            //console.log(results);
            for (var i = 0; i < results.length; i++) {
                var object = results[i];
                myApp.showIndicator();
                check_status[object.get('offer_id')] = object.get('status');
                var name_index = user_offer_past.indexOf(object.get('offer_id'));
                //config.Msg(index);
                if (name_index > -1) {
                    user_offer_past.splice(name_index, 1);
                }
                user_offer_past.push(object.get('offer_id'));
                myApp.hideIndicator();
            }
            func_past(user_offer_past);
            myApp.hideIndicator();
        },
        error: function (error) {
            myApp.hideIndicator();
            config.Msg("That didn’t really go to plan. We’re not sure why. Please reload CarePool.", 'Hmm, something’s wrong', 'OK');
        }
    });

}
function func_upcoming(offer_ids) {
    //alert(offer_ids);
    //get all offers
    myApp.showIndicator();
    var GameScore = Parse.Object.extend("offers");
    if (Parse.User.current()) {

        //upcoming offers
        var query = new Parse.Query(GameScore);

        //var todaysDate = new Date(d.getFullYear()); 

        query.containedIn("objectId", offer_ids);
        query.equalTo("sent", 1);
        query.notEqualTo("deleted", 1);
        query.ascending("date");
        query.find({
            success: function (results) {
                //config.Msg('called upcoming');
                //config.Msg(results);
                //config.Msg("Successfully retrieved " + results.length + " offers.");
                // Do something with the returned Parse.Object values
                $("#offers").empty();
//                if (results.length === 0) {
////                    No events upcoming. Send one now!
//                    $('#offers').append('<li><center><div class="item-title" >\n\
//        <strong><span id="text_new" style="color:black;" class="default_sub_font"><br/>This is your dashboard. \n\
//All the heroic deeds you\'ve done for others or that they\'ve done for you will appear here. \n\
//Click Offer or Ask below to set up your first event!</span></strong> <br> \n\
//<br/></div><div class="item-after"></div></center></li>');
//                } else {

                var newd = new Date().toString().split(" ");

                newd = newd[4];

                var c1 = '#FFF';
                var c2 = '#eee';

                for (var i = 0; i < results.length; i++) {
                    myApp.showIndicator();

                    //alert(i);
                    var object = results[i];
                    //config.Msg(tome_array);
                    //alert(object.get('status'));
                    //this declines means if i had declined then dont show again
                    //this accepted means if any one else had accepted then dont show to me
//                    alert(object.get('status'));
//                    alert(object.get('created_by'));
//                    alert(Parse.User.current().id);
//                    if (check_status[object.id] !== 'Declined') {
//                        if(object.get('status')=='Accepted' && object.get('created_by')==Parse.User.current().id){

                    //alert(object.id);
                    //config.Msg(object.id + ' - ' + object.get('title'));
                    var today = new Date(object.get('date'));
                    var mydate = today.toLocaleFormat('%m-%d-%Y');

                    var date = new Date();

                    // config.Msg(monthNames[mydate[0]]);
                    mydate = mydate.split('-');
                    //config.Msg(mydate[0]);
                    //config.Msg(monthNames[parseInt(mydate[0])-1]);
                    mydate = mydate[1] + ' ' + monthNames[parseInt(mydate[0]) - 1] + ' ' + mydate[2];
                    // config.Msg(mydate);

                    var magic = '';
                    var page = '';
                    var classi = '';
                    var icon_image = '';
                    var img_dislay = '';
                    var name2_index = tome_array.indexOf(object.id);
                    //config.Msg(name2_index);
                    if (name2_index > -1) {
                        page = 'approve';
                        icon_image = 'info.png';

                        classi = 'body_orange';
                        magic = '#fcaf1b';
                        img_display = main_category_array[cats_array.indexOf(object.get('offer_Category_id'))].get('color_image').url();
                    } else {
                        page = 'offerdetail';
                        icon_image = 'info.png';

                        magic = '#40CBF4';
                        classi = 'body_blue';
                        img_display = main_category_array[cats_array.indexOf(object.get('offer_Category_id'))].get('color_image1').url();

                    }
                    var time1 = object.get('start_time').split(':');
                    if (time1[0] > 12) {
                        var time11 = time1[0] - 12;

                        if (time11.toString().length === 1) {
                            time11 = '0' + time11;
                        }
                        time11 = time11 + ':' + time1[1] + ' PM';
                    } else {
                        time11 = object.get('start_time') + ' AM';
                    }

                    var time2 = object.get('end_time').split(':');
                    if (time2[0] >= 12) {
                        if (time2[0] > 12) {
                            var time12 = time2[0] - 12;
                        } else {
                            var time12 = time2[0];
                        }
                        if (time12.toString().length === 1) {
                            time12 = '0' + time12;
                        }
                        time12 = time12 + ':' + time2[1] + ' PM';
                    } else {
                        time12 = object.get('end_time') + ' AM';
                    }

                    var from = '';
                    var message = '';
                    if (magic === '#40CBF4') {
                        from = 'Care Recipient:  ';

                    } else {

                        from = 'Care Provider: ';


                    }

//offer is created by me
                    if (object.get('created_by') === Parse.User.current().id) {
                        //alert(newd);alert(object.get('end_time'));
                        message = 'from';
                        if (object.get('status') === 'Accepted') {
                            if (today.setHours(0, 0, 0, 0) === date.setHours(0, 0, 0, 0)) {
                                if (newd > object.get('end_time')) {
                                    if ($('ul#poffers li').length % 2 === 0) {
                                        cn = c1;
                                    } else {
                                        cn = c2;
                                    }
                                    $('#poffers').append('<li style="background-color: ' + cn + ';"><a class="external" href="offerdetail.html?id=' + object.id + '&color=' + classi + '&type=past"><div class="item-content"><div class="item-media">\n\
                                <img width="60" height="60" src="' + img_display + '"></div><div class="item-inner"> <div class="" >\n\
        <strong><span id="text_new" style="color:' + magic + ';" class="default_sub_font">&nbsp;' + object.get('title') + '</span></strong> <br> \n\
<span id="el_' + object.id + '" class="default_sub_font" style="color:' + magic + '">&nbsp;' + mydate + ', ' + time11 + '-' + time12 + '<br/>' + from + '</span></div></div></div><div class="sortable-handler"></div></a></li>');

                                } else {
                                    if ($('ul#offers li').length % 2 === 0) {
                                        cn = c1;
                                    } else {
                                        cn = c2;
                                    }
                                    $('#offers').append('<li style="background-color: ' + cn + ';"><a class="external" href="offerdetail.html?id=' + object.id + '&color=' + classi + '"><div class="item-content"><div class="item-media">\n\
                                <img width="60" height="60" src="' + img_display + '"></div><div class="item-inner"> <div class="" >\n\
        <strong><span id="text_new" style="color:' + magic + ';" class="default_sub_font">&nbsp;' + object.get('title') + '</span></strong> <br> \n\
<span id="el_' + object.id + '" class="default_sub_font" style="color:' + magic + '">&nbsp;' + mydate + ', ' + time11 + '-' + time12 + '<br/>' + from + '</span></div></div></div><div class="sortable-handler"></div></a></li>');

                                }
                            } else {
                                if ($('ul#offers li').length % 2 === 0) {
                                    cn = c1;
                                } else {
                                    cn = c2;
                                }
                                $('#offers').append('<li style="background-color: ' + cn + ';"><a class="external" href="offerdetail.html?id=' + object.id + '&color=' + classi + '"><div class="item-content"><div class="item-media">\n\
                                <img width="60" height="60" src="' + img_display + '"></div><div class="item-inner"> <div class="" >\n\
        <strong><span id="text_new" style="color:' + magic + ';" class="default_sub_font">&nbsp;' + object.get('title') + '</span></strong> <br> \n\
<span id="el_' + object.id + '" class="default_sub_font" style="color:' + magic + '">&nbsp;' + mydate + ', ' + time11 + '-' + time12 + '<br/>' + from + '</span></div></div></div><div class="sortable-handler"></div></a></li>');

                            }
                        } else {
                            if (today.setHours(0, 0, 0, 0) === date.setHours(0, 0, 0, 0)) {
                                if (newd > object.get('end_time')) {
                                    if ($('ul#poffers li').length % 2 === 0) {
                                        cn = c1;
                                    } else {
                                        cn = c2;
                                    }
                                    $('#poffers').append('<li style="background-color: ' + cn + ';"><a class="external" href="offerdetail.html?id=' + object.id + '&color=' + classi + '&type=past"><div class="item-content"><div class="item-media">\n\
                                <img width="60" height="60" src="' + img_display + '"></div><div class="item-inner"> <div class="" >\n\
        <strong><span id="text_new" style="color:' + magic + ';" class="default_sub_font">&nbsp;' + object.get('title') + '</span></strong> <br> \n\
<span id="el_' + object.id + '" class="default_sub_font" style="color:' + magic + '">&nbsp;' + mydate + ', ' + time11 + '-' + time12 + '<br/>' + from + '</span></div></div></div><div class="sortable-handler"></div></a></li>');

                                } else {
                                    //get all the status against offer
                                    var find_status = $.grep(check_status_from_me, function (e) {
                                        return e.offer_id == object.id;
                                    });
                                    //alert(find_status.length);
                                    //count deleted records against offer 
                                    var numBoys = find_status.reduce(function (n, person) {
                                        return n + (person.status == 'Declined');
                                    }, 0);

                                    //find the occurence of Declined 

                                    //if not declined by all
                                    if (numBoys !== find_status.length) {
                                        //find the occurrence of accepted
                                        var occurances2 = find_status.reduce(function (n, person) {
                                            return n + (person.status == 'Accepted');
                                        }, 0);

                                        if (occurances2 >= 1) {
                                            if ($('ul#offers li').length % 2 === 0) {
                                                cn = c1;
                                            } else {
                                                cn = c2;
                                            }
                                            $('#offers').append('<li style="background-color: ' + cn + ';"><a class="external" href="offerdetail.html?id=' + object.id + '&color=' + classi + '"><div class="item-content"><div class="item-media">\n\
                                <img width="60" height="60" src="' + img_display + '"></div><div class="item-inner"> <div class="" >\n\
        <strong><span id="text_new" style="color:' + magic + ';" class="default_sub_font">&nbsp;' + object.get('title') + '</span></strong> <br> \n\
<span id="el_' + object.id + '" class="default_sub_font" style="color:' + magic + '">&nbsp;' + mydate + ', ' + time11 + '-' + time12 + '<br/>' + from + '</span></div></div></div><div class="sortable-handler"></div></a></li>');

                                        } else {
                                            $('.mypending').show();
                                            if ($('ul#pending li').length % 2 === 0) {
                                                cn = c1;
                                            } else {
                                                cn = c2;
                                            }
                                            $('#pending').append('<li style="background-color: ' + cn + ';"><a class="external" href="offerdetail.html?id=' + object.id + '&color=' + classi + '"><div class="item-content"><div class="item-media">\n\
                                <img width="60" height="60" src="' + img_display + '"></div><div class="item-inner"> <div class="" >\n\
        <strong><span id="text_new" style="color:' + magic + ';" class="default_sub_font">&nbsp;' + object.get('title') + '</span></strong> <br> \n\
<span id="el_' + object.id + '" class="default_sub_font" style="color:' + magic + '">&nbsp;' + mydate + ', ' + time11 + '-' + time12 + '<br/>' + from + '</span></div></div></div><div class="sortable-handler"></div></a></li>');

                                        }
                                    } else {
                                        //display me until i had seen it 391
                                        if ($('ul#poffers li').length % 2 === 0) {
                                            cn = c1;
                                        } else {
                                            cn = c2;
                                        }
                                        $('#poffers').append('<li style="background-color: ' + cn + ';"><a class="external" href="offerdetail.html?id=' + object.id + '&color=' + classi + '&type=past"><div class="item-content"><div class="item-media">\n\
                                <img width="60" height="60" src="' + img_display + '"></div><div class="item-inner"> <div class="" >\n\
        <strong><span id="text_new" style="color:' + magic + ';" class="default_sub_font">&nbsp;' + object.get('title') + '</span></strong> <br> \n\
<span id="el_' + object.id + '" class="default_sub_font" style="color:' + magic + '">&nbsp;' + mydate + ', ' + time11 + '-' + time12 + '<br/>' + from + '</span></div></div></div><div class="sortable-handler"></div></a></li>');

                                    }
                                }
                            } else {
                                //get all the status against offer
                                var find_status = $.grep(check_status_from_me, function (e) {
                                    return e.offer_id == object.id;
                                });
                                //alert(find_status.length);
                                //count deleted records against offer 
                                var numBoys = find_status.reduce(function (n, person) {
                                    return n + (person.status == 'Declined');
                                }, 0);

                                //find the occurence of Declined 

                                //if not declined by all
                                if (numBoys !== find_status.length) {
                                    //find the occurrence of accepted
                                    var occurances2 = find_status.reduce(function (n, person) {
                                        return n + (person.status == 'Accepted');
                                    }, 0);

                                    if (occurances2 >= 1) {
                                        if ($('ul#offers li').length % 2 === 0) {
                                            cn = c1;
                                        } else {
                                            cn = c2;
                                        }
                                        $('#offers').append('<li style="background-color: ' + cn + ';"><a class="external" href="offerdetail.html?id=' + object.id + '&color=' + classi + '"><div class="item-content"><div class="item-media">\n\
                                <img width="60" height="60" src="' + img_display + '"></div><div class="item-inner"> <div class="" >\n\
        <strong><span id="text_new" style="color:' + magic + ';" class="default_sub_font">&nbsp;' + object.get('title') + '</span></strong> <br> \n\
<span id="el_' + object.id + '" class="default_sub_font" style="color:' + magic + '">&nbsp;' + mydate + ', ' + time11 + '-' + time12 + '<br/>' + from + '</span></div></div></div><div class="sortable-handler"></div></a></li>');

                                    } else {
                                        $('.mypending').show();
                                        if ($('ul#pending li').length % 2 === 0) {
                                            cn = c1;
                                        } else {
                                            cn = c2;
                                        }
                                        $('#pending').append('<li style="background-color: ' + cn + ';"><a class="external" href="offerdetail.html?id=' + object.id + '&color=' + classi + '"><div class="item-content"><div class="item-media">\n\
                                <img width="60" height="60" src="' + img_display + '"></div><div class="item-inner"> <div class="" >\n\
        <strong><span id="text_new" style="color:' + magic + ';" class="default_sub_font">&nbsp;' + object.get('title') + '</span></strong> <br> \n\
<span id="el_' + object.id + '" class="default_sub_font" style="color:' + magic + '">&nbsp;' + mydate + ', ' + time11 + '-' + time12 + '<br/>' + from + '</span></div></div></div><div class="sortable-handler"></div></a></li>');

                                    }
                                }
                            }
                        }
                    } else {
                        message = 'to';
                        //offer is created by some1 else
                        //requested to me
                        if (object.get('status') === 'Accepted') {
                            if (object.get('accepted_by') === Parse.User.current().id) {
                                if (today.setHours(0, 0, 0, 0) === date.setHours(0, 0, 0, 0)) {
                                    if (newd > object.get('end_time')) {
                                        if ($('ul#poffers li').length % 2 === 0) {
                                            cn = c1;
                                        } else {
                                            cn = c2;
                                        }
                                        $('#poffers').append('<li style="background-color: ' + cn + ';"><a class="external" href="offerdetail.html?id=' + object.id + '&color=' + classi + '&type=past"><div class="item-content"><div class="item-media">\n\
                                <img width="60" height="60" src="' + img_display + '"></div><div class="item-inner"> <div class="" >\n\
        <strong><span id="text_new" style="color:' + magic + ';" class="default_sub_font">&nbsp;' + object.get('title') + '</span></strong> <br> \n\
<span id="el_' + object.id + '" class="default_sub_font" style="color:' + magic + '">&nbsp;' + mydate + ', ' + time11 + '-' + time12 + '<br/>' + from + '</span></div></div></div><div class="sortable-handler"></div></a></li>');

                                    } else {
                                        if ($('ul#offers li').length % 2 === 0) {
                                            cn = c1;
                                        } else {
                                            cn = c2;
                                        }
                                        $('#offers').append('<li style="background-color: ' + cn + ';"><a class="external" href="offerdetail.html?id=' + object.id + '&color=' + classi + '"><div class="item-content"><div class="item-media">\n\
                                <img width="60" height="60" src="' + img_display + '"></div><div class="item-inner"> <div class="" >\n\
        <strong><span id="text_new" style="color:' + magic + ';" class="default_sub_font">&nbsp;' + object.get('title') + '</span></strong> <br> \n\
<span id="el_' + object.id + '" class="default_sub_font" style="color:' + magic + '">&nbsp;' + mydate + ', ' + time11 + '-' + time12 + '<br/>' + from + '</span></div></div></div><div class="sortable-handler"></div></a></li>');

                                    }
                                } else {
                                    if ($('ul#offers li').length % 2 === 0) {
                                        cn = c1;
                                    } else {
                                        cn = c2;
                                    }
                                    $('#offers').append('<li style="background-color: ' + cn + ';"><a class="external" href="offerdetail.html?id=' + object.id + '&color=' + classi + '"><div class="item-content"><div class="item-media">\n\
                                <img width="60" height="60" src="' + img_display + '"></div><div class="item-inner"> <div class="" >\n\
        <strong><span id="text_new" style="color:' + magic + ';" class="default_sub_font">&nbsp;' + object.get('title') + '</span></strong> <br> \n\
<span id="el_' + object.id + '" class="default_sub_font" style="color:' + magic + '">&nbsp;' + mydate + ', ' + time11 + '-' + time12 + '<br/>' + from + '</span></div></div></div><div class="sortable-handler"></div></a></li>');

                                }
                            }
                            //added for offer things starts here
                            else {
                                if (object.get('category') === 'offer') {
                                    //do code here
                                    if (check_status[object.id] !== 'Declined') {
                                        if (today.setHours(0, 0, 0, 0) === date.setHours(0, 0, 0, 0)) {
                                            if (newd > object.get('end_time')) {
                                                if ($('ul#poffers li').length % 2 === 0) {
                                                    cn = c1;
                                                } else {
                                                    cn = c2;
                                                }
                                                $('#poffers').append('<li style="background-color: ' + cn + ';"><a class="external" href="offerdetail.html?id=' + object.id + '&color=' + classi + '&type=past"><div class="item-content"><div class="item-media">\n\
                                <img width="60" height="60" src="' + img_display + '"></div><div class="item-inner"> <div class="" >\n\
        <strong><span id="text_new" style="color:' + magic + ';" class="default_sub_font">&nbsp;' + object.get('title') + '</span></strong> <br> \n\
<span id="el_' + object.id + '" class="default_sub_font" style="color:' + magic + '">&nbsp;' + mydate + ', ' + time11 + '-' + time12 + '<br/>' + from + '</span></div></div></div><div class="sortable-handler"></div></a></li>');

                                            } else {
                                                if ($('ul#pending li').length % 2 === 0) {
                                                    cn = c1;
                                                } else {
                                                    cn = c2;
                                                }
                                                $('.mypending').show();
                                                $('#pending').append('<li style="background-color: ' + cn + ';"><a class="external" href="offerdetail.html?id=' + object.id + '&color=' + classi + '"><div class="item-content"><div class="item-media">\n\
                                <img width="60" height="60" src="' + img_display + '"></div><div class="item-inner"> <div class="" >\n\
        <strong><span id="text_new" style="color:' + magic + ';" class="default_sub_font">&nbsp;' + object.get('title') + '</span></strong> <br> \n\
<span id="el_' + object.id + '" class="default_sub_font" style="color:' + magic + '">&nbsp;' + mydate + ', ' + time11 + '-' + time12 + '<br/>' + from + '</span></div></div></div><div class="sortable-handler"></div></a></li>');

                                            }
                                        } else {
                                            if ($('ul#pending li').length % 2 === 0) {
                                                cn = c1;
                                            } else {
                                                cn = c2;
                                            }
                                            $('.mypending').show();
                                            $('#pending').append('<li style="background-color: ' + cn + ';"><a class="external" href="offerdetail.html?id=' + object.id + '&color=' + classi + '"><div class="item-content"><div class="item-media">\n\
                                <img width="60" height="60" src="' + img_display + '"></div><div class="item-inner"> <div class="" >\n\
        <strong><span id="text_new" style="color:' + magic + ';" class="default_sub_font">&nbsp;' + object.get('title') + '</span></strong> <br> \n\
<span id="el_' + object.id + '" class="default_sub_font" style="color:' + magic + '">&nbsp;' + mydate + ', ' + time11 + '-' + time12 + '<br/>' + from + '</span></div></div></div><div class="sortable-handler"></div></a></li>');

                                        }
                                    }
                                } else {
                                    //means it will be request then show it in past for me but the question arises will the past will be clickable or not???
                                    if ($('ul#poffers li').length % 2 === 0) {
                                        cn = c1;
                                    } else {
                                        cn = c2;
                                    }
                                    $('#poffers').append('<li style="background-color: ' + cn + ';"><a class="external" href="offerdetail.html?id=' + object.id + '&color=' + classi + '&type=past"><div class="item-content"><div class="item-media">\n\
                                <img width="60" height="60" src="' + img_display + '"></div><div class="item-inner"> <div class="" >\n\
        <strong><span id="text_new" style="color:' + magic + ';" class="default_sub_font">&nbsp;' + object.get('title') + '</span></strong> <br> \n\
<span id="el_' + object.id + '" class="default_sub_font" style="color:' + magic + '">&nbsp;' + mydate + ', ' + time11 + '-' + time12 + '<br/>' + from + '</span></div></div></div><div class="sortable-handler"></div></a></li>');

                                }
                            }

                            //added for offer thingss ends here
                        } else {
                            if (check_status[object.id] !== 'Declined') {

                                if (today.setHours(0, 0, 0, 0) === date.setHours(0, 0, 0, 0)) {
                                    if (newd > object.get('end_time')) {
                                        if ($('ul#poffers li').length % 2 === 0) {
                                            cn = c1;
                                        } else {
                                            cn = c2;
                                        }
                                        $('#poffers').append('<li style="background-color: ' + cn + ';"><a class="external" href="offerdetail.html?id=' + object.id + '&color=' + classi + '&type=past"><div class="item-content"><div class="item-media">\n\
                                <img width="60" height="60" src="' + img_display + '"></div><div class="item-inner"> <div class="" >\n\
        <strong><span id="text_new" style="color:' + magic + ';" class="default_sub_font">&nbsp;' + object.get('title') + '</span></strong> <br> \n\
<span id="el_' + object.id + '" class="default_sub_font" style="color:' + magic + '">&nbsp;' + mydate + ', ' + time11 + '-' + time12 + '<br/>' + from + '</span></div></div></div><div class="sortable-handler"></div></a></li>');

                                    } else {
                                        if ($('ul#pending li').length % 2 === 0) {
                                            cn = c1;
                                        } else {
                                            cn = c2;
                                        }
                                        $('.mypending').show();
                                        $('#pending').append('<li style="background-color: ' + cn + ';"><a class="external" href="offerdetail.html?id=' + object.id + '&color=' + classi + '"><div class="item-content"><div class="item-media">\n\
                                <img width="60" height="60" src="' + img_display + '"></div><div class="item-inner"> <div class="" >\n\
        <strong><span id="text_new" style="color:' + magic + ';" class="default_sub_font">&nbsp;' + object.get('title') + '</span></strong> <br> \n\
<span id="el_' + object.id + '" class="default_sub_font" style="color:' + magic + '">&nbsp;' + mydate + ', ' + time11 + '-' + time12 + '<br/>' + from + '</span></div></div></div><div class="sortable-handler"></div></a></li>');

                                    }
                                } else {
                                    $('.mypending').show();
                                    if ($('ul#pending li').length % 2 === 0) {
                                        cn = c1;
                                    } else {
                                        cn = c2;
                                    }
                                    $('#pending').append('<li style="background-color: ' + cn + ';"><a class="external" href="offerdetail.html?id=' + object.id + '&color=' + classi + '"><div class="item-content"><div class="item-media">\n\
                                <img width="60" height="60" src="' + img_display + '"></div><div class="item-inner"> <div class="" >\n\
        <strong><span id="text_new" style="color:' + magic + ';" class="default_sub_font">&nbsp;' + object.get('title') + '</span></strong> <br> \n\
<span id="el_' + object.id + '" class="default_sub_font" style="color:' + magic + '">&nbsp;' + mydate + ', ' + time11 + '-' + time12 + '<br/>' + from + '</span></div></div></div><div class="sortable-handler"></div></a></li>');

                                }
                            }

                        }
                    }

                    //alert(object.id);
                    //alert(message);
                    findreceivers(object.id, magic, message);
//                    }
//                    }
                }

                if ($('ul#offers li').length === 0) {
                    $('#offers').append('<li><center><div class="" >\n\
        <strong><span id="text_new" style="color:black;padding: 0 25px;" class="default_sub_font"><br/>This is your dashboard. \n\
All the heroic deeds you\'ve done for others or that they\'ve done for you will appear here. \n\
Click Offer or Ask below to set up your first event!</span></strong> <br> \n\
<br/></div><div class="item-after"></div></center></li>');
                }
                //}
                myApp.hideIndicator();
            },
            error: function (error) {
                myApp.hideIndicator();
                config.Msg("That didn’t really go to plan. We’re not sure why. Please reload CarePool.", 'Hmm, something’s wrong', 'OK');
            }
        });

    }

}
function func_past(offer_ids) {

    if (Parse.User.current()) {
        //past offers

        var GameScore = Parse.Object.extend("offers");
        myApp.showIndicator();
        var query2 = new Parse.Query(GameScore);
        query2.containedIn("objectId", offer_ids);
        query2.equalTo("sent", 1);
        query2.notEqualTo("deleted", 1);
        query2.descending("date");
        query2.find({
            success: function (results) {
                //config.Msg('called past');

                //config.Msg("Successfully retrieved " + results.length + " offers.");
                // Do something with the returned Parse.Object values
                //$("#poffers").empty();
                var c3 = '#FFF';
                var c4 = '#eee';
                for (var i = 0; i < results.length; i++) {
                    var object = results[i];
                    if (check_status[object.id] !== 'Declined' && object.get('status') !== 'Accepted') {

                        myApp.showIndicator();

                        //config.Msg(object.id + ' - ' + object.get('title'));

                        var from = '';
                        var message = '';
                        if (main_category_array[cats_array.indexOf(object.get('offer_Category_id'))].get('category') == 'help') {
                            classi = 'body_orange';
                            magic1 = '#fcaf1b';
                            img_display1 = main_category_array[cats_array.indexOf(object.get('offer_Category_id'))].get('color_image').url();
                        } else {
                            magic1 = '#40CBF4';
                            classi = 'body_blue';
                            img_display1 = main_category_array[cats_array.indexOf(object.get('offer_Category_id'))].get('color_image1').url();
                        }
                        if (magic1 === '#40CBF4') {
                            from = 'Care Recipient: ';
                            message = 'from';
                        } else {
                            message = 'to';
                            from = 'Care Provider: ';
                        }
                        //alert(from);
                        var today = new Date(object.get('date'));
                        var mydate = today.toLocaleFormat('%m-%d-%Y');
                        //config.Msg(mydate);

                        // config.Msg(monthNames[mydate[0]]);
                        mydate = mydate.split('-');
                        //config.Msg(mydate[0]);
                        //config.Msg(monthNames[parseInt(mydate[0])-1]);
                        mydate = mydate[1] + ' ' + monthNames[parseInt(mydate[0]) - 1] + ' ' + mydate[2];
                        //                                    $('#poffers').append('<tr id="' + object.id + '"><td>' + object.get('title') + '</td><td>' + mydate + '</td>\n\
                        //                    <td>' + object.get('start_time') + '</td><td>' + object.get('location') + '</td>\n\
                        //                        <td><a href="addoffer.html?id=' + object.id + '" >Edit</a> | \n\
                        //                            <a style="cursor:pointer" onclick="deleteoffer(' + "'" + object.id + "'" + ')">Delete</a></td></tr>');



                        var page = '';
                        var icon_image = '';
                        var img_dislay1 = '';
                        var name2_index = tome_array.indexOf(object.id);
                        //config.Msg(name2_index);
                        if (name2_index > -1) {

                            page = 'approve';
                            icon_image = 'info.png';
                        } else {

                            page = 'offerdetail';
                            icon_image = 'info.png';
                        }



                        var time1 = object.get('start_time').split(':');
                        if (time1[0] > 12) {
                            var time11 = time1[0] - 12;

                            if (time11.toString().length === 1) {
                                time11 = '0' + time11;
                            }
                            time11 = time11 + ':' + time1[1] + ' PM';
                        } else {
                            time11 = object.get('start_time') + ' AM';
                        }

                        var time2 = object.get('end_time').split(':');
                        if (time2[0] >= 12) {
                            if (time2[0] > 12) {
                                var time12 = time2[0] - 12;
                            } else {
                                var time12 = time2[0];
                            }
                            if (time12.toString().length === 1) {
                                time12 = '0' + time12;
                            }
                            time12 = time12 + ':' + time2[1] + ' PM';
                        } else {
                            time12 = object.get('end_time') + ' AM';
                        }

                        //alert(object.get('end_time'));
                        //code 4
                        if ($('ul#poffers li').length % 2 === 0) {
                            cn = c3;
                        } else {
                            cn = c4;
                        }
//                        alert(532);
                        $('#poffers').append('<li style="background-color: ' + cn + ';"><a class="external" href="' + page + '.html?id=' + object.id + '&color=' + classi + '&type=past"><div class="item-content"><div class="item-media">\n\
                                <img width="60" height="60" src="' + img_display1 + '"></div><div class="item-inner"> <div class="">\n\
        <strong><span id="text_new"  style="color:' + magic1 + '" class="default_sub_font">' + object.get('title') + '</span></strong> <br>\n\
<span id="el_' + object.id + '"  class="default_sub_font" style="color:' + magic1 + '">' + mydate + ', ' + time11 + '-' + time12 + '<br/>' + from + '</span></div></div></div><div class="sortable-handler"></div></a></li>');
                        myApp.hideIndicator();
                        findreceivers(object.id, magic1, message);
                    }


                }
                myApp.hideIndicator();

            },
            error: function (error) {
                myApp.hideIndicator();
                config.Msg("That didn’t really go to plan. We’re not sure why. Please reload CarePool.", 'Hmm, something’s wrong', 'OK');
            }
        });
    }
}
//setTimeout(function(){alert($('ul#offers li').length)},1500);
function findreceivers(element_id, from, to) {
//alert(to);
    //upcoming offers
//alert(element_id);
//alert(from);
//    var GameScore = Parse.Object.extend("offers");
//    var query = new Parse.Query(GameScore);
//
//    //var todaysDate = new Date(d.getFullYear()); 
//
//    query.equalTo("objectId", element_id);
//    query.find({
//        success: function (mresults) {
//
//            //alert(mresults.length);
//            if (mresults.length > 0) {
//                var str = '';
//                for (var i = 0; i < mresults.length; i++) {
//
//                    myApp.showIndicator();
//
//
//                    var object = mresults[i];
    //config.Msg(tome_array);

    //if (check_status[object.id] !== 'Declined') {
    //alert(500);
    myApp.showIndicator();
    var off_cat = Parse.Object.extend("user_offers");
    var off = new Parse.Query(off_cat);
    off.equalTo("offer_id", element_id);
    off.find({
        success: function (results) {
            var names = '';
            //alert(results.length);
            for (var i = 0; i < results.length; i++) {
                myApp.showIndicator();
                var myobject = results[i];

                //config.Msg(object.get('date'));
                var name;
                //config.Msg(object.get('date'));
                if (from === '#40CBF4') {
                    name = myobject.get('care_receiver_name');
                } else {
                    name = myobject.get('care_provider_name');
                }

                if (names.indexOf(name) === -1) {
                    names = names + name + ', ';
                }
                //if (i === results.length - 1) {
                if (results.length === 1) {
                    $('#el_' + element_id).append(name);
                } else {
                    if (i === 1) {
                        str = names.substring(', ', names.length - 2);
                        //console.log(str);
                        //alert(str);
                        //$('#el_' + element_id).append(str);
                        if (str.indexOf(',') === -1) {
                            //alert('#el_' + element_id);
                            //alert(str);
                            $('#el_' + element_id).append(str);
                        } else {
                            $('#el_' + element_id).append(str + '...');
                        }

                        break;
                    }
                }

            }

            myApp.hideIndicator();
        },
        error: function (error) {
            config.Msg("That didn’t really go to plan. We’re not sure why. Please reload CarePool.", 'Hmm, something’s wrong', 'OK');
            myApp.hideIndicator();
        }
    });
    //}
//                }
//            }
//            myApp.hideIndicator();
//        },
//        error: function (error) {
//            myApp.hideIndicator();
//            config.Msg("That didn’t really go to plan. We’re not sure why. Please reload CarePool.", 'Hmm, something’s wrong', 'OK');
//        }
//    });

}