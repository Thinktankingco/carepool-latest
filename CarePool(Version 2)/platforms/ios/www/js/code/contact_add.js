var myfriends = new Array();
$(function () {

//get all  my friends
    myApp.showIndicator();

    var Life = Parse.Object.extend("user_friends");
    var mylife = new Parse.Query(Life);
    mylife.equalTo("user_id", Parse.User.current().id);
    mylife.find({
        success: function (results) {
            var fr = '';
            for (var i = 0; i < results.length; i++) {
                var object = results[i];
                fr = object.get('email').replace(/\s/g, '');
                myfriends.push(fr);

            }
            // console.log(myfriends);
            myApp.hideIndicator();
            onDeviceReady();
        },
        error: function (error) {
            myApp.hideIndicator();
            config.Msg("That didn’t really go to plan. We’re not sure why. Please reload CarePool.", 'Hmm, something’s wrong', 'OK');
        }
    });

    //setting page height
    ttlheight = $(window).height() - 200;
    $('.page-content').css('height', ttlheight);

    //get form submit
    $('#submit').click(function () {
        //config.Msg(email_array);
        if (email_array.length > 0) {
            myApp.showIndicator();
            //add to the user_friends table

            // this will store the rows for use with Parse.Object.saveAll
            var lifeArray = [];

            // create a few objects, with a random state 0 or 1.
            for (var i = 0; i < email_array.length; i++) {
                var newLife = new Life();
                newLife.set("user_id", Parse.User.current().id);
                var display_email2 = email_array[i].replace(/\s/g, '');
                display_email2 = display_email2.split('-').join('');
                display_email2 = display_email2.split('(').join('');
                display_email2 = display_email2.split(')').join('');
//                alert(50);
//                alert(display_email2);
                newLife.set("email", display_email2);
                newLife.set("full_name", email_name[i]);

                lifeArray.push(newLife);
            }
            //config.Msg(lifeArray);
            // save all the newly created objects
            Parse.Object.saveAll(lifeArray, {
                success: function (objs) {
                    // objects have been saved...
                    // config.Msg('success');
                    redirectuser();
                },
                error: function (error) {
                    // an error occurred...
                    config.Msg(error);
                    return false;
                }
            });

            //find in existing users
//            for (i = 0; i < email_array.length; i++) {
//                //config.Msg(getKeyByValue(arr,email_array[i]));
//                var index = arr.indexOf(email_array[i]);
//                //config.Msg(index);
//                if (index > -1) {
//                    email_array.splice(index, 1);
//                    email_name.splice(index, 1);
//                }
//
//            }
            myApp.hideIndicator();
            //config.Msg(email_array);
            //send invitation to non-app user list
            //if (email_array.length > 0) {

//                myApp.showIndicator();
//                $.ajax({
//                    type: "post",
//                    url: 'http://www.carepool.co/carepool_emails/invite.php',
//                    data: {name: Parse.User.current().get('full_name'),
//                        email_list: email_array,
//                        email_name: email_name},
//                    crossDomain: true,
//                    dataType: "json",
//                    cache: false,
//                    success: function (data) {
//                        // console.log();
//                        //config.Msg(data);return false;
//                        if (data === 'sent') {
//                            // config.Msg('Email sent to the developer');
//                            
////                            name=junior&email_list%5B%5D=ahsan_ibms154%40yahoo.com&email_list%5B%5D=alijunior%40logisticslogic.com
////&email_name%5B%5D=ahsan+ali&email_name%5B%5D=ali+junior
//                            
//                            redirectuser();
//                        } else {
//                            config.Msg("An eror occured while sending email");
//                            return false;
//                        }
//                        myApp.hideIndicator();
//                    },
//                    error: function (error) {
//                        // config.Msg(JSON.stringify(error));
//                        //console.log();
//                        config.Msg("That didn’t really go to plan. We’re not sure why. Please reload CarePool.", 'Hmm, something’s wrong', 'OK');
//                        myApp.hideIndicator();
//                        return false;
//                    }
//
//                });
//            } else {
//                redirectuser();
//            }
        } else {
            config.Msg('Please Select atleast 1 contact to Invite', 'Hmm, something’s wrong', 'OK');
            return false;
        }


    });

    //get all current users

    myApp.showIndicator();

    var arr = new Array(); // {} will create an object

    var off_cat = Parse.Object.extend("User");
    var off = new Parse.Query(off_cat);
    off.find({
        success: function (results) {
            standarresult = results;
            for (var i = 0; i < results.length; i++) {
                var object = results[i];

                arr.push(object.get('username'));
            }
            //console.log(arr);
            //config.Msg(arr);
            myApp.hideIndicator();
            // config.Msg(cats_array);

        },
        error: function (error) {
            myApp.hideIndicator();
            config.Msg("That didn’t really go to plan. We’re not sure why. Please reload CarePool.", 'Hmm, something’s wrong', 'OK');
        }
    });

});

function redirectuser() {
    window.location.href = 'contact.html';
}

//document.addEventListener("deviceready", onDeviceReady, false);
function onDeviceReady() {
    //console.log(navigator.contacts);
    //config.Msg('page is ready');
    myApp.showIndicator();
    // find all contacts
    var options = new ContactFindOptions();
    //config.Msg(options);
    options.filter = "";
    options.multiple = true;
    options.hasPhoneNumber = true;
    options.desiredFields = [navigator.contacts.fieldType.name, navigator.contacts.fieldType.phoneNumbers];
    var filter = [navigator.contacts.fieldType.name, navigator.contacts.fieldType.phoneNumbers];

    navigator.contacts.find(filter, onSuccess, onError, options);
    myApp.showIndicator();
}
var cSort = function (a, b)
{
    //alert(172);
    //
//config.Msg(JSON.stringify(a));config.Msg(JSON.stringify(b));
//config.Msg(a.name.formatted);config.Msg(b.name.formatted);
    if (typeof a.name.formatted === 'string') {


        aName = a.name.formatted;
        bName = b.name.formatted;

        //alert(aName);
        //alert(bName);
        return aName < bName ? -1 : (aName == bName ? 0 : 1);
    }
};
function onSuccess(contacts) {
    var already = [];
    var namestart = '';
    myApp.showIndicator();
    var j = 1;
    contacts = contacts.sort(cSort);
    myApp.showIndicator();
    $('#contactlist').empty();
    //looping throught each contact
    //config.Msg(contacts);
    data = JSON.stringify(contacts);
    var regExp = /^00[0-9].*$/;
    for (var i = 0; i < contacts.length; i++) {

        // config.Msg(contacts[i]);break;return false;

        myApp.showIndicator();
        var display_name = contacts[i].name.formatted;
        //alert(display_name.slice(0,1).toLowerCase());
        //check if user has email addresses or not
        if (contacts[i].phoneNumbers) {
            myApp.showIndicator();

            var k = 0;
            //looping through all email adreses to display
            for (; k < contacts[i].phoneNumbers.length; k++) {

                var display_email = contacts[i].phoneNumbers[k].value;
                display_email = display_email.replace(/\s/g, '');
                display_email = display_email.split('-').join('');
                display_email = display_email.split('(').join('');
                display_email = display_email.split(')').join('');
                if (display_email.indexOf("+") === -1) {

                    if (regExp.test(display_email)) {
                        //alert(regExp.test("001")); // true 
                        display_email = '+' + display_email.substring(2);
                    } else {
                        display_email = Parse.User.current().get('country_code') + display_email.substring(1);
                    }

                }

                var index = myfriends.indexOf(display_email);
                var index2 = already.indexOf(display_email);


                // config.Msg(index);
                // config.Msg(typeof index);
                if (index2 === -1) {
                    already.push(display_email);
                    if (display_name === undefined) {
                        display_name = display_email;
                    }
                    if (typeof display_name === 'string') {
                        namestart = display_name.slice(0, 1).toLowerCase();
                    } else {
                        namestart = '';
                    }

                    // alert(221);
                    if (display_email !== Parse.User.current().get('username') && index === -1) {
                        myApp.showIndicator();

                        //config.Msg(contacts[i].name.formatted);
                        $('#contactlist').append('<li class="' + namestart + '"><div class="item-content"><div class="item-media">\n\
<img src="img/profile.png" width="30" height="30"></div><div class="item-inner"><div class="item-title">' + display_name + '<br/>Number: ' + display_email + '</div></div>\n\
<label class="item-content col-20">\n\
<input myname="' + display_name + '" myemail="' + display_email + '" type="checkbox" id="' + j + '" onChange="sendemail(' + j + ')">\n\
<div class="item-media"> <i class="icon icon-form-checkbox"></i></div></div></li>');
                        //
                        j++;
                        //config.Msg(j);
                    } else {
                        $('#contactlist').append('<li class="' + namestart + '" style="background-color: lightgreen"><div class="item-content"><div class="item-media">\n\
<img src="img/profile.png" width="30" height="30"></div><div class="item-inner"><div class="item-title">' + display_name + '<br/>Number: ' + display_email + '<br/>(Already Added)</div></div>\n\
<label class="item-content col-20">\n\
<input disabled myname="' + display_name + '" myemail="' + display_email + '" type="checkbox">\n\
<div class="item-media"> <i class="icon icon-form-checkbox"></i></div></div></li>');
                    }
                }
            }
        } else {
            if (typeof display_name === 'string') {
                namestart = display_name.slice(0, 1).toLowerCase();
            } else {
                namestart = '';
            }
//           / alert(238);
            $('#contactlist').append('<li class="' + namestart + '" style="background-color: #eee"><div class="item-content"><div class="item-media">\n\
<img src="img/profile.png" width="30" height="30"></div><div class="item-inner"><div class="item-title">' + display_name + '</div></div>\n\
<label class="item-content col-20">\n\
<input myname="' + display_name + '" disabled type="checkbox">\n\
<div class="item-media"> <i class="icon icon-form-checkbox"></i></div></div></li>');
        }
    }
    myApp.hideIndicator();
}
;

function onError(contactError) {
    config.Msg("In Order To reactivate this permission, please got to settings->app->carepool and allow contacts", 'Hmm, something’s wrong', 'OK');
    //alert(258);
    myApp.hideIndicator();
}

function sendletter(elem) {
    var container = $('div.page-content'),
            scrollTo = $('.' + elem + ':first');
    container.animate({
        scrollTop: scrollTo.offset().top - container.offset().top + container.scrollTop()
    });
}

var email_array = []; // {} will create an object
var email_name = [];
function sendemail(id) {
    //config.Msg(id);
//config.Msg($('#'+ id).attr('myemail'));
//config.Msg($('#'+ id).attr('myname'));
    var email = $('#' + id).attr('myemail');
    var name = $('#' + id).attr('myname');
    myApp.showIndicator();
    if ($('#' + id).is(':checked')) {
        // Do stuff
        email_array.push(email);
        email_name.push(name);
    } else {
        var index = email_array.indexOf(email);
        //config.Msg(index);
        if (index > -1) {
            email_array.splice(index, 1);
        }

        var name_index = email_name.indexOf(name);
        //config.Msg(index);
        if (name_index > -1) {
            email_name.splice(name_index, 1);
        }

    }
//config.Msg(email_array);
//config.Msg(email_name);
    myApp.hideIndicator();
}