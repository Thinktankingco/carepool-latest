/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
var app = {
    // Application Constructor
    initialize: function () {
        this.bindEvents();
    },
    // Bind Event Listeners
    //
    // Bind any events that are required on startup. Common events are:
    // 'load', 'deviceready', 'offline', and 'online'.
    bindEvents: function () {
        document.addEventListener('deviceready', this.onDeviceReady, false);
    },
    // deviceready Event Handler
    //
    // The scope of 'this' is the event. In order to call the 'receivedEvent'
    // function, we must explicitly call 'app.receivedEvent(...);'
    onDeviceReady: function () {
        app.receivedEvent('deviceready');
    },
    // Update DOM on a Received Event
    receivedEvent: function (id) {
        var parentElement = document.getElementById(id);
        var listeningElement = parentElement.querySelector('.listening');
        var receivedElement = parentElement.querySelector('.received');

        listeningElement.setAttribute('style', 'display:none;');
        receivedElement.setAttribute('style', 'display:block;');

        console.log('Received Event: ' + id);
    }
};

document.addEventListener('deviceready', function () {

    //localStorage.removeItem('primary_key')
//    if (localStorage.getItem('primary_key') === null) {
//        myApp.showIndicator();
//
//        localStorage.setItem('start_time', new Date());
//        addnew(localStorage.getItem('start_time'));
//    }
    // 
    // Enable to debug issues.
    // window.plugins.OneSignal.setLogLevel({logLevel: 4, visualLevel: 4});
    myApp.showIndicator();
    var notificationOpenedCallback = function (jsonData) {
//        alert('didReceiveRemoteNotificationCallBack: ' + JSON.stringify(jsonData));
//        
//        if (jsonData.additionalData
//                && jsonData.additionalData.stacked_notifications) {
//            var notifications = jsonData.additionalData.stacked_notifications;
//            for (var i = 0; i < notifications.length; i++) {
//                alert(notifications[i].message);
//                console.log(notifications[i]);
//            }
//        }

        window.location = 'dashboard.html';
    };


    window.plugins.OneSignal.init("8c1378a8-a783-400e-94da-e9505801a0c1",
            {googleProjectNumber: "278418549534"},
            notificationOpenedCallback);

    window.plugins.OneSignal.getIds(function (ids) {

//              alert("UserID: " + ids.userId);
//              alert("PushToken: " + ids.pushToken);
//              alert('getIds: ' + JSON.stringify(ids));

        localStorage.setItem('player_id', ids.userId);
        myApp.hideIndicator();

    });

    // Show an alert box if a notification comes in when the user is in your app.
    window.plugins.OneSignal.enableInAppAlertNotification(true);
    window.plugins.OneSignal.enableNotificationsWhenActive(true);

    //put status bar over here
    StatusBar.styleDefault();
}, false);

//document.addEventListener("pause", onPause, false);
//
//function onPause() {
//    // Handle the pause event
//    //alert('Paused');
//    localStorage.setItem('paused_time', new Date());
//}
//document.addEventListener("resume", onResume, false);
//
//function onResume() {
//    //alert('resumed');
////    // Handle the resume event
////    alert('pause DATETIME: '+localStorage.getItem('paused_time'));
////
//    localStorage.setItem('resume_time', new Date());
////     alert('Resume DATETIME: '+localStorage.getItem('resume_time'));
////
////     alert('First time DATETIME: '+localStorage.getItem('start_time'));
//    myApp.showIndicator();
//    var GameScore = Parse.Object.extend("time_in_app");
//    var query = new GameScore();
//    query.set("objectId", localStorage.getItem('primary_key'));
//    query.set("end_time", new Date(localStorage.getItem('paused_time')));
//
//    query.save(null,
//            {
//                success: function (object) {
//                    localStorage.removeItem('primary_key')
//                    //alert('136success');
//                    myApp.hideIndicator();
//                    //  alert(object.id);
//                    addnew(localStorage.getItem('resume_time'));
//                },
//                error: function (model, error)
//                {
//                    alert('141error');
//                    alert(JSON.stringify(error));
//
//                    myApp.hideIndicator();
//                    //$(".error").show();
//                    //config.Msg("That didn’t really go to plan. We’re not sure why. Please reload CarePool.", 'Hmm, something’s wrong', 'OK');
//                }
//            });
//
//
//
//}
//
//function addnew(timekhan) {
//    //alert(141);
//    var GameScore = Parse.Object.extend("time_in_app");
//    var query = new GameScore();
//    query.set("user_id", Parse.User.current().id);
//    query.set("start_time", new Date(timekhan));
//
//    query.save(null,
//            {
//                success: function (object) {
//                    localStorage.setItem('primary_key', object.id)
//                    // alert('success');
//                    //  alert(object.id);
//                    myApp.hideIndicator();
//                },
//                error: function (model, error)
//                {
//                    //alert('error');
//                    //alert(JSON.stringify(error));
//
//                    myApp.hideIndicator();
//                    //$(".error").show();
//                    //config.Msg("That didn’t really go to plan. We’re not sure why. Please reload CarePool.", 'Hmm, something’s wrong', 'OK');
//                }
//            });
//}