
$(function () {

    ttlheight = $(window).height() - 1;
    $('.page-content').css('height', ttlheight);

    //Parse initialize
    var id = Parse.User.current().id;
//alert(id);
    if (id) {
        window.location.href = "more.html";
    }

    // Defaults to sessionStorage for storing the Facebook token

    //  Uncomment the line below to store the Facebook token in localStorage instead of sessionStorage
    //  openFB.init({appId: 'YOUR_FB_APP_ID', tokenStore: window.localStorage});

});

function login() {
    myApp.showIndicator();
    openFB.init({appId: '1035425336490081'});
    openFB.login(
            function (response) {
                if (response.status === 'connected') {

                    //alert('Facebook login succeeded, got access token: ' + response.authResponse.accessToken);
                    myApp.showIndicator();
                    localStorage.setItem('mytoken', response.authResponse.accessToken);
                    $.ajax({
                        type: "get",
                        url: 'https://graph.facebook.com/me?access_token=' + response.authResponse.accessToken + '&fields=first_name,last_name,email',
                        //data: {email:email, name:fullname, message:msg},
                        crossDomain: true,
                        dataType: "json",
                        cache: false,
                        success: function (data) {
                            
                            myApp.showIndicator();
                            var user = new Parse.User();
                            user.set("username", data.email);
                            user.set("password", '789');
                            user.set("full_name", data.first_name + ' ' + data.last_name);
                            user.set("facebook_id", data.id);
                            user.set("facebook_image", 'http://graph.facebook.com/' + data.id + '/picture?type=large');
                            user.set("player_id", localStorage.getItem('player_id'));
                            user.set("push_notes", 'Yes');
                             user.set("type", 'Client');
                            myApp.showIndicator();
                            user.signUp(null, {
                                success: function (user) {
                                  
                                    myApp.showIndicator();
                                    $.ajax({
                                        type: "post",
                                        url: 'http://www.carepool.co/carepool_emails/welcome.php',
                                        data: {email: data.email, name: data.first_name + ' ' + data.last_name},
                                        crossDomain: true,
                                        dataType: "json",
                                        cache: false,
                                        success: function (data) {
                                            // console.log();
                                           
                                            if (data == 'sent') {
                                                
                                                myApp.showIndicator();
                                                //change name in user friends 
                                                var Life = Parse.Object.extend("user_friends");
                                                var query = new Parse.Query(Life);

                                                //updaintg user friends table
                                                query.equalTo("email", $('#email').val());
                                                //query.equalTo("email", 'ahsan.dev.drc@gmail.com');
                                                query.find({
                                                    success: function (results) {
                                                       
                                                        myApp.showIndicator();
                                                        //console.log(results);
                                                        if (results.length) {
                                                            var lifeArray = [];
                                                            for (var i = 0; i < results.length; i++) {
                                                                var update_name = new Life();
                                                                var object = results[i];
                                                                update_name.set("objectId", object.id);
                                                                update_name.set("full_name", data.first_name + ' ' + data.last_name);
                                                                lifeArray.push(update_name);
                                                            }
                                                            Parse.Object.saveAll(lifeArray, {
                                                                success: function (objs) {
                                                                    //alert(91);
                                                                    // objects have been saved...
                                                                    // config.Msg('success');
                                                                    //alert('changed all the names');
                                                                    //myApp.hideIndicator();
                                                                    sum();
                                                                },
                                                                error: function (error) {
                                                                    // an error occurred...
                                                                    //config.Msg(error);
                                                                    config.Msg("That didn’t really go to plan. We’re not sure why. Please reload CarePool.",'Hmm, something’s wrong','OK');
                                                                    myApp.hideIndicator();
                                                                    return false;
                                                                }
                                                            });

                                                        }else{
                                                            sum();
                                                        }
                                                        // results is an array of AgentReleases
                                                        myApp.hideIndicator();
                                                    },
                                                    error: function (error) {
                                                        //alert("Error: " + error.code + " " + error.message);
                                                        myApp.hideIndicator();
                                                    }
                                                });
                                                //updating user_offers providers name first
                                                myApp.showIndicator();
                                                var Provider = Parse.Object.extend("user_offers");
                                                var query = new Parse.Query(Provider);
                                                query.equalTo("care_provider", $('#email').val());
                                                query.find({
                                                    success: function (results) {
                                                       
                                                        myApp.showIndicator();
                                                        //console.log(results);
                                                        if (results.length) {
                                                            var lifeArray = [];
                                                            for (var i = 0; i < results.length; i++) {
                                                                var update_name = new Provider();
                                                                var object = results[i];
                                                                update_name.set("objectId", object.id);
                                                                update_name.set("care_provider_name", data.first_name + ' ' + data.last_name);
                                                                lifeArray.push(update_name);
                                                            }
                                                            Parse.Object.saveAll(lifeArray, {
                                                                success: function (objs) {
                                                                    //alert(139);
                                                                    // objects have been saved...
                                                                    // config.Msg('success');
                                                                    //alert('changed all the providers');
                                                                    sum();
                                                                    //myApp.hideIndicator();
                                                                },
                                                                error: function (error) {
                                                                    // an error occurred...
                                                                    //config.Msg(error);
                                                                    config.Msg("That didn’t really go to plan. We’re not sure why. Please reload CarePool.",'Hmm, something’s wrong','OK');
                                                                    myApp.hideIndicator();
                                                                    return false;
                                                                }
                                                            });
                                                            //console.log(results);
                                                        }else{
                                                            sum();
                                                        }
                                                        // results is an array of AgentReleases
                                                        myApp.hideIndicator();
                                                    },
                                                    error: function (error) {
                                                        //alert("Error: " + error.code + " " + error.message);
                                                    }
                                                });
                                                myApp.showIndicator();
                                                //updating user_offers receivers name first
                                                query.equalTo("care_receiver", $('#email').val());
                                                query.find({
                                                    success: function (results) {
                                                       
                                                        //console.log(results);
                                                        myApp.showIndicator();
                                                        if (results.length) {
                                                            var lifeArray = [];
                                                            for (var i = 0; i < results.length; i++) {
                                                                var update_name = new Provider();
                                                                var object = results[i];
                                                                update_name.set("objectId", object.id);
                                                                update_name.set("care_receiver_name", data.first_name + ' ' + data.last_name);
                                                                lifeArray.push(update_name);
                                                            }
                                                            Parse.Object.saveAll(lifeArray, {
                                                                success: function (objs) {
                                                                    //alert(184);
                                                                    // objects have been saved...
                                                                    // config.Msg('success');
                                                                    //alert('changed all the providers');
                                                                    sum();
                                                                    //myApp.hideIndicator();
                                                                },
                                                                error: function (error) {
                                                                    // an error occurred...
                                                                   //config.Msg(error);
                                                                    config.Msg("That didn’t really go to plan. We’re not sure why. Please reload CarePool.",'Hmm, something’s wrong','OK');
                                                                    myApp.hideIndicator();
                                                                    return false;
                                                                }
                                                            });
                                                            // console.log(results);
                                                        }else{
                                                            sum();
                                                        }
                                                        // results is an array of AgentReleases
                                                        myApp.hideIndicator();
                                                    },
                                                    error: function (error) {
                                                       // alert("Error: " + error.code + " " + error.message);
                                                        myApp.hideIndicator();
                                                    }
                                                });

                                            } else {
                                                myApp.hideIndicator();
                                                //config.Msg("An eror occured while sending email");
                                                config.Msg("That didn’t really go to plan. We’re not sure why. Please reload CarePool.",'Hmm, something’s wrong','OK');
                                            }
                                        },
                                        error: function () {
                                            //console.log();
                                            config.Msg("That didn’t really go to plan. We’re not sure why. Please reload CarePool.",'Hmm, something’s wrong','OK');
                                        }

                                    });

                                },
                                error: function (user, error) {
                                    //alert(error.code);
                                    if (error.code !== 202) {
                                        // Show the error message somewhere and let the user try again.
                                        config.Msg("That didn’t really go to plan. We’re not sure why. Please reload CarePool.",'Hmm, something’s wrong','OK');
                                    } else {
                                        config.Msg("Ooops, that email address is already connected to a CarePool account. \n\
Recover your password on the Sign In screen or use another email address",'Hmm, something’s wrong','OK');
                                    }

                                }
                            });


                            myApp.hideIndicator();
                        },
                        error: function () {
                            //console.log();
                            config.Msg("That didn’t really go to plan. We’re not sure why. Please reload CarePool.",'Hmm, something’s wrong','OK');
                            myApp.hideIndicator();
                        }

                    });

                } else {
                    config.Msg("That didn’t really go to plan. We’re not sure why. Please reload CarePool.",'Hmm, something’s wrong','OK');
                    myApp.hideIndicator();
                }
                myApp.hideIndicator();
            }, {scope: 'email,publish_actions,public_profile,user_friends'});
}
var counter=0;
function sum() {
    ++counter;
    if (counter === 3) {
        myApp.hideIndicator();
        //config.Msg('successfully registered');
        window.location = 'dashboard.html';
    }
}