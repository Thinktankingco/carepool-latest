var off_cat = Parse.Object.extend("subscription");
$(function () {
    ttlheight = $(window).height() - 250;
    myApp.showIndicator();
    $('.page-content').css('height', ttlheight);
    myApp.hideIndicator();


//    if (Parse.User.current().get('push_notes') === undefined || Parse.User.current().get('push_notes') === 'No') {
//        $('#change_push').attr('checked', false);
//    } else {
//        $('#change_push').attr('checked', true);
//    }

    //check whether subscribed or not starts here
    myApp.showIndicator();

    var off = new Parse.Query(off_cat);
    off.equalTo("user_id", Parse.User.current().get('username'));
    off.count({
        success: function (results) {
            //alert(results);
            if (results === 0) {
                //on
                $('#change_subs').attr('checked', true);
            } else {
                //off
                $('#change_subs').attr('checked', false);
            }

            myApp.hideIndicator();
        },
        error: function (error) {
            config.Msg("That didn’t really go to plan. We’re not sure why. Please reload CarePool.", 'Hmm, something’s wrong', 'OK');
            myApp.hideIndicator();
        }
    });
    //check whether subscribed or not ends here

    $('#change_password').click(function () {
        //check if facebook user or not
        if (Parse.User.current().get('facebook_id')) {
            config.Msg("You are unable to change password as you have logged in via Facebook", 'Hmm, something’s wrong', 'OK');
            return false;

        }

        myApp.showIndicator();
        var pwd = $('#passwordchange').val();
        var oldpwd = $('#oldpassword').val();

        var id = Parse.User.current().id;
        //check if empty old password field
        if (oldpwd == '') {
            config.Msg('…and try again :)', 'Enter your old password', 'OK');
            myApp.hideIndicator();
            return false;
        }
        //check if empty new password field
        if (pwd == '') {
            config.Msg('…and try again :)', 'Enter your new password', 'OK');
            myApp.hideIndicator();
            return false;
        }

        var TestObject = Parse.Object.extend("User");
        //let make him login to check if valid password given
        Parse.User.logIn(Parse.User.current().get('username'), oldpwd, {
            // If the username and password matches
            success: function (user) {
                //config.Msg('Welcome! ' + Parse.User.current().get('username'));

                var testObject = new TestObject();
                //change password
                testObject.save({objectId: id, password: pwd},
                        {
                            success: function (object) {
                                myApp.hideIndicator();
                                config.Msg('Your password has been successfully changed. Don’t forget to engrave the new one in your memory :)', 'Password changed', 'OK');
                                log_out_now();

                                //check whether unsubscribed or not for receiving emails
//                        myApp.showIndicator();
//                        var off_cat = Parse.Object.extend("subscription");
//                        var off = new Parse.Query(off_cat);
//                        off.equalTo("user_id", Parse.User.current().get('username'));
//                        off.count({
//                            success: function (results) {
//
//                                if (results === 0) {
//                                    //now send email
//                                    $.ajax({
//                                        type: "post",
//                                        url: 'http://www.carepool.co/carepool_emails/changepass.php',
//                                        data: {email: Parse.User.current().get('username'), name: Parse.User.current().get('full_name')},
//                                        crossDomain: true,
//                                        dataType: "json",
//                                        cache: false,
//                                        success: function (data) {
//                                            // console.log();
//                                            if (data == 'sent') {
//                                                myApp.hideIndicator();
//                                                config.Msg('Your password has been successfully changed. Don’t forget to engrave the new one in your memory :)', 'Password changed', 'OK');
//                                                log_out_now();
//                                            } else {
//                                                myApp.hideIndicator();
//                                                config.Msg("That didn’t really go to plan. We’re not sure why. Please reload CarePool.", 'Hmm, something’s wrong', 'OK');
//
//                                            }
//                                        },
//                                        error: function () {
//                                            //console.log();
//                                            config.Msg("That didn’t really go to plan. We’re not sure why. Please reload CarePool.", 'Hmm, something’s wrong', 'OK');
//                                            myApp.hideIndicator();
//                                        }
//
//                                    });
//                                } else {
//                                    myApp.hideIndicator();
//                                    config.Msg('Your password has been successfully changed. Don’t forget to engrave the new one in your memory :)', 'Password changed', 'OK');
//                                    log_out_now();
//                                }
//
//                                myApp.hideIndicator();
//                            },
//                            error: function (error) {
//                                config.Msg("That didn’t really go to plan. We’re not sure why. Please reload CarePool.", 'Hmm, something’s wrong', 'OK');
//                                myApp.hideIndicator();
//                            }
//                        });
//                        //check whether unsubscribed or not ends here

                            },
                            error: function (model, error)
                            {
                                //$(".error").show();
                                //config.Msg('error');
                                config.Msg("That didn’t really go to plan. We’re not sure why. Please reload CarePool.", 'Hmm, something’s wrong', 'OK');
                                myApp.hideIndicator();
                            }
                        });

            },
            // If there is an error
            error: function (user, error) {
                //alert(error.code);
                myApp.hideIndicator();
                if (error.code !== 101) {
                    // Show the error message somewhere and let the user try again.
                    config.Msg("That didn’t really go to plan. We’re not sure why. Please reload CarePool.", 'Hmm, something’s wrong', 'OK');
                } else {
                    //config.Msg("Oops, that was the wrong email/password combination");
                    config.Msg("Ooops, that was the wrong password!", 'Try another', 'OK');

                }
            }
        });

        return false;
    });

//    $('#change_push').on('change', function () {
//        if ($(this).is(':checked')) {
//            change_push_status('Yes');
//
//        } else {
//            change_push_status('No');
//
//        }
//    });

    $('#change_subs').on('change', function () {
        if ($(this).is(':checked')) {
            change_subs_status('Delete');

        } else {
            change_subs_status('add');

        }
    });

});
function change_subs_status(value) {
    myApp.showIndicator();
    if (value === 'Delete') {

        var query = new Parse.Query('subscription');
        query.equalTo("user_id", Parse.User.current().get('username'));
        query.find().then(function (users) {

            users.forEach(function (user) {
                user.destroy({
                    success: function () {
                        myApp.hideIndicator();
                    },
                    error: function () {
                        // ERROR CODE HERE, IF YOU WANT
                        myApp.hideIndicator();
                    }
                });
            });


        }, function (error) {
            //config.Msg(error);
            myApp.hideIndicator();
            config.Msg("That didn’t really go to plan. We’re not sure why. Please reload CarePool.", 'Hmm, something’s wrong', 'OK');
            return false;
        });
    } else {

        var query = new off_cat();
        query.set("user_id", Parse.User.current().get('username'));
        query.set("status", 0);
        query.save(null,
                {
                    success: function (object) {

                        myApp.hideIndicator();
                    },
                    error: function (model, error)
                    {
                        myApp.hideIndicator();
                        //$(".error").show();
                        config.Msg("That didn’t really go to plan. We’re not sure why. Please reload CarePool.", 'Hmm, something’s wrong', 'OK');
                    }
                });
    }

}
//function change_push_status(status) {
//    myApp.showIndicator();
//    var id = Parse.User.current().id;
//    var TestObject = Parse.Object.extend("User");
//    var testObject = new TestObject();
//
//    testObject.save({objectId: id, push_notes: status},
//            {
//                success: function (object) {
//                    //alert(status);
//                    var title = '';
//                    var message = '';
//                    if (status == 'No') {
//                        title = 'Plings off!';
//                        message = "Don’t forget to check your Dashboard regularly to make sure you don’t miss any updates.";
//                    } else {
//                        title = 'Great!';
//                        message = 'We’ll let you know about important changes in events involving you.';
//                    }
//                    config.Msg(message, title, 'OK');
//                    myApp.hideIndicator();
//                },
//                error: function (model, error)
//                {
//                    //$(".error").show();
//                    // config.Msg('error');
//                    config.Msg("That didn’t really go to plan. We’re not sure why. Please reload CarePool.", 'Hmm, something’s wrong', 'OK');
//                    myApp.hideIndicator();
//                }
//            });
//}
			