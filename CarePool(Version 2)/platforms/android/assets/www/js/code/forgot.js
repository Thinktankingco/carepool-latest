
$(function () {
    Parse.User.logOut();
    //forgot
    $('#login_form').submit(function () {

        var email = $('#musername').val();
//alert(email);
        myApp.showIndicator();

        var emailIsValid = checkEmail(email);

        if (!emailIsValid)
        {
            config.Msg("Oooops, that email address does not seem to be linked to a CarePool account. Try another one?",'Have you been here before?','OK');
            myApp.hideIndicator();
            return false;
        }

        Parse.User.requestPasswordReset(email, {
            success: function () {
                
                
                config.Msg("Password reset link has been sent to " + email+'. Follow steps to reset your password.','Reset link has been sent','OK');
                myApp.hideIndicator();
                window.location = 'signin.html';
            },
            error: function (error) {
               
                config.Msg("That didn’t really go to plan. We’re not sure why. Please reload CarePool.",'Hmm, something’s wrong','OK');
                return false;
            }
        });
        return false;
    });

});
function checkEmail(email)
{
    var atpos = email.indexOf("@");
    var dotpos = email.lastIndexOf(".");

    if (atpos < 1 || dotpos < atpos + 2 || dotpos + 2 >= email.length)
    {
        return false;
    }
    return true;
}