
//code for displaying date data
Date.prototype.toLocaleFormat = Date.prototype.toLocaleFormat || function (pattern) {
    var year = this.getFullYear(), month = this.getMonth() + 1, day = this.getDate();
    if (month < 10)
        month = '0' + month;
    if (day < 10)
        day = '0' + day;
    return pattern.replace(/%Y/g, year).replace(/%m/g, month).replace(/%d/g, day);
};

var monthNames = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'
];
var d = new Date();
//d.setDate(d.getDate() - 1);
d.setHours(0, 0, 0, 0);
//alert(d);
//code for displaying date data
var tome_array = [];//for the whole data
var check_status = {};//for pending/approved/deleted
var check_viewed = {};//for pending/approved/deleted
var check_status_from_me = [];
var main_category_array = [];//for the whole data
var cats_array = [];//only for category primary key
var ttlheight = 0;
var user_offer_upcoming = []; //my sent/received offers
var user_offer_past = []; //my sent/received offers
var cn = '';
$(function () {
    // get page height
    ttlheight = $(window).height() - 160;
    $('.page-content').css('height', ttlheight);


    myApp.showIndicator();

    //get offers categories

    var off_cat = Parse.Object.extend("offer_Category");
    var off = new Parse.Query(off_cat);
    off.find({
        success: function (results) {
            main_category_array = results;
            for (var i = 0; i < results.length; i++) {
                myApp.showIndicator();
                var object = results[i];

                cats_array.push(object.id);
                myApp.hideIndicator();
            }
            //config.Msg(cats_array);
            user_offers();

            myApp.hideIndicator();
            // config.Msg(cats_array);
        },
        error: function (error) {
            myApp.hideIndicator();
            config.Msg("That didn’t really go to plan. We’re not sure why. Please reload CarePool.", 'Hmm, something’s wrong', 'OK');
        }
    });

    //setTimeout(func, 500);

});
///window.setInterval(emtpybefore, 60000);

function emtpybefore() {

    $(".mypending").hide();
    $("#pending").empty();
    $("#offers").empty();
    $("#poffers").empty();
    user_offers();
}
var suc = 0;
function futureboth_succcess() {

    suc++;

    if (suc === 2) {

        func_upcoming(user_offer_upcoming);
        myApp.hideIndicator();
        suc = 0;
    }

}
function user_offers() {

    myApp.showIndicator();
    var GameScore = Parse.Object.extend("user_offers");

    //upcomg chapter started

    //upcomg where i am care provider
    var mainQuery_provider = new Parse.Query(GameScore);
    mainQuery_provider.greaterThanOrEqualTo("event_date", d);
    mainQuery_provider.equalTo("care_provider", Parse.User.current().get('username'));
    mainQuery_provider.notEqualTo("care_provider_delete", 1);
    mainQuery_provider.find({
        success: function (results) {
            // console.log(results);
            for (var i = 0; i < results.length; i++) {
                var object = results[i];
                myApp.showIndicator();
//                alert(object.get('offer_id'));
//                alert(object.get('status'));
                check_status[object.get('offer_id')] = object.get('status');
                check_viewed[object.get('offer_id')] = object.get('viewed');


                check_status_from_me.push({
                    offer_id: object.get('offer_id'),
                    status: object.get('status')
                });


                if (object.get('care_receiver') == Parse.User.current().get('username')) {
                    //config.Msg(object.get('care_receiver'));
                    tome_array.push(object.get('offer_id'));
                }
                var name_index = user_offer_upcoming.indexOf(object.get('offer_id'));
                //config.Msg(index);
                if (name_index > -1) {
                    user_offer_upcoming.splice(name_index, 1);
                }
                user_offer_upcoming.push(object.get('offer_id'));
                myApp.hideIndicator();
            }
            futureboth_succcess();
        },
        error: function (error) {
            myApp.hideIndicator();
            config.Msg("That didn’t really go to plan. We’re not sure why. Please reload CarePool.", 'Hmm, something’s wrong', 'OK');
        }
    });

    //upcomg where i am care receiver
    var mainQuery_provider = new Parse.Query(GameScore);
    mainQuery_provider.greaterThanOrEqualTo("event_date", d);
    mainQuery_provider.equalTo("care_receiver", Parse.User.current().get('username'));
    mainQuery_provider.notEqualTo("care_receiver_delete", 1);
    mainQuery_provider.find({
        success: function (results) {
            // console.log(results);
            for (var i = 0; i < results.length; i++) {
                var object = results[i];
                myApp.showIndicator();
//                alert(object.get('offer_id'));
//                alert(object.get('status'));
                check_status[object.get('offer_id')] = object.get('status');
                check_viewed[object.get('offer_id')] = object.get('viewed');


                check_status_from_me.push({
                    offer_id: object.get('offer_id'),
                    status: object.get('status')
                });


                if (object.get('care_receiver') == Parse.User.current().get('username')) {
                    //config.Msg(object.get('care_receiver'));
                    tome_array.push(object.get('offer_id'));
                }
                var name_index = user_offer_upcoming.indexOf(object.get('offer_id'));
                //config.Msg(index);
                if (name_index > -1) {
                    user_offer_upcoming.splice(name_index, 1);
                }
                user_offer_upcoming.push(object.get('offer_id'));
                myApp.hideIndicator();
            }
            futureboth_succcess();
        },
        error: function (error) {
            myApp.hideIndicator();
            config.Msg("That didn’t really go to plan. We’re not sure why. Please reload CarePool.", 'Hmm, something’s wrong', 'OK');
        }
    });

    //past chapter started

    myApp.showIndicator();
    //past offer/requests where i am care provider
    var past_provider = new Parse.Query(GameScore);
    past_provider.lessThanOrEqualTo("event_date", d);
    past_provider.equalTo("care_provider", Parse.User.current().get('username'));
    past_provider.notEqualTo("care_provider_delete", 1);
    past_provider.find({
        success: function (results) {
            //console.log(results);
            // alert(results.length);
            for (var i = 0; i < results.length; i++) {
                var object = results[i];
                myApp.showIndicator();

                check_status[object.get('offer_id')] = object.get('status');
                check_viewed[object.get('offer_id')] = object.get('viewed');

                var name_index = user_offer_past.indexOf(object.get('offer_id'));
                //config.Msg(index);
                if (name_index > -1) {
                    user_offer_past.splice(name_index, 1);
                }
                check_status_from_me.push({
                    offer_id: object.get('offer_id'),
                    status: object.get('status')
                });
                user_offer_past.push(object.get('offer_id'));
                myApp.hideIndicator();

            }
            pastboth_succcess();
        },
        error: function (error) {
            myApp.hideIndicator();
            config.Msg("That didn’t really go to plan. We’re not sure why. Please reload CarePool.", 'Hmm, something’s wrong', 'OK');
        }
    });

    myApp.showIndicator();
    //past offer/requests where i am care receiver
    var past_receiver = new Parse.Query(GameScore);
    past_receiver.lessThanOrEqualTo("event_date", d);
    past_receiver.equalTo("care_receiver", Parse.User.current().get('username'));
    past_receiver.notEqualTo("care_receiver_delete", 1);
    past_receiver.find({
        success: function (results) {
            //console.log(results);
//            alert(results.length);
            for (var i = 0; i < results.length; i++) {
                var object = results[i];
                myApp.showIndicator();

                check_status[object.get('offer_id')] = object.get('status');
                check_viewed[object.get('offer_id')] = object.get('viewed');

                var name_index = user_offer_past.indexOf(object.get('offer_id'));
                //config.Msg(index);
                if (name_index > -1) {
                    user_offer_past.splice(name_index, 1);
                }
                check_status_from_me.push({
                    offer_id: object.get('offer_id'),
                    status: object.get('status')
                });
                user_offer_past.push(object.get('offer_id'));
                myApp.hideIndicator();

            }
            pastboth_succcess();

        },
        error: function (error) {
            myApp.hideIndicator();
            config.Msg("That didn’t really go to plan. We’re not sure why. Please reload CarePool.", 'Hmm, something’s wrong', 'OK');
        }
    });

}
var succc = 0;
function pastboth_succcess() {
    succc++;
    if (succc === 2) {
        func_past(user_offer_past);
        myApp.hideIndicator();
        succc = 0;
    }

}

function func_upcoming(offer_ids) {
    // alert(offer_ids);
    // alert(262);
    //get all offers
    myApp.showIndicator();
    var GameScore = Parse.Object.extend("offers");
    if (Parse.User.current()) {

        //upcoming offers
        var query = new Parse.Query(GameScore);

        //var todaysDate = new Date(d.getFullYear()); 

        query.containedIn("objectId", offer_ids);
        query.equalTo("sent", 1);
        //query.notEqualTo("deleted", 1);
        query.ascending("date");
        query.find({
            success: function (results) {

                $("#offers").empty();

                var newd = new Date().toString().split(" ");

                newd = newd[4];

                var c1 = '#FFF';
                var c2 = '#eee';

                for (var i = 0; i < results.length; i++) {


                    myApp.showIndicator();

                    //alert(i);
                    var object = results[i];

                    if (check_viewed[object.id] || check_status[object.id] == 'Declined') {

                        var show_image = '';
                    } else {
                       
                        var show_image = '<img src="img/icon-pink-32.png" width="16" style="float:right;" />';
                    }

                    if (object.get('creator_viewed')) {

                        var show_image_for_creator = '';
                    } else {

                        var show_image_for_creator = '<img src="img/icon-pink-32.png" width="16" style="float:right;" />';
                    }


                    var today = new Date(object.get('date'));
                    var mydate = today.toLocaleFormat('%m-%d-%Y');

                    var date = new Date();

                    // config.Msg(monthNames[mydate[0]]);
                    mydate = mydate.split('-');
                    //config.Msg(mydate[0]);
                    //config.Msg(monthNames[parseInt(mydate[0])-1]);
                    mydate = mydate[1] + ' ' + monthNames[parseInt(mydate[0]) - 1] + ' ' + mydate[2];
                    // config.Msg(mydate);

                    var magic = '';
                    var page = '';
                    var classi = '';
                    var icon_image = '';
                    var img_dislay = '';
                    var name2_index = tome_array.indexOf(object.id);
                    //config.Msg(name2_index);
                    if (name2_index > -1) {
                        page = 'offerdetail';
                        icon_image = 'info.png';

                        classi = 'body_orange';
                        magic = '#fcaf1b';
                        img_display = main_category_array[cats_array.indexOf(object.get('offer_Category_id'))].get('color_image').url();
                    } else {
                        page = 'offerdetail';
                        icon_image = 'info.png';

                        magic = '#40CBF4';
                        classi = 'body_blue';
                        img_display = main_category_array[cats_array.indexOf(object.get('offer_Category_id'))].get('color_image1').url();

                    }
                    var time1 = object.get('start_time').split(':');
                    if (time1[0] > 12) {
                        var time11 = time1[0] - 12;

//                        if (time11.toString().length === 1) {
//                            time11 = '0' + time11;
//                        }
                        time11 = time11 + ':' + time1[1] + ' PM';
                    } else {
                        time11 = object.get('start_time') + ' AM';
                    }

                    var time2 = object.get('end_time').split(':');
                    if (time2[0] >= 12) {
                        if (time2[0] > 12) {
                            var time12 = time2[0] - 12;
                        } else {
                            var time12 = time2[0];
                        }
//                        if (time12.toString().length === 1) {
//                            time12 = '0' + time12;
//                        }
                        time12 = time12 + ':' + time2[1] + ' PM';
                    } else {
                        time12 = object.get('end_time') + ' AM';
                    }

                    var from = '';
                    var message = '';
                    if (magic === '#40CBF4') {
                        from = 'Care Recipient:  ';
                    } else {
                        from = 'Care Provider: ';
                    }
                    if (object.get('deleted') === 1) {
                       
                        if (object.get('created_by') === Parse.User.current().id) {
                            show_image = '';
                        }

                        if ($('ul#poffers li').length % 2 === 0) {
                            cn = c1;
                        } else {
                            cn = c2;
                        }
                        $('#poffers').append('<li id="past_' + object.id + '" class="swipeout" style="background-color: #eee"><div class="item-content swipeout-content"><div class="item-media">\n\
                                <a class="external" href="' + page + '.html?id=' + object.id + '&color=' + classi + '&type=past"><img width="60" height="60" src="' + img_display + '"></a></div><div class="item-inner" style="bottom:1px;">\n\
<div class="" style="width:100%">\n\
        <a class="external" href="' + page + '.html?id=' + object.id + '&color=' + classi + '&type=past"><strong><span id="text_new"  style="color:#8E8E93" class="default_sub_font">' + object.get('title') + '</span></strong> <br>\n\
<span id="el_' + object.id + '"  class="default_sub_font" style="color:#8E8E93">' + mydate + ', ' + time11 + '-' + time12 + '<br/>' + from + '</span></a></div>' + show_image + '</div></div><div class="sortable-handler"></div>\n\
<div class="swipeout-actions-right"><a onclick="killme(' + "'" + object.id + "'" + ')" style="background-color:red;color:white;"><img src="img/delete.png" width="30" /></a></div></div></li>');

                    } else {
                        //offer/request is created by me
                        if (object.get('created_by') === Parse.User.current().id) {


                            message = 'from';
                            //if my event is accepted by some 1 in offers table
                            if (object.get('status') === 'Accepted') {

                                //if event date is today
                                if (today.setHours(0, 0, 0, 0) === date.setHours(0, 0, 0, 0)) {
                                    //if event end time is passed
                                    if (newd > object.get('end_time')) {

                                        if ($('ul#poffers li').length % 2 === 0) {
                                            cn = c1;
                                        } else {
                                            cn = c2;
                                        }
                                        $('#poffers').append('<li id="past_' + object.id + '" class="swipeout" style="background-color: #eee"><div class="item-content swipeout-content"><div class="item-media">\n\
                                <a class="external" href="' + page + '.html?id=' + object.id + '&color=' + classi + '&type=past"><img width="60" height="60" src="' + img_display + '"></a></div><div class="item-inner" style="bottom:1px;">\n\
<div class="" style="width:100%">\n\
        <a class="external" href="' + page + '.html?id=' + object.id + '&color=' + classi + '&type=past"><strong><span id="text_new"  style="color:' + magic + '" class="default_sub_font">' + object.get('title') + '</span></strong> <br>\n\
<span id="el_' + object.id + '"  class="default_sub_font" style="color:' + magic + '">' + mydate + ', ' + time11 + '-' + time12 + '<br/>' + from + '</span></a></div>' + show_image + '</div></div><div class="sortable-handler"></div>\n\
<div class="swipeout-actions-right"><a onclick="killme(' + "'" + object.id + "'" + ')" style="background-color:red;color:white;"><img src="img/delete.png" width="30" /></a></div></div></li>');

                                    } else {
                                        //event date is today but end time is not passsed
                                        if ($('ul#offers li').length % 2 === 0) {
                                            cn = c1;
                                        } else {
                                            cn = c2;
                                        }
                                        $('#offers').append('<li style="background-color: ' + cn + ';"><a class="external" href="offerdetail.html?id=' + object.id + '&color=' + classi + '"><div class="item-content"><div class="item-media">\n\
                                <img width="60" height="60" src="' + img_display + '"></div><div class="item-inner"> <div class="" style="width:100%;">\n\
        <strong><span id="text_new" style="color:' + magic + ';" class="default_sub_font">&nbsp;' + object.get('title') + '</span></strong> <br> \n\
<span id="el_' + object.id + '" class="default_sub_font" style="color:' + magic + '">&nbsp;' + mydate + ', ' + time11 + '-' + time12 + '<br/>' + from + '</span></div>' + show_image_for_creator + '</div></div><div class="sortable-handler"></div></a></li>');

                                    }
                                } else {
                                    //event is not today but in upcoming dates
                                    if ($('ul#offers li').length % 2 === 0) {
                                        cn = c1;
                                    } else {
                                        cn = c2;
                                    }
                                    $('#offers').append('<li style="background-color: ' + cn + ';"><a class="external" href="offerdetail.html?id=' + object.id + '&color=' + classi + '"><div class="item-content"><div class="item-media">\n\
                                <img width="60" height="60" src="' + img_display + '"></div><div class="item-inner"> <div class="" style="width:100%;">\n\
        <strong><span id="text_new" style="color:' + magic + ';" class="default_sub_font">&nbsp;' + object.get('title') + '</span></strong> <br> \n\
<span id="el_' + object.id + '" class="default_sub_font" style="color:' + magic + '">&nbsp;' + mydate + ', ' + time11 + '-' + time12 + '<br/>' + from + '</span></div>' + show_image_for_creator + '</div></div><div class="sortable-handler"></div></a></li>');

                                }
                            } else {

                                //event is not accepted in offers table flag
                                //if event date is today
                                if (today.setHours(0, 0, 0, 0) === date.setHours(0, 0, 0, 0)) {

                                    //if event end time is passed
                                    if (newd > object.get('end_time')) {

                                        if ($('ul#poffers li').length % 2 === 0) {
                                            cn = c1;
                                        } else {
                                            cn = c2;
                                        }
                                        $('#poffers').append('<li id="past_' + object.id + '" class="swipeout" style="background-color: #eee"><div class="item-content swipeout-content"><div class="item-media">\n\
                                <a class="external" href="' + page + '.html?id=' + object.id + '&color=' + classi + '&type=past"><img width="60" height="60" src="' + img_display + '"></a></div><div class="item-inner" style="bottom:1px;">\n\
<div class="" style="width:100%">\n\
        <a class="external" href="' + page + '.html?id=' + object.id + '&color=' + classi + '&type=past"><strong><span id="text_new"  style="color:#8E8E93;" class="default_sub_font">' + object.get('title') + '</span></strong> <br>\n\
<span id="el_' + object.id + '"  class="default_sub_font" style="color:#8E8E93;">' + mydate + ', ' + time11 + '-' + time12 + '<br/>' + from + '</span></a></div></div></div><div class="sortable-handler"></div>\n\
<div class="swipeout-actions-right"><a onclick="killme(' + "'" + object.id + "'" + ')" style="background-color:red;color:white;"><img src="img/delete.png" width="30" /></a></div></div></li>');

                                    } else {

                                        //event is coming in upcoming hours

                                        //get all the status against event
                                        var find_status = $.grep(check_status_from_me, function (e) {
                                            return e.offer_id == object.id;
                                        });

                                        //count deleted records against event 
                                        var numBoys = find_status.reduce(function (n, person) {
                                            return n + (person.status == 'Declined');
                                        }, 0);

                                        //find the occurence of Declined 

                                        //if not declined by all
                                        if (numBoys !== find_status.length) {

                                            //as not declined by all
                                            //find the occurrence of accepted
                                            var occurances2 = find_status.reduce(function (n, person) {
                                                return n + (person.status === 'Accepted');
                                            }, 0);
                                            //if accepted by any1
                                            if (occurances2 >= 1) {

                                                if ($('ul#offers li').length % 2 === 0) {
                                                    cn = c1;
                                                } else {
                                                    cn = c2;
                                                }
                                                $('#offers').append('<li style="background-color: ' + cn + ';"><a class="external" href="offerdetail.html?id=' + object.id + '&color=' + classi + '"><div class="item-content"><div class="item-media">\n\
                                <img width="60" height="60" src="' + img_display + '"></div><div class="item-inner"> <div class="" style="width:100%;">\n\
        <strong><span id="text_new" style="color:' + magic + ';" class="default_sub_font">&nbsp;' + object.get('title') + '</span></strong> <br> \n\
<span id="el_' + object.id + '" class="default_sub_font" style="color:' + magic + '">&nbsp;' + mydate + ', ' + time11 + '-' + time12 + '<br/>' + from + '</span></div>' + show_image_for_creator + '</div></div><div class="sortable-handler"></div></a></li>');

                                            } else {
                                                //neither declined not accepted event status
                                                $('.mypending').show();
                                                if ($('ul#pending li').length % 2 === 0) {
                                                    cn = c1;
                                                } else {
                                                    cn = c2;
                                                }
                                                $('#pending').append('<li style="background-color: ' + cn + ';"><a class="external" href="offerdetail.html?id=' + object.id + '&color=' + classi + '"><div class="item-content"><div class="item-media">\n\
                                <img width="60" height="60" src="' + img_display + '"></div><div class="item-inner"> <div class="" style="width:100%;">\n\
        <strong><span id="text_new" style="color:' + magic + ';" class="default_sub_font">&nbsp;' + object.get('title') + '</span></strong> <br> \n\
<span id="el_' + object.id + '" class="default_sub_font" style="color:' + magic + '">&nbsp;' + mydate + ', ' + time11 + '-' + time12 + '<br/>' + from + '</span></div>' + show_image_for_creator + '</div></div><div class="sortable-handler"></div></a></li>');

                                            }
                                        } else {
//                                            alert(show_image);
//                                            alert(537);
                                            //all of the recepients have declined the event

                                            if ($('ul#poffers li').length % 2 === 0) {
                                                cn = c1;
                                            } else {
                                                cn = c2;
                                            }
                                            $('#poffers').append('<li id="past_' + object.id + '" class="swipeout" style="background-color: #eee"><div class="item-content swipeout-content"><div class="item-media">\n\
                                <a class="external" href="' + page + '.html?id=' + object.id + '&color=' + classi + '&type=past"><img width="60" height="60" src="' + img_display + '"></a></div><div class="item-inner" style="bottom:1px;">\n\
<div class="" style="width:100%">\n\
        <a class="external" href="' + page + '.html?id=' + object.id + '&color=' + classi + '&type=past"><strong><span id="text_new"  style="color:#8E8E93" class="default_sub_font">' + object.get('title') + '</span></strong> <br>\n\
<span id="el_' + object.id + '"  class="default_sub_font" style="color:#8E8E93;">' + mydate + ', ' + time11 + '-' + time12 + '<br/>' + from + '</span></a></div>' + show_image_for_creator + '</div></div><div class="sortable-handler"></div>\n\
<div class="swipeout-actions-right"><a onclick="killme(' + "'" + object.id + "'" + ')" style="background-color:red;color:white;"><img src="img/delete.png" width="30" /></a></div></div></li>');

                                        }
                                    }
                                } else {
                                    //event date is in upcoming days

                                    //get all the status against event
                                    var find_status = $.grep(check_status_from_me, function (e) {
                                        return e.offer_id == object.id;
                                    });

                                    //count deleted records against offer 
                                    var numBoys = find_status.reduce(function (n, person) {
                                        return n + (person.status == 'Declined');
                                    }, 0);

                                    //find the occurence of Declined 

                                    //if not declined by all
                                    if (numBoys !== find_status.length) {
                                        //find the occurrence of accepted
                                        var occurances2 = find_status.reduce(function (n, person) {
                                            return n + (person.status == 'Accepted');
                                        }, 0);
                                        //as not declined by all, find if any 1 has accepted
                                        if (occurances2 >= 1) {
                                            if ($('ul#offers li').length % 2 === 0) {
                                                cn = c1;
                                            } else {
                                                cn = c2;
                                            }
                                            $('#offers').append('<li style="background-color: ' + cn + ';"><a class="external" href="offerdetail.html?id=' + object.id + '&color=' + classi + '"><div class="item-content"><div class="item-media">\n\
                                <img width="60" height="60" src="' + img_display + '"></div><div class="item-inner"> <div class="" style="width:100%;">\n\
        <strong><span id="text_new" style="color:' + magic + ';" class="default_sub_font">&nbsp;' + object.get('title') + '</span></strong> <br> \n\
<span id="el_' + object.id + '" class="default_sub_font" style="color:' + magic + '">&nbsp;' + mydate + ', ' + time11 + '-' + time12 + '<br/>' + from + '</span></div>' + show_image_for_creator + '</div></div><div class="sortable-handler"></div></a></li>');

                                        } else {
                                            //neither accepted nor declined by any1
                                            $('.mypending').show();
                                            if ($('ul#pending li').length % 2 === 0) {
                                                cn = c1;
                                            } else {
                                                cn = c2;
                                            }
                                            $('#pending').append('<li style="background-color: ' + cn + ';"><a class="external" href="offerdetail.html?id=' + object.id + '&color=' + classi + '"><div class="item-content"><div class="item-media">\n\
                                <img width="60" height="60" src="' + img_display + '"></div><div class="item-inner"> <div class="" style="width:100%;">\n\
        <strong><span id="text_new" style="color:' + magic + ';" class="default_sub_font">&nbsp;' + object.get('title') + '</span></strong> <br> \n\
<span id="el_' + object.id + '" class="default_sub_font" style="color:' + magic + '">&nbsp;' + mydate + ', ' + time11 + '-' + time12 + '<br/>' + from + '</span></div>' + show_image_for_creator + '</div></div><div class="sortable-handler"></div></a></li>');

                                        }
                                    } else {
                                        //event is declined by all
                                        //display me until i had seen it 391
                                        if ($('ul#poffers li').length % 2 === 0) {
                                            cn = c1;
                                        } else {
                                            cn = c2;
                                        }
                                        $('#poffers').append('<li id="past_' + object.id + '" class="swipeout" style="background-color: #eee"><div class="item-content swipeout-content"><div class="item-media">\n\
                                <a class="external" href="' + page + '.html?id=' + object.id + '&color=' + classi + '&type=past"><img width="60" height="60" src="' + img_display + '"></a></div><div class="item-inner" style="bottom:1px;">\n\
<div class="" style="width:100%">\n\
        <a class="external" href="' + page + '.html?id=' + object.id + '&color=' + classi + '&type=past"><strong><span id="text_new"  style="color:#8E8E93" class="default_sub_font">' + object.get('title') + '</span></strong> <br>\n\
<span id="el_' + object.id + '"  class="default_sub_font" style="color:#8E8E93">' + mydate + ', ' + time11 + '-' + time12 + '<br/>' + from + '</span></a></div>' + show_image + '</div></div><div class="sortable-handler"></div>\n\
<div class="swipeout-actions-right"><a onclick="killme(' + "'" + object.id + "'" + ')" style="background-color:red;color:white;"><img src="img/delete.png" width="30" /></a></div></div></li>');

                                    }
                                }
                            }
                        } else {

                            message = 'to';
                            //event is created by some1 else
                            //requested to me
                            //if event is accepted in offers table flag
                            if (object.get('status') === 'Accepted') {

                                //if event is acccepted by me
                                if (object.get('accepted_by') === Parse.User.current().id) {

                                    //if event date is today
                                    if (today.setHours(0, 0, 0, 0) === date.setHours(0, 0, 0, 0)) {
                                        //if event end time is passed
                                        if (newd > object.get('end_time')) {

                                            if ($('ul#poffers li').length % 2 === 0) {
                                                cn = c1;
                                            } else {
                                                cn = c2;
                                            }
                                            $('#poffers').append('<li id="past_' + object.id + '" class="swipeout" style="background-color: #eee"><div class="item-content swipeout-content"><div class="item-media">\n\
                                <a class="external" href="' + page + '.html?id=' + object.id + '&color=' + classi + '&type=past"><img width="60" height="60" src="' + img_display + '"></a></div><div class="item-inner" style="bottom:1px;">\n\
<div class="" style="width:100%">\n\
        <a class="external" href="' + page + '.html?id=' + object.id + '&color=' + classi + '&type=past"><strong><span id="text_new"  style="color:' + magic + '" class="default_sub_font">' + object.get('title') + '</span></strong> <br>\n\
<span id="el_' + object.id + '"  class="default_sub_font" style="color:' + magic + '">' + mydate + ', ' + time11 + '-' + time12 + '<br/>' + from + '</span></a></div>' + show_image + '</div></div><div class="sortable-handler"></div>\n\
<div class="swipeout-actions-right"><a onclick="killme(' + "'" + object.id + "'" + ')" style="background-color:red;color:white;"><img src="img/delete.png" width="30" /></a></div></div></li>');

                                        } else {
                                            //event is coming in next hours
                                            if ($('ul#offers li').length % 2 === 0) {
                                                cn = c1;
                                            } else {
                                                cn = c2;
                                            }
                                            $('#offers').append('<li style="background-color: ' + cn + ';"><a class="external" href="offerdetail.html?id=' + object.id + '&color=' + classi + '"><div class="item-content"><div class="item-media">\n\
                                <img width="60" height="60" src="' + img_display + '"></div><div class="item-inner"> <div class="" style="width:100%;">\n\
        <strong><span id="text_new" style="color:' + magic + ';" class="default_sub_font">&nbsp;' + object.get('title') + '</span></strong> <br> \n\
<span id="el_' + object.id + '" class="default_sub_font" style="color:' + magic + '">&nbsp;' + mydate + ', ' + time11 + '-' + time12 + '<br/>' + from + '</span></div>' + show_image + '</div></div><div class="sortable-handler"></div></a></li>');

                                        }
                                    } else {
                                        
                                        //event is coming in next days
                                        if ($('ul#offers li').length % 2 === 0) {
                                            cn = c1;
                                        } else {
                                            cn = c2;
                                        }
                                        $('#offers').append('<li style="background-color: ' + cn + ';"><a class="external" href="offerdetail.html?id=' + object.id + '&color=' + classi + '"><div class="item-content"><div class="item-media">\n\
                                <img width="60" height="60" src="' + img_display + '"></div><div class="item-inner"> <div class="" style="width:100%;">\n\
        <strong><span id="text_new" style="color:' + magic + ';" class="default_sub_font">&nbsp;' + object.get('title') + '</span></strong> <br> \n\
<span id="el_' + object.id + '" class="default_sub_font" style="color:' + magic + '">&nbsp;' + mydate + ', ' + time11 + '-' + time12 + '<br/>' + from + '</span></div>' + show_image + '</div></div><div class="sortable-handler"></div></a></li>');

                                    }
                                }
                                //event is accepted by some 1 else
                                //added for offer things starts here
                                else {
                                    
                                    //exception for offer as it can be accepted by multiple recepients
                                    if (object.get('category') === 'offer') {
                                        //do code here
                                        //if i hadn't declined it
                                        if (check_status[object.id] !== 'Declined') {
                                            //if offer date is today
                                            if (today.setHours(0, 0, 0, 0) === date.setHours(0, 0, 0, 0)) {
                                                //if offer end time is passed
                                                if (newd > object.get('end_time')) {

                                                    if ($('ul#poffers li').length % 2 === 0) {
                                                        cn = c1;
                                                    } else {
                                                        cn = c2;
                                                    }
                                                    $('#poffers').append('<li id="past_' + object.id + '" class="swipeout" style="background-color: #eee"><div class="item-content swipeout-content"><div class="item-media">\n\
                                <a class="external" href="' + page + '.html?id=' + object.id + '&color=' + classi + '&type=past"><img width="60" height="60" src="' + img_display + '"></a></div><div class="item-inner" style="bottom:1px;">\n\
<div class="" style="width:100%">\n\
        <a class="external" href="' + page + '.html?id=' + object.id + '&color=' + classi + '&type=past"><strong><span id="text_new"  style="color:#8E8E93;" class="default_sub_font">' + object.get('title') + '</span></strong> <br>\n\
<span id="el_' + object.id + '"  class="default_sub_font" style="color:#8E8E93;">' + mydate + ', ' + time11 + '-' + time12 + '<br/>' + from + '</span></a></div>' + show_image + '</div></div><div class="sortable-handler"></div>\n\
<div class="swipeout-actions-right"><a onclick="killme(' + "'" + object.id + "'" + ')" style="background-color:red;color:white;"><img src="img/delete.png" width="30" /></a></div></div></li>');

                                                } else {
                                                  
                                                    //15 june logic - if accepted show in offers else in pending
                                                    if (check_status[object.id] === 'Accepted') {
                                                      
                                                        if ($('ul#offers li').length % 2 === 0) {
                                                            cn = c1;
                                                        } else {
                                                            cn = c2;
                                                        }
                                                        $('#offers').append('<li style="background-color: ' + cn + ';"><a class="external" href="offerdetail.html?id=' + object.id + '&color=' + classi + '"><div class="item-content"><div class="item-media">\n\
                                <img width="60" height="60" src="' + img_display + '"></div><div class="item-inner"> <div class="" style="width:100%;">\n\
        <strong><span id="text_new" style="color:' + magic + ';" class="default_sub_font">&nbsp;' + object.get('title') + '</span></strong> <br> \n\
<span id="el_' + object.id + '" class="default_sub_font" style="color:' + magic + '">&nbsp;' + mydate + ', ' + time11 + '-' + time12 + '<br/>' + from + '</span></div>' + show_image + '</div></div><div class="sortable-handler"></div></a></li>');

                                                    } else {
                                                        $('.mypending').show();
                                                        if ($('ul#pending li').length % 2 === 0) {
                                                            cn = c1;
                                                        } else {
                                                            cn = c2;
                                                        }
                                                        $('#pending').append('<li style="background-color: ' + cn + ';"><a class="external" href="offerdetail.html?id=' + object.id + '&color=' + classi + '">\n\
<div class="item-content"><div class="item-media"><img width="60" height="60" src="' + img_display + '"></div><div class="item-inner"> <div class="" style="width:100%;">\n\
' + show_image + '<strong><span id="text_new" style="color:' + magic + ';" class="default_sub_font">&nbsp;' + object.get('title') + '</span></strong> <br> \n\
<span id="el_' + object.id + '" class="default_sub_font" style="color:' + magic + '">&nbsp;' + mydate + ', ' + time11 + '-' + time12 + '<br/>' + from + '</span>\n\
</div></div></div><div class="sortable-handler"></div></a></li>');

                                                    }
                                                }
                                            } else {
                                                
                                                //15 june logic - if accepted show in offers else in pending
                                                if (check_status[object.id] === 'Accepted') {
                                                   
                                                    if ($('ul#offers li').length % 2 === 0) {
                                                        cn = c1;
                                                    } else {
                                                        cn = c2;
                                                    }
                                                    $('#offers').append('<li style="background-color: ' + cn + ';"><a class="external" href="offerdetail.html?id=' + object.id + '&color=' + classi + '"><div class="item-content"><div class="item-media">\n\
                                <img width="60" height="60" src="' + img_display + '"></div><div class="item-inner"> <div class="" style="width:100%;">\n\
        <strong><span id="text_new" style="color:' + magic + ';" class="default_sub_font">&nbsp;' + object.get('title') + '</span></strong> <br> \n\
<span id="el_' + object.id + '" class="default_sub_font" style="color:' + magic + '">&nbsp;' + mydate + ', ' + time11 + '-' + time12 + '<br/>' + from + '</span></div>' + show_image + '</div></div><div class="sortable-handler"></div></a></li>');

                                                } else {
                                                    $('.mypending').show();
                                                    if ($('ul#pending li').length % 2 === 0) {
                                                        cn = c1;
                                                    } else {
                                                        cn = c2;
                                                    }
                                                    $('#pending').append('<li style="background-color: ' + cn + ';"><a class="external" href="offerdetail.html?id=' + object.id + '&color=' + classi + '">\n\
<div class="item-content"><div class="item-media"><img width="60" height="60" src="' + img_display + '"></div><div class="item-inner"> <div class="" style="width:100%;">\n\
' + show_image + '<strong><span id="text_new" style="color:' + magic + ';" class="default_sub_font">&nbsp;' + object.get('title') + '</span></strong> <br> \n\
<span id="el_' + object.id + '" class="default_sub_font" style="color:' + magic + '">&nbsp;' + mydate + ', ' + time11 + '-' + time12 + '<br/>' + from + '</span>\n\
</div></div></div><div class="sortable-handler"></div></a></li>');

                                                }
                                            }
                                        }
                                        //i had declined it
                                        else {
                                            if ($('ul#poffers li').length % 2 === 0) {
                                                cn = c1;
                                            } else {
                                                cn = c2;
                                            }
                                            $('#poffers').append('<li id="past_' + object.id + '" class="swipeout" style="background-color: #eee"><div class="item-content swipeout-content"><div class="item-media">\n\
                                <a class="external" href="' + page + '.html?id=' + object.id + '&color=' + classi + '&type=past"><img width="60" height="60" src="' + img_display + '"></a></div><div class="item-inner" style="bottom:1px;">\n\
<div class="" style="width:100%">\n\
        <a class="external" href="' + page + '.html?id=' + object.id + '&color=' + classi + '&type=past"><strong><span id="text_new"  style="color:#8E8E93;" class="default_sub_font">' + object.get('title') + '</span></strong> <br>\n\
<span id="el_' + object.id + '"  class="default_sub_font" style="color:#8E8E93;">' + mydate + ', ' + time11 + '-' + time12 + '<br/>' + from + '</span></a></div>' + show_image + '</div></div><div class="sortable-handler"></div>\n\
<div class="swipeout-actions-right"><a onclick="killme(' + "'" + object.id + "'" + ')" style="background-color:red;color:white;"><img src="img/delete.png" width="30" /></a></div></div></li>');

                                        }
                                    } else {
                                        if ($('ul#poffers li').length % 2 === 0) {
                                            cn = c1;
                                        } else {
                                            cn = c2;
                                        }
                                        //means it will be request then show it in past for me but the question arises will the past will be clickable or not???
                                        $('#poffers').append('<li id="past_' + object.id + '" class="swipeout" style="background-color: #eee"><div class="item-content swipeout-content"><div class="item-media">\n\
                                <a class="external" href="' + page + '.html?id=' + object.id + '&color=' + classi + '&type=past"><img width="60" height="60" src="' + img_display + '"></a></div><div class="item-inner" style="bottom:1px;">\n\
<div class="" style="width:100%">\n\
        <a class="external" href="' + page + '.html?id=' + object.id + '&color=' + classi + '&type=past"><strong><span id="text_new"  style="color:#8E8E93" class="default_sub_font">' + object.get('title') + '</span></strong> <br>\n\
<span id="el_' + object.id + '"  class="default_sub_font" style="color:#8E8E93">' + mydate + ', ' + time11 + '-' + time12 + '<br/>' + from + '</span></a></div>' + show_image + '</div></div><div class="sortable-handler"></div>\n\
<div class="swipeout-actions-right"><a onclick="killme(' + "'" + object.id + "'" + ')" style="background-color:red;color:white;"><img src="img/delete.png" width="30" /></a></div></div></li>');

                                    }
                                }

                                //added for offer thingss ends here
                            } else {
 
                                //if i had not declined
                                if (check_status[object.id] !== 'Declined') {

                                    //if event date is today
                                    if (today.setHours(0, 0, 0, 0) === date.setHours(0, 0, 0, 0)) {
                                        //if event end time is passed
                                        if (newd > object.get('end_time')) {

                                            $('#poffers').append('<li id="past_' + object.id + '" class="swipeout" style="background-color: #eee"><div class="item-content swipeout-content"><div class="item-media">\n\
                                <a class="external" href="' + page + '.html?id=' + object.id + '&color=' + classi + '&type=past"><img width="60" height="60" src="' + img_display + '"></a></div><div class="item-inner" style="bottom:1px;">\n\
<div class="" style="width:100%">\n\
        <a class="external" href="' + page + '.html?id=' + object.id + '&color=' + classi + '&type=past"><strong><span id="text_new"  style="color:#8E8E93;" class="default_sub_font">' + object.get('title') + '</span></strong> <br>\n\
<span id="el_' + object.id + '"  class="default_sub_font" style="color:#8E8E93;">' + mydate + ', ' + time11 + '-' + time12 + '<br/>' + from + '</span></a></div>' + show_image + '</div></div><div class="sortable-handler"></div>\n\
<div class="swipeout-actions-right"><a onclick="killme(' + "'" + object.id + "'" + ')" style="background-color:red;color:white;"><img src="img/delete.png" width="30" /></a></div></div></li>');

                                        } else {
                                            if (check_status[object.id] === 'Accepted') {
                                                if ($('ul#offers li').length % 2 === 0) {
                                                    cn = c1;
                                                } else {
                                                    cn = c2;
                                                }
                                                $('#offers').append('<li style="background-color: ' + cn + ';"><a class="external" href="offerdetail.html?id=' + object.id + '&color=' + classi + '"><div class="item-content"><div class="item-media">\n\
                                <img width="60" height="60" src="' + img_display + '"></div><div class="item-inner"> <div class="" style="width:100%;">\n\
        <strong><span id="text_new" style="color:' + magic + ';" class="default_sub_font">&nbsp;' + object.get('title') + '</span></strong> <br> \n\
<span id="el_' + object.id + '" class="default_sub_font" style="color:' + magic + '">&nbsp;' + mydate + ', ' + time11 + '-' + time12 + '<br/>' + from + '</span></div></div></div><div class="sortable-handler"></div></a></li>');

                                            } else {
                                                $('.mypending').show();
                                                if ($('ul#pending li').length % 2 === 0) {
                                                    cn = c1;
                                                } else {
                                                    cn = c2;
                                                }
                                                $('#pending').append('<li style="background-color: ' + cn + ';"><a class="external" href="offerdetail.html?id=' + object.id + '&color=' + classi + '">\n\
<div class="item-content"><div class="item-media"><img width="60" height="60" src="' + img_display + '"></div><div class="item-inner"> <div class="" style="width:100%;">\n\
' + show_image + '<strong><span id="text_new" style="color:' + magic + ';" class="default_sub_font">&nbsp;' + object.get('title') + '</span></strong> <br> \n\
<span id="el_' + object.id + '" class="default_sub_font" style="color:' + magic + '">&nbsp;' + mydate + ', ' + time11 + '-' + time12 + '<br/>' + from + '</span>\n\
</div></div></div><div class="sortable-handler"></div></a></li>');

                                            }
                                        }
                                    } else {
                                        //new code paste here
                                        //event is coming in next days

                                        if (check_status[object.id] === 'Accepted') {

                                            if ($('ul#offers li').length % 2 === 0) {
                                                cn = c1;
                                            } else {
                                                cn = c2;
                                            }
                                            $('#offers').append('<li style="background-color: ' + cn + ';"><a class="external" href="offerdetail.html?id=' + object.id + '&color=' + classi + '"><div class="item-content"><div class="item-media">\n\
                                <img width="60" height="60" src="' + img_display + '"></div><div class="item-inner"> <div class="" style="width:100%;">\n\
        <strong><span id="text_new" style="color:' + magic + ';" class="default_sub_font">&nbsp;' + object.get('title') + '</span></strong> <br> \n\
<span id="el_' + object.id + '" class="default_sub_font" style="color:' + magic + '">&nbsp;' + mydate + ', ' + time11 + '-' + time12 + '<br/>' + from + '</span></div>' + show_image + '</div></div><div class="sortable-handler"></div></a></li>');

                                        } else {
                                            $('.mypending').show();
                                            if ($('ul#pending li').length % 2 === 0) {
                                                cn = c1;
                                            } else {
                                                cn = c2;
                                            }
                                            $('#pending').append('<li style="background-color: ' + cn + ';"><a class="external" href="offerdetail.html?id=' + object.id + '&color=' + classi + '">\n\
<div class="item-content"><div class="item-media"><img width="60" height="60" src="' + img_display + '"></div><div class="item-inner"> <div class="" style="width:100%;">\n\
' + show_image + '<strong><span id="text_new" style="color:' + magic + ';" class="default_sub_font">&nbsp;' + object.get('title') + '</span></strong> <br> \n\
<span id="el_' + object.id + '" class="default_sub_font" style="color:' + magic + '">&nbsp;' + mydate + ', ' + time11 + '-' + time12 + '<br/>' + from + '</span>\n\
</div></div></div><div class="sortable-handler"></div></a></li>');

                                        }

                                    }
                                } else {
show_image='';
                                    $('#poffers').append('<li id="past_' + object.id + '" class="swipeout" style="background-color:#eee"><div class="item-content swipeout-content"><div class="item-media">\n\
                                <a class="external" href="' + page + '.html?id=' + object.id + '&color=' + classi + '&type=past"><img width="60" height="60" src="' + img_display + '"></a></div><div class="item-inner" style="bottom:1px;">\n\
<div class="" style="width:100%">\n\
        <a class="external" href="' + page + '.html?id=' + object.id + '&color=' + classi + '&type=past"><strong><span id="text_new"  style="color:#8E8E93" class="default_sub_font">' + object.get('title') + '</span></strong> <br>\n\
<span id="el_' + object.id + '"  class="default_sub_font" style="color:#8E8E93">' + mydate + ', ' + time11 + '-' + time12 + '<br/>' + from + '</span></a></div>' + show_image + '</div></div><div class="sortable-handler"></div>\n\
<div class="swipeout-actions-right"><a onclick="killme(' + "'" + object.id + "'" + ')" style="background-color:red;color:white;"><img src="img/delete.png" width="30" /></a></div></div></li>');

                                }

                            }
                        }
                    }
                    //alert(object.id);
                    //alert(message);
                    findreceivers(object.id, magic, message);
//                    }
//                    }
                }

                if ($('ul#offers li').length === 0) {
                    $('#offers').append('<li><center><div class="" style="width:100%;">\n\
        <strong><span id="text_new" style="color:black;padding: 0 25px;" class="default_sub_font"><br/>This is your dashboard. \n\
All the heroic deeds you\'ve done for others or that they\'ve done for you will appear here. \n\
Click Offer or Ask below to set up your first event!</span></strong> <br> \n\
<br/></div><div class="item-after"></div></center></li>');
                }
                //}
                myApp.hideIndicator();
            },
            error: function (error) {
                myApp.hideIndicator();
                config.Msg("That didn’t really go to plan. We’re not sure why. Please reload CarePool.", 'Hmm, something’s wrong', 'OK');
            }
        });

    }

}
function func_past(offer_ids) {

    if (Parse.User.current()) {
        //past offers

        var GameScore = Parse.Object.extend("offers");
        myApp.showIndicator();
        var query2 = new Parse.Query(GameScore);
        query2.containedIn("objectId", offer_ids);
        query2.equalTo("sent", 1);
        //query2.notEqualTo("deleted", 1);
        query2.descending("date");
        query2.find({
            success: function (results) {
                //config.Msg('called past');

                var c3 = '#FFF';
                var c4 = '#eee';

                for (var i = 0; i < results.length; i++) {
                    var object = results[i];

                    if (check_viewed[object.id] || check_status[object.id] == 'Declined') {
                        var show_image = '';
                    } else {

                        var show_image = '<img src="img/icon-pink-32.png" width="16" style="float:right;" />';
                    }

                    //if (check_status[object.id] !== 'Declined' && object.get('status') !== 'Accepted') {

                    myApp.showIndicator();

                    //config.Msg(object.id + ' - ' + object.get('title'));

                    var from = '';
                    var message = '';
                    if (main_category_array[cats_array.indexOf(object.get('offer_Category_id'))].get('category') == 'help') {
                        classi = 'body_orange';
                        magic1 = '#fcaf1b';
                        img_display1 = main_category_array[cats_array.indexOf(object.get('offer_Category_id'))].get('color_image').url();
                    } else {
                        magic1 = '#40CBF4';
                        classi = 'body_blue';
                        img_display1 = main_category_array[cats_array.indexOf(object.get('offer_Category_id'))].get('color_image1').url();
                    }
                    if (magic1 === '#40CBF4') {
                        from = 'Care Recipient: ';
                        message = 'from';
                    } else {
                        message = 'to';
                        from = 'Care Provider: ';
                    }
                    if (main_category_array[cats_array.indexOf(object.get('offer_Category_id'))].get('category') == 'help') {
                        classi = 'body_orange';
                        magic1 = '#fcaf1b';
                        img_display1 = main_category_array[cats_array.indexOf(object.get('offer_Category_id'))].get('color_image').url();
                    } else {
                        magic1 = '#40CBF4';
                        classi = 'body_blue';
                        img_display1 = main_category_array[cats_array.indexOf(object.get('offer_Category_id'))].get('color_image1').url();
                    }
                    //alert(from);
                    var today = new Date(object.get('date'));
                    var mydate = today.toLocaleFormat('%m-%d-%Y');
                    //config.Msg(mydate);

                    // config.Msg(monthNames[mydate[0]]);
                    mydate = mydate.split('-');
                    //config.Msg(mydate[0]);
                    //config.Msg(monthNames[parseInt(mydate[0])-1]);
                    mydate = mydate[1] + ' ' + monthNames[parseInt(mydate[0]) - 1] + ' ' + mydate[2];

                    var page = '';
                    //var icon_image = '';
                    //var img_dislay1 = '';
                    var name2_index = tome_array.indexOf(object.id);
                    //config.Msg(name2_index);
                    if (name2_index > -1) {

                        page = 'offerdetail';
                        icon_image = 'info.png';
                    } else {

                        page = 'offerdetail';
                        icon_image = 'info.png';
                    }

                    var time1 = object.get('start_time').split(':');
                    if (time1[0] > 12) {
                        var time11 = time1[0] - 12;

//                        if (time11.toString().length === 1) {
//                            time11 = '0' + time11;
//                        }
                        time11 = time11 + ':' + time1[1] + ' PM';
                    } else {
                        time11 = object.get('start_time') + ' AM';
                    }

                    var time2 = object.get('end_time').split(':');
                    if (time2[0] >= 12) {
                        if (time2[0] > 12) {
                            var time12 = time2[0] - 12;
                        } else {
                            var time12 = time2[0];
                        }
//                        if (time12.toString().length === 1) {
//                            time12 = '0' + time12;
//                        }
                        time12 = time12 + ':' + time2[1] + ' PM';
                    } else {
                        time12 = object.get('end_time') + ' AM';
                    }


                    cn = '#eee';


/////////////////////////////
                    if (object.get('deleted') === 'true') {
                        if (object.get('created_by') === Parse.User.current().id) {
                            show_image = '';
                        }
                        $('#poffers').append('<li id="past_' + object.id + '" class="swipeout" style="background-color:  ' + cn + '"><div class="item-content swipeout-content"><div class="item-media">\n\
                                <a class="external" href="' + page + '.html?id=' + object.id + '&color=' + classi + '&type=past"><img width="60" height="60" src="' + img_display1 + '"></a></div><div class="item-inner" style="bottom:1px;">\n\
<div class="" style="width:100%">\n\
        <a class="external" href="' + page + '.html?id=' + object.id + '&color=' + classi + '&type=past"><strong><span id="text_new"  style="color:#8E8E93" class="default_sub_font">' + object.get('title') + '</span></strong> <br>\n\
<span id="el_' + object.id + '"  class="default_sub_font" style="color:#8E8E93">' + mydate + ', ' + time11 + '-' + time12 + '<br/>' + from + '</span></a></div>' + show_image + '</div></div><div class="sortable-handler"></div>\n\
<div class="swipeout-actions-right"><a onclick="killme(' + "'" + object.id + "'" + ')" style="background-color:red;color:white;"><img src="img/delete.png" width="30" /></a></div></div></li>');

                    } else {

                        //offer is created by me
                        if (object.get('created_by') === Parse.User.current().id) {

                            if (object.get('status') === 'Accepted') {

                                $('#poffers').append('<li id="past_' + object.id + '" class="swipeout" style="background-color: ' + cn + ';"><div class="item-content swipeout_content"><div class="item-media">\n\
                                <a class="external" href="offerdetail.html?id=' + object.id + '&color=' + classi + '&type=past"><img width="60" height="60" src="' + img_display1 + '"></a></div><div class="item-inner" style="bottom:1px;"> \n\
<div class="" style="width:100%;">\n\
        <a class="external" href="offerdetail.html?id=' + object.id + '&color=' + classi + '&type=past"><strong><span id="text_new" style="color:' + magic1 + ';" class="default_sub_font">&nbsp;' + object.get('title') + '</span></strong> <br> \n\
<span id="el_' + object.id + '" class="default_sub_font" style="color:' + magic1 + '">&nbsp;' + mydate + ', ' + time11 + '-' + time12 + '<br/>' + from + '</span></a></div>' + show_image + '</div></div><div class="sortable-handler"></div>\n\
<div class="swipeout-actions-right"><a onclick="killme(' + "'" + object.id + "'" + ')" style="background-color:red;color:white;"><img src="img/delete.png" width="30" /></a></div></div></li>');

                            } else {



                                var find_status = $.grep(check_status_from_me, function (e) {
                                    return e.offer_id == object.id;
                                });

                                //alert(find_status.length);
                                //count deleted records against offer 
                                var numBoys = find_status.reduce(function (n, person) {
                                    return n + (person.status == 'Declined');
                                }, 0);

                                //find the occurence of Declined 

                                //if not declined by all
                                if (numBoys !== find_status.length) {
                                    //find the occurrence of accepted
                                    var occurances2 = find_status.reduce(function (n, person) {
                                        return n + (person.status == 'Accepted');
                                    }, 0);

                                    if (occurances2 >= 1) {
                                        //colored
                                        $('#poffers').append('<li id="past_' + object.id + '" class="swipeout" style="background-color: ' + cn + ';"><div class="item-content swipeout_content"><div class="item-media">\n\
                                <a class="external" href="offerdetail.html?id=' + object.id + '&color=' + classi + '&type=past"><img width="60" height="60" src="' + img_display1 + '"></a></div><div class="item-inner" style="bottom:1px;"> \n\
<div class="" style="width:100%;">\n\
        <a class="external" href="offerdetail.html?id=' + object.id + '&color=' + classi + '&type=past"><strong><span id="text_new" style="color:' + magic1 + ';" class="default_sub_font">&nbsp;' + object.get('title') + '</span></strong> <br> \n\
<span id="el_' + object.id + '" class="default_sub_font" style="color:' + magic1 + '">&nbsp;' + mydate + ', ' + time11 + '-' + time12 + '<br/>' + from + '</span></a></div>' + show_image + '</div></div><div class="sortable-handler"></div>\n\
<div class="swipeout-actions-right"><a onclick="killme(' + "'" + object.id + "'" + ')" style="background-color:red;color:white;"><img src="img/delete.png" width="30" /></a></div></div></li>');

                                    } else {
                                        //grey
                                        $('#poffers').append('<li id="past_' + object.id + '" class="swipeout" style="background-color: ' + cn + ';"><div class="item-content swipeout_content"><div class="item-media">\n\
                                <a class="external" href="offerdetail.html?id=' + object.id + '&color=' + classi + '&type=past"><img width="60" height="60" src="' + img_display1 + '"></a></div><div class="item-inner" style="bottom:1px;"> \n\
<div class="" style="width:100%;">\n\
        <a class="external" href="offerdetail.html?id=' + object.id + '&color=' + classi + '&type=past"><strong><span id="text_new" style="color:#8E8E93;" class="default_sub_font">&nbsp;' + object.get('title') + '</span></strong> <br> \n\
<span id="el_' + object.id + '" class="default_sub_font" style="color:#8E8E93">&nbsp;' + mydate + ', ' + time11 + '-' + time12 + '<br/>' + from + '</span></a></div>' + show_image + '</div></div><div class="sortable-handler"></div>\n\
<div class="swipeout-actions-right"><a onclick="killme(' + "'" + object.id + "'" + ')" style="background-color:red;color:white;"><img src="img/delete.png" width="30" /></a></div></div></li>');

                                    }
                                } else {

                                    //rejected by all
                                    //grey color
                                    $('#poffers').append('<li id="past_' + object.id + '" class="swipeout" style="background-color: ' + cn + ';"><div class="item-content swipeout_content"><div class="item-media">\n\
                                <a class="external" href="offerdetail.html?id=' + object.id + '&color=' + classi + '&type=past"><img width="60" height="60" src="' + img_display1 + '"></a></div><div class="item-inner" style="bottom:1px;"> \n\
<div class="" style="width:100%;">\n\
        <a class="external" href="offerdetail.html?id=' + object.id + '&color=' + classi + '&type=past"><strong><span id="text_new" style="color:#8E8E93;" class="default_sub_font">&nbsp;' + object.get('title') + '</span></strong> <br> \n\
<span id="el_' + object.id + '" class="default_sub_font" style="color:#8E8E93">&nbsp;' + mydate + ', ' + time11 + '-' + time12 + '<br/>' + from + '</span></a></div>' + show_image + '</div></div><div class="sortable-handler"></div>\n\
<div class="swipeout-actions-right"><a onclick="killme(' + "'" + object.id + "'" + ')" style="background-color:red;color:white;"><img src="img/delete.png" width="30" /></a></div></div></li>');

                                }

                            }
                        } else {

                            //offer is created by some1 else
                            //requested to me

                            if (object.get('status') === 'Accepted') {

                                if (object.get('accepted_by') === Parse.User.current().id) {

                                    $('#poffers').append('<li id="past_' + object.id + '" class="swipeout" style="background-color: ' + cn + ';"><div class="item-content swipeout_content"><div class="item-media">\n\
                                <a class="external" href="offerdetail.html?id=' + object.id + '&color=' + classi + '&type=past"><img width="60" height="60" src="' + img_display1 + '"></a></div><div class="item-inner" style="bottom:1px;"> \n\
<div class="" style="width:100%;">\n\
        <a class="external" href="offerdetail.html?id=' + object.id + '&color=' + classi + '&type=past"><strong><span id="text_new" style="color:' + magic1 + ';" class="default_sub_font">&nbsp;' + object.get('title') + '</span></strong> <br> \n\
<span id="el_' + object.id + '" class="default_sub_font" style="color:' + magic1 + '">&nbsp;' + mydate + ', ' + time11 + '-' + time12 + '<br/>' + from + '</span></a></div>' + show_image + '</div></div><div class="sortable-handler"></div>\n\
<div class="swipeout-actions-right"><a onclick="killme(' + "'" + object.id + "'" + ')" style="background-color:red;color:white;"><img src="img/delete.png" width="30" /></a></div></div></li>');

                                }
                                //added for offer thing starts
                                else {
                                    //check if offer or request from offers table
                                    if (object.get('category') === 'offer') {
                                        if (check_status[object.id] !== 'Declined') {
                                            if (check_status[object.id] === 'Accepted') {

                                                $('#poffers').append('<li id="past_' + object.id + '" class="swipeout" style="background-color: ' + cn + ';"><div class="item-content swipeout_content"><div class="item-media">\n\
                                <a class="external" href="offerdetail.html?id=' + object.id + '&color=' + classi + '&type=past"><img width="60" height="60" src="' + img_display1 + '"></a></div><div class="item-inner" style="bottom:1px;"> \n\
<div class="" style="width:100%;">\n\
        <a class="external" href="offerdetail.html?id=' + object.id + '&color=' + classi + '&type=past"><strong><span id="text_new" style="color:' + magic1 + ';" class="default_sub_font">&nbsp;' + object.get('title') + '</span></strong> <br> \n\
<span id="el_' + object.id + '" class="default_sub_font" style="color:' + magic1 + '">&nbsp;' + mydate + ', ' + time11 + '-' + time12 + '<br/>' + from + '</span></a></div>' + show_image + '</div></div><div class="sortable-handler"></div>\n\
<div class="swipeout-actions-right"><a onclick="killme(' + "'" + object.id + "'" + ')" style="background-color:red;color:white;"><img src="img/delete.png" width="30" /></a></div></div></li>');

                                            } else {

                                                $('#poffers').append('<li id="past_' + object.id + '" class="swipeout" style="background-color: ' + cn + ';"><div class="item-content swipeout_content"><div class="item-media">\n\
                                <a class="external" href="offerdetail.html?id=' + object.id + '&color=' + classi + '&type=past"><img width="60" height="60" src="' + img_display1 + '"></a></div><div class="item-inner" style="bottom:1px;"> \n\
<div class="" style="width:100%;">\n\
        <a class="external" href="offerdetail.html?id=' + object.id + '&color=' + classi + '&type=past"><strong><span id="text_new" style="color:#8E8E93;;" class="default_sub_font">&nbsp;' + object.get('title') + '</span></strong> <br> \n\
<span id="el_' + object.id + '" class="default_sub_font" style="color:#8E8E93;">&nbsp;' + mydate + ', ' + time11 + '-' + time12 + '<br/>' + from + '</span></a></div>' + show_image + '</div></div><div class="sortable-handler"></div>\n\
<div class="swipeout-actions-right"><a onclick="killme(' + "'" + object.id + "'" + ')" style="background-color:red;color:white;"><img src="img/delete.png" width="30" /></a></div></div></li>');

                                            }
                                        } else {
                                            $('#poffers').append('<li id="past_' + object.id + '" class="swipeout" style="background-color:  ' + cn + '"><div class="item-content swipeout-content"><div class="item-media">\n\
                                <a class="external" href="' + page + '.html?id=' + object.id + '&color=' + classi + '&type=past"><img width="60" height="60" src="' + img_display1 + '"></a></div><div class="item-inner" style="bottom:1px;">\n\
<div class="" style="width:100%">\n\
        <a class="external" href="' + page + '.html?id=' + object.id + '&color=' + classi + '&type=past"><strong><span id="text_new"  style="color:#8E8E93" class="default_sub_font">' + object.get('title') + '</span></strong> <br>\n\
<span id="el_' + object.id + '"  class="default_sub_font" style="color:#8E8E93">' + mydate + ', ' + time11 + '-' + time12 + '<br/>' + from + '</span></a></div>' + show_image + '</div></div><div class="sortable-handler"></div>\n\
<div class="swipeout-actions-right"><a onclick="killme(' + "'" + object.id + "'" + ')" style="background-color:red;color:white;"><img src="img/delete.png" width="30" /></a></div></div></li>');

                                        }
                                    } else {
                                        $('#poffers').append('<li id="past_' + object.id + '" class="swipeout" style="background-color:  ' + cn + '"><div class="item-content swipeout-content"><div class="item-media">\n\
                                <a class="external" href="' + page + '.html?id=' + object.id + '&color=' + classi + '&type=past"><img width="60" height="60" src="' + img_display1 + '"></a></div><div class="item-inner" style="bottom:1px;">\n\
<div class="" style="width:100%">\n\
        <a class="external" href="' + page + '.html?id=' + object.id + '&color=' + classi + '&type=past"><strong><span id="text_new"  style="color:#8E8E93" class="default_sub_font">' + object.get('title') + '</span></strong> <br>\n\
<span id="el_' + object.id + '"  class="default_sub_font" style="color:#8E8E93">' + mydate + ', ' + time11 + '-' + time12 + '<br/>' + from + '</span></a></div>' + show_image + '</div></div><div class="sortable-handler"></div>\n\
<div class="swipeout-actions-right"><a onclick="killme(' + "'" + object.id + "'" + ')" style="background-color:red;color:white;"><img src="img/delete.png" width="30" /></a></div></div></li>');

                                    }
                                }
                                //added for offer thing ends
                            } else {

                                if (check_status[object.id] !== 'Declined') {

                                    if (check_status[object.id] === 'Accepted') {

                                        $('#poffers').append('<li id="past_' + object.id + '" class="swipeout" style="background-color: ' + cn + ';"><div class="item-content swipeout_content"><div class="item-media">\n\
                                <a class="external" href="offerdetail.html?id=' + object.id + '&color=' + classi + '&type=past"><img width="60" height="60" src="' + img_display1 + '"></a></div><div class="item-inner" style="bottom:1px;"> \n\
<div class="" style="width:100%;">\n\
        <a class="external" href="offerdetail.html?id=' + object.id + '&color=' + classi + '&type=past"><strong><span id="text_new" style="color:' + magic1 + ';" class="default_sub_font">&nbsp;' + object.get('title') + '</span></strong> <br> \n\
<span id="el_' + object.id + '" class="default_sub_font" style="color:' + magic1 + '">&nbsp;' + mydate + ', ' + time11 + '-' + time12 + '<br/>' + from + '</span></a></div>' + show_image + '</div></div><div class="sortable-handler"></div>\n\
<div class="swipeout-actions-right"><a onclick="killme(' + "'" + object.id + "'" + ')" style="background-color:red;color:white;"><img src="img/delete.png" width="30" /></a></div></div></li>');

                                    } else {

                                        $('#poffers').append('<li id="past_' + object.id + '" class="swipeout" style="background-color: ' + cn + ';"><div class="item-content swipeout_content"><div class="item-media">\n\
                                <a class="external" href="offerdetail.html?id=' + object.id + '&color=' + classi + '&type=past"><img width="60" height="60" src="' + img_display1 + '"></a></div><div class="item-inner" style="bottom:1px;"> \n\
<div class="" style="width:100%;">\n\
        <a class="external" href="offerdetail.html?id=' + object.id + '&color=' + classi + '&type=past"><strong><span id="text_new" style="color:#8E8E93;" class="default_sub_font">&nbsp;' + object.get('title') + '</span></strong> <br> \n\
<span id="el_' + object.id + '" class="default_sub_font" style="color:#8E8E93;">&nbsp;' + mydate + ', ' + time11 + '-' + time12 + '<br/>' + from + '</span></a></div>' + show_image + '</div></div><div class="sortable-handler"></div>\n\
<div class="swipeout-actions-right"><a onclick="killme(' + "'" + object.id + "'" + ')" style="background-color:red;color:white;"><img src="img/delete.png" width="30" /></a></div></div></li>');

                                    }
                                } else {

                                    $('#poffers').append('<li id="past_' + object.id + '" class="swipeout" style="background-color:  ' + cn + '"><div class="item-content swipeout-content"><div class="item-media">\n\
                                <a class="external" href="' + page + '.html?id=' + object.id + '&color=' + classi + '&type=past"><img width="60" height="60" src="' + img_display1 + '"></a></div><div class="item-inner" style="bottom:1px;">\n\
<div class="" style="width:100%">\n\
        <a class="external" href="' + page + '.html?id=' + object.id + '&color=' + classi + '&type=past"><strong><span id="text_new"  style="color: #8E8E93;" class="default_sub_font">&nbsp;' + object.get('title') + '</span></strong> <br>\n\
<span id="el_' + object.id + '"  class="default_sub_font" style="color:#8E8E93;">' + mydate + ', ' + time11 + '-' + time12 + '<br/>' + from + '</span></a></div>' + show_image + '</div></div><div class="sortable-handler"></div>\n\
<div class="swipeout-actions-right"><a onclick="killme(' + "'" + object.id + "'" + ')" style="background-color:red;color:white;"><img src="img/delete.png" width="30" /></a></div></div></li>');

                                }
                            }
                        }
                    }

/////////////////////////////

                    myApp.hideIndicator();
                    findreceivers(object.id, magic1, message);
                    //}

                }
                myApp.hideIndicator();

            },
            error: function (error) {
                myApp.hideIndicator();
                config.Msg("That didn’t really go to plan. We’re not sure why. Please reload CarePool.", 'Hmm, something’s wrong', 'OK');
            }
        });
    }
}
function killme(offer_id) {

    config.Confirm(
            "Are you sure you want to remove?", // message
            function (buttonIndex) {
                yeskillme(buttonIndex, offer_id);
            }, // callback
            'Delete event?', // title
            'No,Yes' // buttonName
            );

    return false;


}
function yeskillme(answer, id) {
    switch (answer) {
        case 1:
            // alert('no');

            break;
        case 2:
            //get all user_offers against this offer_id in order to select deleted flags
            myApp.showIndicator();
            var offer_id = id;
            var ids_array = [];

            var off_cat = Parse.Object.extend("user_offers");
            var off = new Parse.Query(off_cat);
            off.equalTo("offer_id", offer_id);

            off.find({
                success: function (results) {
                    var countfor_provider = 0;
                    var countfor_receiver = 0;
                    for (var i = 0; i < results.length; i++) {
                        var object = results[i];
                        if (object.get('care_provider') === Parse.User.current().get('username')) {
                            countfor_provider = countfor_provider + 1;
                            ids_array.push(object.id);
                        }
                        if (object.get('care_receiver') === Parse.User.current().get('username')) {
                            countfor_receiver = countfor_receiver + 1;
                            ids_array.push(object.id);
                        }

                    }
                    var column_to_update = '';
                    if (countfor_provider > 0) {
                        column_to_update = 'care_provider_delete';
                    }
                    if (countfor_receiver > 0) {
                        column_to_update = 'care_receiver_delete';
                    }

                    var lifeArray = [];


                    for (var i = 0; i < ids_array.length; i++) {
                        var newLife = new off_cat();
                        newLife.set("objectId", ids_array[i]);
                        newLife.set(column_to_update, 1);
                        lifeArray.push(newLife);
                    }

                    // save all the newly created objects
                    Parse.Object.saveAll(lifeArray, {
                        success: function (objs) {
                            // objects have been saved...
                            $('#past_' + offer_id).remove();
                            myApp.hideIndicator();
                        },
                        error: function (error) {
                            // an error occurred...
                            myApp.hideIndicator();
                            config.Msg("That didn’t really go to plan. We’re not sure why. Please reload CarePool.", 'Hmm, something’s wrong', 'OK');
                            return false;
                        }
                    });

                },
                error: function (error) {
                    config.Msg("That didn’t really go to plan. We’re not sure why. Please reload CarePool.", 'Hmm, something’s wrong', 'OK');
                    myApp.hideIndicator();
                }
            });

            break;
    }
}
//setTimeout(function(){alert($('ul#offers li').length)},1500);
function findreceivers(element_id, from, to) {

    //if (check_status[object.id] !== 'Declined') {
    //alert(500);
    myApp.showIndicator();
    var off_cat = Parse.Object.extend("user_offers");
    var off = new Parse.Query(off_cat);
    off.equalTo("offer_id", element_id);
    off.find({
        success: function (results) {
            var names = '';
            //alert(results.length);
            for (var i = 0; i < results.length; i++) {
                myApp.showIndicator();
                var myobject = results[i];

                //config.Msg(object.get('date'));
                var name;
                //config.Msg(object.get('date'));
                if (from === '#40CBF4') {
                    name = myobject.get('care_receiver_name');
                } else {
                    name = myobject.get('care_provider_name');
                }

                if (names.indexOf(name) === -1) {
                    names = names + name + ', ';
                }
                //if (i === results.length - 1) {
                if (results.length === 1) {
                    $('#el_' + element_id).append(name);
                } else {
                    if (i === 1) {
                        str = names.substring(', ', names.length - 2);
                        //console.log(str);
                        //alert(str);
                        //$('#el_' + element_id).append(str);
                        if (str.indexOf(',') === -1) {
                            //alert('#el_' + element_id);
                            //alert(str);
                            $('#el_' + element_id).append(str);
                        } else {
                            $('#el_' + element_id).append(str + '...');
                        }

                        break;
                    }
                }

            }

            myApp.hideIndicator();
        },
        error: function (error) {
            config.Msg("That didn’t really go to plan. We’re not sure why. Please reload CarePool.", 'Hmm, something’s wrong', 'OK');
            myApp.hideIndicator();
        }
    });

}