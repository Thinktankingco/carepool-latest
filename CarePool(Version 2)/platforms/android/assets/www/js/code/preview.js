/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


function getUrlVars() {
    var vars = {};
    var parts = window.location.href.replace(
            /[?&]+([^=&]+)=([^&]*)/gi, function (m, key, value) {
                vars[key] = value;
            });
    return vars;
}
var arr_r = new Array(); // {} will create an object
function getallusers() {

    var off_cat = Parse.Object.extend("User");
    var off = new Parse.Query(off_cat);
    off.find({
        success: function (results) {
            standarresult = results;
            for (var i = 0; i < results.length; i++) {
                var object = results[i];

                arr_r.push(object.get('username'));

            }
            myApp.hideIndicator();


        },
        error: function (error) {
            myApp.hideIndicator();
            config.Msg("That didn’t really go to plan. We’re not sure why. Please reload CarePool.", 'Hmm, something’s wrong', 'OK');
        }
    });

}
$(function () {
    if (getUrlVars()["cat"] == 'ask') {
        $('body').addClass('body_orange');
    } else {
        $('body').addClass('body_blue');
    }


    ttlheight = $(window).height() - 180;
    $('.page-content').css('height', ttlheight);
    $('#auth_user').html(Parse.User.current().get('full_name'));
    //get offer details
    myApp.showIndicator();
    var off_cat = Parse.Object.extend("offers");
    var off = new Parse.Query(off_cat);
    off.equalTo("objectId", getUrlVars()["id"]);
    off.find({
        success: function (results) {
            var monthNames = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'
            ];
            Date.prototype.toLocaleFormat = Date.prototype.toLocaleFormat || function (pattern) {
                var year = this.getFullYear(), month = this.getMonth() + 1, day = this.getDate();
                if (month < 10)
                    month = '0' + month;
                if (day < 10)
                    day = '0' + day;
                return pattern.replace(/%Y/g, year).replace(/%m/g, month).replace(/%d/g, day);
            };
            for (var i = 0; i < results.length; i++) {
                var object = results[i];
                console.log(object);
                //config.Msg(object.get('date'));
                var today = new Date(object.get('date'));
                var mydate = today.toLocaleFormat('%m-%d-%Y');
                //config.Msg(mydate);

                // config.Msg(monthNames[mydate[0]]);
                mydate = mydate.split('-');
                //config.Msg(mydate[0]);
                //config.Msg(monthNames[parseInt(mydate[0])-1]);
                mydate = mydate[1] + ' ' + monthNames[parseInt(mydate[0]) - 1] + ' ' + mydate[2];
                $('#date').html(mydate);
                var time1 = object.get('start_time').split(':');
                if (time1[0] > 12) {
                    var time11 = time1[0] - 12;

                    if (time11.toString().length === 1) {
                        time11 = '0' + time11;
                    }
                    time11 = time11 + ':' + time1[1] + ' PM';
                } else {
                    time11 = object.get('start_time') + ' AM';
                }
                //alert(object.get('end_time'));
                var time2 = object.get('end_time').split(':');
                if (time2[0] >= 12) {
                    if (time2[0] > 12) {
                        var time12 = time2[0] - 12;
                    } else {
                        var time12 = time2[0];
                    }

                    if (time12.toString().length === 1) {
                        time12 = '0' + time12;
                    }
                    time12 = time12 + ':' + time2[1] + ' PM';
                } else {
                    time12 = object.get('end_time') + ' AM';
                }
                $('#time').html(time11 + '-' + time12);
                $('#location').html(object.get('location'));
                $('#message').html(object.get('message'));
                $("#title").html(object.get('title'));
                func_getcategory(object.get('offer_Category_id'));
            }

            myApp.hideIndicator();
        },
        error: function (error) {
            config.Msg("That didn’t really go to plan. We’re not sure why. Please reload CarePool.", 'Hmm, something’s wrong', 'OK');
            myApp.hideIndicator();
        }
    });
    var name;
    var email_adress = [];
    var email_name = [];
    var player_ids = [];
    //get offer recepient
    myApp.showIndicator();
    var off_cat = Parse.Object.extend("user_offers");
    var off = new Parse.Query(off_cat);
    off.equalTo("offer_id", getUrlVars()["id"]);
    off.find({
        success: function (results) {
            var names = '';
            for (var i = 0; i < results.length; i++) {
                var object = results[i];
                console.log(object);
                //config.Msg(object.get('date'));

                if (getUrlVars()["cat"] === 'ask') {
                    name = object.get('care_provider_name');
                    email_adress.push(object.get('care_provider'));
                    email_name.push(object.get('care_provider_name'));
                } else {
                    name = object.get('care_receiver_name');
                    email_adress.push(object.get('care_receiver'));
                    email_name.push(object.get('care_receiver_name'));
                }

                names = names + name + ', ';

            }
            str = names.substring(', ', names.length - 2);
            $('#recepients').html(str);
            myApp.hideIndicator();
            getallusers();
        },
        error: function (error) {
            config.Msg("That didn’t really go to plan. We’re not sure why. Please reload CarePool.", 'Hmm, something’s wrong', 'OK');
            myApp.hideIndicator();
        }
    });

    $('#c1').click(function () {
        // config.Msg(getUrlVars()["id"]);

        if (Parse.User.current()) {
            //update offer to be in set mode
            myApp.showIndicator();
            var GameScore = Parse.Object.extend("offers");
            var query = new GameScore();

            //config.Msg(savedate);
            //return false;
            query.save({
                objectId: getUrlVars()["id"],
                sent: 1,
                creation_start: new Date(localStorage.getItem('creation_start')),
                creation_ends: new Date()
            },
                    {
                        success: function (object) {
                            //send push to receiving parties start
                            var off_cat = Parse.Object.extend("User");
                            var off = new Parse.Query(off_cat);
                            off.containedIn("username", email_adress);
                            off.notEqualTo("player_id", null);
                            off.equalTo("push_notes", 'Yes');
                            off.find({
                                success: function (results) {
                                    standarresult = results;
                                    if (results.length > 0) {
                                        for (var i = 0; i < results.length; i++) {
                                            var object = results[i];

                                            player_ids.push(object.get('player_id'));
                                            if (i === results.length - 1) {
                                                //alert(player_ids);
                                                if (player_ids.length > 0) {

                                                    if (getUrlVars()["cat"] === 'offer') {
                                                        titleforpush = 'Yippee!';
                                                        message = "You have received a new offer of care from " + Parse.User.current().get('full_name') + ". Please refresh your Dashboard for details.";
                                                    } else {
                                                        titleforpush = 'Someone needs you!';
                                                        message = "You have received a new request for help from " + Parse.User.current().get('full_name') + ". Please refresh your Dashboard for details.";

                                                    }
                                                    // alert(477);
                                                    $.ajax({
                                                        type: "post",
                                                        url: 'https://onesignal.com/api/v1/notifications',
                                                        data: {app_id: '8c1378a8-a783-400e-94da-e9505801a0c1',
                                                            include_player_ids: player_ids,
                                                            //include_player_ids: ['07e84db6-2f92-4bc3-ba31-9a20c66ad0e8', '943cdab1-364e-47a8-b28b-fcf614e8bad4'],
                                                            contents: {"en": message},
                                                            headings: {'en': titleforpush},
                                                             android_group:true,
                                                            android_group_message: {"en": "You have $[notif_count] new messages"},
                                                            ios_badgeType: 'Increase',
                                                            ios_badgeCount: 1},
                                                        crossDomain: true,
                                                        dataType: "json",
                                                        cache: false,
                                                        success: function (data) {
                                                            //alert(JSON.stringify(data));

                                                            gobacktodashboard();
                                                        },
                                                        error: function (error) {
                                                            //console.log();
                                                            //alert(JSON.stringify(data));
                                                            config.Msg("That didn’t really go to plan. We’re not sure why. Please reload CarePool.", 'Hmm, something’s wrong', 'OK');
                                                        }
                                                    });
                                                } else {
                                                    //alert(499);
                                                    gobacktodashboard();
                                                }
                                            }

                                        }
                                    } else {
                                        gobacktodashboard();
                                    }


                                    myApp.showIndicator();

                                    //check here whether subscribed or not starts here in order to send count email -starts
                                    myApp.showIndicator();
                                    var off_cat = Parse.Object.extend("subscription");
                                    var off = new Parse.Query(off_cat);
                                    off.equalTo("user_id", Parse.User.current().get('username'));
                                    off.count({
                                        success: function (results) {

                                            if (results === 0) {
                                                if (getUrlVars()["cat"] === 'offer') {
                                                    //getting count of offers till now
                                                    var off_cat = Parse.Object.extend("offers");
                                                    var off = new Parse.Query(off_cat);
                                                    off.equalTo("created_by", Parse.User.current().id);
                                                    off.equalTo("cat", 'offer');
                                                    off.count({
                                                        success: function (results_offer) {
                                                            if (results_offer === 5) {
                                                                send_accomplishments('http://www.carepool.co/carepool_emails/user_accomp_5.php');
                                                            } else if (results_offer === 20) {
                                                                send_accomplishments('http://www.carepool.co/carepool_emails/user_accomp_20.php');
                                                            } else {
                                                                gobacktodashboard();
                                                            }

                                                            // myApp.hideIndicator();
                                                        },
                                                        error: function (error) {
                                                            config.Msg("That didn’t really go to plan. We’re not sure why. Please reload CarePool.", 'Hmm, something’s wrong', 'OK');
                                                            myApp.hideIndicator();
                                                        }
                                                    });
                                                } else {
                                                    //getting count of ask till now
                                                    var off_cat = Parse.Object.extend("offers");
                                                    var off = new Parse.Query(off_cat);
                                                    off.equalTo("created_by", Parse.User.current().id);
                                                    off.equalTo("cat", 'ask');
                                                    off.count({
                                                        success: function (results_count) {
                                                            if (results_count === 10) {
                                                                send_accomplishments('http://www.carepool.co/carepool_emails/user_accomp_10.php');
                                                            } else {
                                                                gobacktodashboard();
                                                            }
                                                            //myApp.hideIndicator();
                                                        },
                                                        error: function (error) {
                                                            config.Msg("That didn’t really go to plan. We’re not sure why. Please reload CarePool.", 'Hmm, something’s wrong', 'OK');
                                                            myApp.hideIndicator();
                                                        }
                                                    });
                                                }
                                            } else {
                                                gobacktodashboard();
                                            }
                                            //myApp.hideIndicator();
                                        },
                                        error: function (error) {
                                            config.Msg("That didn’t really go to plan. We’re not sure why. Please reload CarePool.", 'Hmm, something’s wrong', 'OK');
                                            myApp.hideIndicator();
                                        }
                                    });
                                    //check whether subscribed or not ends here

                                    //myApp.hideIndicator();

                                },
                                error: function (error) {
                                    myApp.hideIndicator();
                                    config.Msg("That didn’t really go to plan. We’re not sure why. Please reload CarePool.", 'Hmm, something’s wrong', 'OK');
                                }
                            });
                            //send push to receiving parties ends


                            //now invite those who are not part of the app-starts


                            var array_to_dismiss = [];
                            var name_to_dismiss = [];
                            var count_for_me = email_adress.length;

                            for (i = 0; i < count_for_me; i++) {

                                var index = arr_r.indexOf(email_adress[i]);

                                if (index > -1) {

                                    array_to_dismiss.push(email_adress[i]);
                                    name_to_dismiss.push(email_name[i]);
                                    // email_array.splice(email_array.indexOf(email_array[i]), 1);
                                    // email_name.splice(email_name.indexOf(email_name[i]), 1);
                                }

                            }
                            //alert(array_to_dismiss);
                            //remove recepients
                            var count_for_me = email_adress.length;
                            for (i = 0; i < count_for_me; i++) {

                                var index = email_adress.indexOf(array_to_dismiss[i]);

                                if (index > -1) {

                                    email_adress.splice(email_adress.indexOf(array_to_dismiss[i]), 1);
                                    //email_name.splice(email_name.indexOf(array_to_dismiss[i]), 1);
                                    email_name.splice(email_name.indexOf(name_to_dismiss[i]), 1);
                                }

                            }




//                            var count_for_me = email_adress.length;
//                            for (i = 0; i < count_for_me; i++) {
//
//                                var index = arr_r.indexOf(email_adress[i]);
//
//                                if (index > -1) {
//
//                                    //array_to_dismiss.push(email_adress[i]);
//                                    email_adress.splice(email_adress.indexOf(email_adress[i]), 1);
//                                    email_name.splice(email_name.indexOf(email_name[i]), 1);
//                                }
//
//                            }
                            //alert(email_adress);
                            //aalert(email_name);
                            //return false;
                            if (email_adress.length > 0) {
                                //send invite email
                                $.ajax({
                                    type: "post",
                                    url: 'http://www.carepool.co/carepool_emails/invite.php',
                                    data: {name: Parse.User.current().get('full_name'),
                                        email_list: email_adress,
                                        email_name: email_name},
                                    crossDomain: true,
                                    dataType: "json",
                                    cache: false,
                                    success: function (data) {
                                        // console.log();

                                        if (data === 'sent') {
                                            gobacktodashboard();
                                        } else {

                                            config.Msg("That didn’t really go to plan. We’re not sure why. Please reload CarePool.", 'Hmm, something’s wrong', 'OK');
                                            return false;
                                        }
                                        myApp.hideIndicator();
                                    },
                                    error: function (error) {

                                        //console.log();
                                        config.Msg("That didn’t really go to plan. We’re not sure why. Please reload CarePool.", 'Hmm, something’s wrong', 'OK');
                                        //myApp.hideIndicator();
                                        return false;
                                    }

                                });
                            } else {
                                //redirect to dashboard
                                gobacktodashboard();
                            }
                            //now invite those who are not part of the app-ends
                        },
                        error: function (model, error)
                        {
                            //$(".error").show();
                            // config.Msg('error');
                            config.Msg("That didn’t really go to plan. We’re not sure why. Please reload CarePool.", 'Hmm, something’s wrong', 'OK');
                            myApp.hideIndicator();
                        }
                    });
        }
    });
    $('#c2').click(function () {
        // config.Msg(getUrlVars()["id"]);
        if (Parse.User.current()) {
            window.location = 'create.html?o_id=' + getUrlVars()["id"] + '&cat=' + getUrlVars()["cat"];
        }
    });
});
function func_getcategory(id) {
    //get offers categories
    myApp.showIndicator();
    var off_cat = Parse.Object.extend("offer_Category");
    var off = new Parse.Query(off_cat);
    off.equalTo("objectId", id);
    off.find({
        success: function (results) {
            for (var i = 0; i < results.length; i++) {
                var object = results[i];
                console.log(object);
                $("#cat_img")[0].src = object.get('simple_image').url();

            }
            myApp.hideIndicator();
        },
        error: function (error) {
            config.Msg("That didn’t really go to plan. We’re not sure why. Please reload CarePool.", 'Hmm, something’s wrong', 'OK');
            myApp.hideIndicator();
        }
    });
}
function send_accomplishments(url) {

    $.ajax({
        type: "post",
        url: url,
        data: {email: Parse.User.current().get('email'), name: Parse.User.current().get('full_name')},
        crossDomain: true,
        dataType: "json",
        cache: false,
        success: function (data) {
            gobacktodashboard();
        },
        error: function (error) {
            //console.log();
            //alert(JSON.stringify(data));
            config.Msg("That didn’t really go to plan. We’re not sure why. Please reload CarePool.", 'Hmm, something’s wrong', 'OK');
        }
    });

}
var count_forback = 0;
function gobacktodashboard() {
    count_forback++;
    if (count_forback === 3) {
        myApp.hideIndicator();
        window.location = 'dashboard.html';
    }

}