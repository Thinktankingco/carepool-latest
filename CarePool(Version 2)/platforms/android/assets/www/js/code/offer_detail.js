function getUrlVars() {
    var vars = {};
    var parts = window.location.href.replace(
            /[?&]+([^=&]+)=([^&]*)/gi, function (m, key, value) {
                vars[key] = value;
            });
    return vars;
}
var monthNames = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'
];
var rec = []; // requester/offerer email address array
var rec_names = []; //requestr/offerer names array
var rec_status = []; //requestr/offerer status array
var player_ids = []; //requester/offere player ids
var user_offers_ids = [];
var myplayer_ids = []; //requester/offere player ids
var type = ''; //whether it is a request/offer
var event_date = ''; //date of the event
var event_name = ''; //name of the event
var event_time = ''; //time of the event
var owner = ''; //i created or other
$(function () {


    //hide everything before deciding whether it is requested by me/requested to me
    $('#signin_btn').hide();

    $('.c5').hide();
    $('#recepients').hide();
    $('#approve').hide();
    //change the background color the screen to distinguish 
    $('#change_me').addClass(getUrlVars()["color"]);
    //setting height of the page according to content
    ttlheight = $(window).height() - 200;
    $('#views').css('height', ttlheight);
    ttlheight = $(window).height() - 120;
    $('.toolbar-fixed').css('height', ttlheight);
    $('.messages').css('max-height', $(window).height() - 100);
    $('.toolbar-fixed').css('overflow', 'scroll');
    $('.panel-right').css('height', $(window).height());
    //$('#message').focus();
    //get offer details
    myApp.showIndicator();
    var off_cat = Parse.Object.extend("offers");
    var off = new Parse.Query(off_cat);
    off.equalTo("objectId", getUrlVars()["id"]);
    off.find({
        success: function (results) {

            Date.prototype.toLocaleFormat = Date.prototype.toLocaleFormat || function (pattern) {
                var year = this.getFullYear(), month = this.getMonth() + 1, day = this.getDate();
                if (month < 10)
                    month = '0' + month;
                if (day < 10)
                    day = '0' + day;
                return pattern.replace(/%Y/g, year).replace(/%m/g, month).replace(/%d/g, day);
            };
            for (var i = 0; i < results.length; i++) {
                //alert(i);
                var object = results[i];
                //console.log(object);

                localStorage.setItem('checkdate', object.get('date'));
                localStorage.setItem('start_time', object.get('start_time'));
                localStorage.setItem('end_time', object.get('end_time'));

                if (object.get('created_by') === Parse.User.current().id) {
                    owner = 'me';
                    //alert(49);
                    $('#signin_btn').show();
                    $('#recepients').show();
                    $('#approve').hide();
                    $('.c5').show();
                    //update offer as viewed by creator
                    myApp.showIndicator();
                    var GameScore = Parse.Object.extend("offers");
                    var query = new GameScore();

                    //config.Msg(savedate);
                    //return false;
                    query.save({
                        objectId: getUrlVars()["id"],
                        creator_viewed: 1

                    },
                            {
                                success: function (object) {
                                    // alert(71);
                                    myApp.hideIndicator();
                                },
                                error: function (model, error)
                                {
                                    //$(".error").show();
                                    // config.Msg('error');
                                    config.Msg("That didn’t really go to plan. We’re not sure why. Please reload CarePool.", 'Hmm, something’s wrong', 'OK');
                                    myApp.hideIndicator();
                                }
                            });


                } else {

                    viewed_by_other_party();
                    localStorage.setItem('offer_id', getUrlVars()["id"]);
                    $('#signin_btn').hide();
                    $('#recepients').hide();
                    $('#approve').show();
//                     /$('.c5').show();
                }

                var today = new Date(object.get('date'));
                var mydate = today.toLocaleFormat('%m-%d-%Y');
                //config.Msg(mydate);

                // config.Msg(monthNames[mydate[0]]);
                mydate = mydate.split('-');
                //config.Msg(mydate[0]);
                //config.Msg(monthNames[parseInt(mydate[0])-1]);
                mydate = mydate[1] + ' ' + monthNames[parseInt(mydate[0]) - 1] + ' ' + mydate[2];
                event_date = mydate;
                event_name = object.get('title');
                $('#date').html(mydate);
                var time1 = object.get('start_time').split(':');
                if (time1[0] > 12) {
                    var time11 = time1[0] - 12;
                    if (time11.toString().length === 1) {
                        time11 = '0' + time11;
                    }
                    time11 = time11 + ':' + time1[1] + ' PM';
                } else {
                    time11 = object.get('start_time') + ' AM';
                }

                var time2 = object.get('end_time').split(':');
                if (time2[0] >= 12) {
                    if (time2[0] > 12) {
                        var time12 = time2[0] - 12;
                    } else {
                        var time12 = time2[0];
                    }
                    if (time12.toString().length === 1) {
                        time12 = '0' + time12;
                    }
                    time12 = time12 + ':' + time2[1] + ' PM';
                } else {
                    time12 = object.get('end_time') + ' AM';
                }
                $('#time').html(', ' + time11 + '-' + time12);
                event_time = 'From: ' + time11 + ' To: ' + time12;
                $('#location').html(object.get('location'));
                $('#note').html(object.get('message'));
                $("#title").html(object.get('title'));
                func_getcategory(object.get('offer_Category_id'));
            }

            myApp.hideIndicator();
        },
        error: function (error) {
            config.Msg("That didn’t really go to plan. We’re not sure why. Please reload CarePool.", 'Hmm, something’s wrong', 'OK');
            myApp.hideIndicator();
        }
    });
    //get other offer recepients/providers to display if i am requester/offerer and display on the page

    myApp.showIndicator();
    var from_me = new Parse.Query("user_offers");
    from_me.equalTo("care_provider", Parse.User.current().get('username'));
    var to_me = new Parse.Query("user_offers");
    to_me.equalTo("care_receiver", Parse.User.current().get('username'));
    //upcomg
    var mainQuery = Parse.Query.or(from_me, to_me);
    mainQuery.equalTo("offer_id", getUrlVars()["id"]);
    mainQuery.find({
        success: function (results) {

            for (var i = 0; i < results.length; i++) {

                var object = results[i];

                user_offers_ids.push(object.id);
                localStorage.setItem('primary_key', object.id);
                localStorage.setItem('careprovider_email', object.get('care_provider'));
                //localStorage.setItem('checkdate', object.get('event_date'));
                viewed_by_other_party();
                var color = '';
                if (object.get('status') === 'Accepted') {
                    color = '#fcaf1b;';
                } else if (object.get('status') === 'Declined') {
                    color = '#02CAE4;';
                } else {
                    color = '#666;';
                }
                var name = '';
                if (object.get('care_provider') == Parse.User.current().get('username')) {

                    //i created this
                    $('#auth_user').html(object.get('care_provider_name'));
                    if ($('#rec_user').html() == '') {
                        $('#rec_user').html(object.get('care_receiver_name'));
                    } else {
                        if ($('#rec_user').html() !== object.get('care_receiver_name')) {
                            $('#rec_user').append(', ' + object.get('care_receiver_name'));
                        }

                    }
                    var name = object.get('care_receiver_name');
                    if (object.get('status') !== 'Pending') {
                        //alert(148);
                        $('#c3').hide();
                        if ($('#signin_btn').is(":visible") === false) {
                            // alert(192);
                            $('.c5').show();
                        }
                        // alert(type);
                        //You're booked in to help

                        setTimeout(function () {
                            if (object.get('status') === 'Declined') {
                                $('#approve_status').html('<br/><font class="default_font" style="color:#737375;">You have ' + object.get('status') + ' this ' + type + '.</font><br/><br/>');

                            } else {
                                if (type === 'Request') {
                                    $('#approve_status').html('<br/><font class="default_font" style="color:#737375;">You\'re booked in to help.</font><br/><br/>');
                                } else {
                                    $('#approve_status').html('<br/><font class="default_font" style="color:#737375;">You have ' + object.get('status') + ' this ' + type + '.</font><br/><br/>');
                                }
                            }
                        }, 1200);
                    }
                    $('#approve_select').val(object.get('status'));
                    //alert(object.get('care_receiver_name'));
                    var name = object.get('care_receiver_name');
                    rec.push(object.get('care_receiver'));
                    rec_names.push(object.get('care_receiver_name'));
                    rec_status.push(object.get('status'));
//                    alert(object.get('care_receiver'));
//                    alert(object.get('status'));
                } else {


                    // others created this
                    if ($('#auth_user').html() == '') {
                        $('#auth_user').html(object.get('care_provider_name'));
                    } else {
                        if ($('#auth_user').html() !== object.get('care_provider_name')) {
                            $('#auth_user').append(', ' + object.get('care_provider_name'));
                        }
                    }
                    $('#rec_user').html(object.get('care_receiver_name'));
                    //var name=object.get('care_provider_name');
                    if (object.get('status') !== 'Pending') {
                        //alert(170);

                        if ($('#signin_btn').is(":visible") === false) {
                            //alert(object.get('status'));
                            if (object.get('status') !== 'Declined') {
                                $('.c5').show();
                            }

                        }

                        $('#c3').hide();
                        setTimeout(function () {
                            if (object.get('status') === 'Declined') {
                                $('#approve_status').html('<br/><font class="default_font" style="color:#737375;">You have ' + object.get('status') + ' this ' + type + '.</font><br/><br/>');

                            } else {
                                if (type === 'Request') {
                                    $('#approve_status').html('<br/><font class="default_font" style="color:#737375;">You\'re booked in to help.</font><br/><br/>');
                                } else {
                                    $('#approve_status').html('<br/><font class="default_font" style="color:#737375;">You have ' + object.get('status') + ' this ' + type + '.</font><br/><br/>');
                                }
                            }
                        }, 1200);
                    }
                    $('#approve_select').val(object.get('status'));
                    var name = object.get('care_provider_name');
                    rec.push(object.get('care_provider'));
                    rec_names.push(object.get('care_provider_name'));
                    rec_status.push(object.get('status'));

                }
                //if (object.get('status') !== 'Pending') {


                    var check_actual = new Date(localStorage.getItem('checkdate'));
                    //check_actual.setDate(check_actual.getDate() + 1);
                    check_actual.setDate(check_actual.getDate());
                    var check_date = check_actual.toJSON().slice(0, 10);
                    //alert(check_date);
                    check_date=check_date.split('-');
                    check_date=check_date[1]+'/'+check_date[2]+'/'+check_date[0];
                   // alert(check_date);
                    var strt_time = localStorage.getItem('start_time');
                    var end_time = localStorage.getItem('end_time');
                    //salert(end_time);
                    
                    var utc = new Date().toJSON().slice(0, 10);
                   // alert(utc);
                    utc=utc.split('-');
                    utc=utc[1]+'/'+utc[2]+'/'+utc[0];
//                    alert(utc);
                    
                    var newd = new Date().toString().split(" ");
                    newd = newd[4];

                    var newdd = newd.toString().split(":");
                    var newtime = newdd[0] + ':' + newdd[1];

                    var sname = rec;
                    //alert(sname);
                    var timecheck = "";
//alert(check_date + " " + end_time);
//                    if (new Date(check_date + " " + end_time) > new Date(utc + " " + newtime)) {
                        //if (object.get('status') !== 'Declined') {
                            myApp.showIndicator();
                            var val = '&#34;' + Parse.User.current().get('username') + '&#34';
                            var from_me = new Parse.Query("event_messages");
                            from_me.exists("receiver", val);

                            var to_me = new Parse.Query("event_messages");
                            to_me.equalTo("sender", val);
                            //upcomg
                            var mainQuery = Parse.Query.or(from_me, to_me);
                            mainQuery.equalTo("offer_id", getUrlVars()["id"]);
                            mainQuery.ascending("createdAt");
                            mainQuery.find({
                                success: function (results) {
                                    var careprovider = $('#careprovider').val();
                                    if (results.length > 0) $('#chaticon').html('<a data-panel="right" class="button open-panel messagebar link" onclick="getemail(&#34;' + careprovider + '&#34;)" style="width: 52px;border:0;height: 40px;margin-top: 20px;"><img src="img/bubble.png" /></a>');
                                    else {
                                        $('#chaticon').html('<a data-panel="right" class="button open-panel messagebar link" onclick="getemail(&#34;' + careprovider + '&#34;)" style="width: 52px;border:0;height: 40px;margin-top: 20px;"><img src="img/empty-bubble.png" /></a>');
                                    }
                                },
                                error: function (model, error)
                                {
                                    var careprovider = $('#careprovider').val();
                                    $('#chaticon').html('<a data-panel="right" class="button open-panel messagebar link" onclick="getemail(&#34;' + careprovider + '&#34;)" style="width: 52px;border:0;height: 40px;margin-top: 20px;"><img src="img/empty-bubble.png" /></a>');
                                    config.Msg("That didn’t really go to plan. We’re not sure why. Please reload CarePool.", 'Hmm, something’s wrong', 'OK');
                                    myApp.hideIndicator();
                                }
                             });
                             myApp.hideIndicator();
                        //}
                    //}
//
//                    if (check_date >= utc) {
//
//                        if (newtime < end_time) {
//
//
//                        }
//                    }

                //}
                //alert(rec_names.toString());

                var check_actual = new Date(localStorage.getItem('checkdate'));
                check_actual.setDate(check_actual.getDate() + 1);
                var check_date = check_actual.toJSON().slice(0, 10);
                var strt_time = localStorage.getItem('start_time');
                var end_time = localStorage.getItem('end_time');
                //salert(end_time);

                var utc = new Date().toJSON().slice(0, 10);

                var newd = new Date().toString().split(" ");
                newd = newd[4];

                var newdd = newd.toString().split(":");
                var newtime = newdd[0] + ':' + newdd[1];
                if (owner === '') {
                    var remail = object.get('care_receiver');
                } else {
                    var remail = object.get('care_provider');
                }
                //alert(rec);
                var showchat = '';
//                if (check_date >= utc) {
//
//                    if (newtime < end_time) {
//                        showchat = '<a data-panel="right" class="button open-panel messagebar link" onclick="getemail(&#34;' + remail + '&#34;)"style="width:40px;border:0;margin:-30px 0 0 -45px !important;"><i class="fa fa-2x fa-comments-o" style="color:#737375;"></i></a>';
//
//                    }
//                }

                $('#recepients').append('<li><div class="row"><div class="col-60"> <font class="default_font" style="color:#737375;">' + name + ' </font> \n\
</div><div class="col-30 default_font" style="color:' + color + '"> ' + object.get('status') + ' ' + showchat + ' </div></div></li>');



            }

            myApp.hideIndicator();
        },
        error: function (error) {
            config.Msg("That didn’t really go to plan. We’re not sure why. Please reload CarePool.", 'Hmm, something’s wrong', 'OK');
            myApp.hideIndicator();
        }
    });
    //Below is the click events of the data

    //c1 and c2 appears for accpeting/rejecting offer
    $('#c1').on('click', function () {
        var cp = localStorage.getItem('careprovider_email');
        /*$('#confirmbox').show();
        $('#message_one').attr('placeholder', 'Tell other party that you had accepted.');
        $('#hmsg').val('Accepted');
        $('#careprovider').val(cp);
        return false;*/
        findmyid('Accepted');
        return true;
    });
    $('#c2').on('click', function () {
        /*var cp = localStorage.getItem('careprovider_email');
        $('#confirmbox').show();
        $('#message_one').attr('placeholder', 'Tell other party that you are declining.');
        $('#hmsg').val('Declined');
        $('#careprovider').val(cp);*/
        //return false;
        findmyid('Declined');
        return true;
    });
    $('.c5').on('click', function () {
        config.Confirm(
            "Are you sure you want to cancel this event?", // message
            cancelfromme, // callback
            'Double check', // title
            'No,Yes' // buttonName
            );
    });
});

// Recipient Line 269

function getemail(val) {
    //  $('#message').focus();
    if (val === 1) {
        var message = $('#message_one').val();
        var hmsg = $('#hmsg').val();
        var careprovider = $('#careprovider').val();

        if (message === '') {
            alert('Please fill message box');
            $('#message').focus();
        } else {
            //return false;
            var GameScore = Parse.Object.extend("event_messages");
            var query = new GameScore();

            query.save({
                offer_id: getUrlVars()["id"],
                message: message,
                sender: Parse.User.current().get('username'),
                receiver: careprovider
            },
                    {
                        success: function (object) {
                            findmyid(hmsg);

                        },
                        error: function (model, error)
                        {
                            config.Msg("That didn’t really go to plan. We’re not sure why. Please reload CarePool.", 'Hmm, something’s wrong', 'OK');
                            //myApp.hideIndicator();
                        }
                    });


        }
    }

    localStorage.setItem('remail', val);
    getmessages(val);

}

function getmessages(val) {
    //alert(val);
    myApp.showIndicator();
    $('.wow').show();
    var from_me = new Parse.Query("event_messages");
    from_me.exists("receiver", val);

    var to_me = new Parse.Query("event_messages");
    to_me.equalTo("sender", val);
    //upcomg
    var mainQuery = Parse.Query.or(from_me, to_me);
    mainQuery.equalTo("offer_id", getUrlVars()["id"]);
    mainQuery.ascending("createdAt");

    mainQuery.find({
        success: function (results) {


            $('.messages').html('');

            for (var i = 0; i < results.length; i++) {
                var object = results[i];


                var today = new Date(object.get('createdAt'));
                var mydate = today.toLocaleFormat('%m-%d-%Y');

                mydate = mydate.split('-');
//                //config.Msg(mydate[0]);
                //config.Msg(monthNames[parseInt(mydate[0])-1]);
                mydate = mydate[1] + ' ' + monthNames[parseInt(mydate[0]) - 1] + ', ';

                var time1 = today.toLocaleTimeString().split(':');

                //alert(time1);

                //alert(time1);
                if (time1[0] > 12) {
                    var time11 = time1[0] - 12;
                    if (time11.toString().length === 1) {
                        time11 = '0' + time11;
                    }
                    time11 = time11 + ':' + time1[1] + ' PM';
                } else {
                    time11 = time1[0] + ':' + time1[1] + ' AM';
                }

                var resultant = mydate + time11;
                if (object.get('sender') === Parse.User.current().get('username')) {
                    $('.messages').append('<div class="message message-sent"><div class="messages-date" style="display:block;">' + resultant + '</div>\n\
<div class="message-text">' + object.get('message') + '</div></div>');
                } else {
                    if (rec_names[rec.indexOf(object.get('sender'))]) {
                        $('.messages').append('<div class="message message-received"><div class="messages-date" style="display:block;">' + resultant + '<br/><br/>' + rec_names[rec.indexOf(object.get('sender'))] + '</div>\n\
<div class="message-text">' + object.get('message') + '</div></div>');
                    }


                }


                //break;
            }

            ttlheight = $(window).height() - 160;
            $('.messages-content').css('height', ttlheight);
            if(results.length){
              $('.messages').scrollTop($('.messages .message:last-child').position().top);  
            }
            

            $('.message').focus();
            //$('#message').focus();

            setTimeout(function () {
                getmessages(val);
            }, 10000);
            myApp.hideIndicator();
        },
        error: function (error) {
            myApp.hideIndicator();
            config.Msg("That didn’t really go to plan. We’re not sure why. Please reload CarePool.", 'Hmm, something’s wrong', 'OK');

        }
    });
}

$('.send-message').click(function () {
    var msg = $('#message').val();
    //ahsan

    //alert(rec);
    //return false;
    if (msg != '') {
        myApp.showIndicator();
        var GameScore = Parse.Object.extend("event_messages");
        var query = new GameScore();
        player_ids = [];
        query.save({
            offer_id: getUrlVars()["id"],
            message: msg,
            receiver: rec.toString(),
            sender: Parse.User.current().get('username')
        },
                {
                    success: function (object) {
                        var mycheck;
                        var count_my_length = rec.length;
                        for (mycheck = 0; mycheck < count_my_length; mycheck++) {

                            if (rec_status[mycheck] === 'Declined') {
                                var index = rec.indexOf(rec[mycheck]);
                                rec.splice(index, 1);

                            }
                        }
                        //alert(rec);
                        //send push start
                        var off_cat = Parse.Object.extend("User");
                        var off = new Parse.Query(off_cat);
                        off.containedIn("username", rec);
                        off.notEqualTo("player_id", null);
                        off.equalTo("push_notes", 'Yes');
                        off.find({
                            success: function (results) {
                                standarresult = results;

                                if (results.length) {
                                    for (var i = 0; i < results.length; i++) {
                                        var object = results[i];
                                        player_ids.push(object.get('player_id'));
                                        if (i === results.length - 1) {
                                            //alert(player_ids);
                                            if (player_ids.length > 0) {
                                                var titleforpush = '';
                                                var message = '';
                                                // alert(player_ids);
                                                titleforpush = 'Message Arrived';
                                                message = Parse.User.current().get('full_name') + ' has sent a new message - check it out on your Dashboard';

                                                $.ajax({
                                                    type: "post",
                                                    url: 'https://onesignal.com/api/v1/notifications',
                                                    data: {app_id: '8c1378a8-a783-400e-94da-e9505801a0c1',
                                                        include_player_ids: player_ids,
                                                        contents: {"en": message},
                                                        headings: {'en': titleforpush},
                                                        android_group: true,
                                                        android_group_message: {"en": "You have $[notif_count] new messages"},
                                                        ios_badgeType: 'Increase',
                                                        ios_badgeCount: 1},
                                                    crossDomain: true,
                                                    dataType: "json",
                                                    cache: false,
                                                    success: function (data) {
                                                        //alert(JSON.stringify(data));

                                                    },
                                                    error: function (error) {
                                                        //console.log();
                                                        //alert(JSON.stringify(data));
                                                        config.Msg("That didn’t really go to plan. We’re not sure why. Please reload CarePool.", 'Hmm, something’s wrong', 'OK');
                                                    }
                                                });
                                            } //if got player_id
                                        }

                                    }//end of looping
                                    //console.log(arr);
                                }
                                //if got player id of the requesting party
                                myApp.hideIndicator();
                            },
                            error: function (error) {
                                myApp.hideIndicator();
                                config.Msg("That didn’t really go to plan. We’re not sure why. Please reload CarePool.", 'Hmm, something’s wrong', 'OK');
                            }
                        });
                        //send push ends


                        getmessagerecord(getUrlVars()["id"], localStorage.getItem('remail'));

//                        ttlheight = $(window).height() - 200;
//                        $('.page-content').css('height', ttlheight);

                        $('#message').val('');
                        $('.message').focus();

                        if (owner === 'me') {

                            myApp.showIndicator();
                            var MLife = Parse.Object.extend("user_offers");
                            var lifeArray = [];
                            // create a few objects, with a random state 0 or 1.
                            for (var i = 0; i < user_offers_ids.length; i++) {
                                var newLife = new MLife();
                                newLife.set("objectId", user_offers_ids[i]);
                                newLife.set("viewed", null);
                                // alert(user_offers_ids[i]);
                                lifeArray.push(newLife);
                            }

                            // save all the newly created objects
                            Parse.Object.saveAll(lifeArray, {
                                success: function (objs) {
                                    //alert('success');
                                    myApp.hideIndicator();
                                },
                                error: function (error) {
                                    myApp.hideIndicator();
                                    // an error occurred...
                                    config.Msg("That didn’t really go to plan. We’re not sure why. Please reload CarePool.", 'Hmm, something’s wrong', 'OK');
                                    return false;
                                }
                            });

                        } else {
                            myApp.showIndicator();
                            var GameScore = Parse.Object.extend("offers");
                            var query = new GameScore();

                            //config.Msg(savedate);
                            //return false;
                            query.save({
                                objectId: getUrlVars()["id"],
                                creator_viewed: null

                            },
                                    {
                                        success: function (object) {
                                            // alert(71);
                                            myApp.hideIndicator();
                                        },
                                        error: function (model, error)
                                        {
                                            //$(".error").show();
                                            // config.Msg('error');
                                            config.Msg("That didn’t really go to plan. We’re not sure why. Please reload CarePool.", 'Hmm, something’s wrong', 'OK');
                                            myApp.hideIndicator();
                                        }
                                    });

                        }
                        //alert(localStorage.getItem('remail'));
                    },
                    error: function (model, error)
                    {
                        config.Msg("That didn’t really go to plan. We’re not sure why. Please reload CarePool.", 'Hmm, something’s wrong', 'OK');
                        //myApp.hideIndicator();
                    }
                });

    }

});

function getmessagerecord(offerid, receiver) {
    myApp.showIndicator();
    var query = new Parse.Query("event_messages");

    var from_me = new Parse.Query("event_messages");
    from_me.exists("receiver", receiver);

    var to_me = new Parse.Query("event_messages");
    to_me.equalTo("sender", receiver);
    //upcomg
    var mainQuery = Parse.Query.or(from_me, to_me);
    mainQuery.equalTo("offer_id", offerid);
    mainQuery.ascending("createdAt");

    mainQuery.find({
        success: function (results) {

            $('.messages').html('');

            for (var i = 0; i < results.length; i++) {
                var object = results[i];


                var today = new Date(object.get('createdAt'));
                var mydate = today.toLocaleFormat('%m-%d-%Y');

                mydate = mydate.split('-');
//                //config.Msg(mydate[0]);
                //config.Msg(monthNames[parseInt(mydate[0])-1]);
                mydate = mydate[1] + ' ' + monthNames[parseInt(mydate[0]) - 1] + ', ';

                var time1 = today.toLocaleTimeString().split(':');

                //alert(time1);

                //alert(time1);
                if (time1[0] > 12) {
                    var time11 = time1[0] - 12;
                    if (time11.toString().length === 1) {
                        time11 = '0' + time11;
                    }
                    time11 = time11 + ':' + time1[1] + ' PM';
                } else {
                    time11 = time1[0] + ':' + time1[1] + ' AM';
                }

                var resultant = mydate + time11;
                if (object.get('sender') === Parse.User.current().get('username')) {
                    $('.messages').append('<div class="message message-sent"><div class="messages-date" style="display:block;">' + resultant + '</div>\n\
<div class="message-text">' + object.get('message') + '</div></div>');
                } else {
                    if (rec_names[rec.indexOf(object.get('sender'))]) {
                        $('.messages').append('<div class="message message-received"><div class="messages-date" style="display:block;">' + resultant + '<br/><br/>' + rec_names[rec.indexOf(object.get('sender'))] + '</div>\n\
<div class="message-text">' + object.get('message') + '</div></div>');
                    }

                }

                //break;
            }
            myApp.hideIndicator();
        },
        error: function (error) {
            myApp.hideIndicator();
            config.Msg("That didn’t really go to plan. We’re not sure why. Please reload CarePool.", 'Hmm, something’s wrong', 'OK');

        }
    });
}

// Recipient Line 269

var viewed_count = 0;
function viewed_by_other_party() {
    viewed_count++;
    if (viewed_count === 2) {
        //alert(localStorage.getItem('offer_id'));
        //alert(localStorage.getItem('primary_key'));
        if (localStorage.getItem('offer_id')) {
            //alert('make updation');

            myApp.showIndicator();
            var GameScore = Parse.Object.extend("user_offers");
            var query = new GameScore();

            //config.Msg(savedate);
            //return false;
            query.save({
                objectId: localStorage.getItem('primary_key'),
                viewed: 1
            },
                    {
                        success: function (object) {
//                            localStorage.removeItem('offer_id');
//                            localStorage.removeItem('primary_key');
                            myApp.hideIndicator();
                        },
                        error: function (model, error)
                        {
                            //$(".error").show();
                            // config.Msg('error');
                            config.Msg("That didn’t really go to plan. We’re not sure why. Please reload CarePool.", 'Hmm, something’s wrong', 'OK');
                            myApp.hideIndicator();
                        }
                    });
        }
    }
}

function cancelfromme(answer) {
    switch (answer) {
        case 1:
            //alert('no');

            break;
        case 2:
            var message = 'So sorry, I have to cancel this event.';
            var careprovider = $('#careprovider').val();
            var GameScore = Parse.Object.extend("event_messages");
            var query = new GameScore();
            query.save({
                offer_id: getUrlVars()["id"],
                message: message,
                sender: Parse.User.current().get('username'),
                receiver: careprovider
            },
            {
                success: function (object) {
                    findmyid('Declined', 'rejected_after_acception');
                    alert("Click Message Icon!");
                },
                error: function (model, error)
                {
                    config.Msg("That didn’t really go to plan. We’re not sure why. Please reload CarePool.", 'Hmm, something’s wrong', 'OK');
                    //myApp.hideIndicator();
                }
            });
            break;
    }
    return false;
}

//this function is called to bring offer id against which my decision will be saveds
function findmyid(valuee, rejected_after_acception) {
    myApp.showIndicator();
    var from_me = new Parse.Query("user_offers");
    from_me.equalTo("care_provider", Parse.User.current().get('username'));
    var to_me = new Parse.Query("user_offers");
    to_me.equalTo("care_receiver", Parse.User.current().get('username'));
    //upcomg
    var mainQuery = Parse.Query.or(from_me, to_me);
    mainQuery.equalTo("offer_id", getUrlVars()["id"]);
    mainQuery.find({
        success: function (results) {

            for (var i = 0; i < results.length; i++) {
                var object = results[i];
                update_me(object.id, valuee, rejected_after_acception);
            }

            myApp.hideIndicator();
        },
        error: function (error) {
            config.Msg("That didn’t really go to plan. We’re not sure why. Please reload CarePool.", 'Hmm, something’s wrong', 'OK');
            myApp.hideIndicator();
        }
    });
}

function send_accept_email() {
//alert(type);alert(event_date);return false;
//email send to the other party about my decision
    //alert(event_name);return false;
    var url = '';
    if (type === 'Offer') {
        url = 'http://www.carepool.co/carepool_emails/offer_accept.php';
    } else {
        url = 'http://www.carepool.co/carepool_emails/request_accept.php';
    }
    myApp.showIndicator();
    var new_rec = []; // requester/offerer email address array
    var off_cat = Parse.Object.extend("User");
    var off = new Parse.Query(off_cat);
    off.containedIn("username", rec);
    off.find({
        success: function (results) {

//            standarresult = results;
            if (results.length) {

                for (var i = 0; i < results.length; i++) {
                    var object = results[i];

                    if (object.get('email') !== undefined && object.get('email') !== '') {
                        new_rec.push(object.get('email'));

                    }
                    if (i === results.length - 1) {
                        // send_email(new_rec,rec_names);
                        //code here to sendemail
                        $.ajax({
                            type: "post",
                            url: url,
                            data: {name: Parse.User.current().get('full_name'), email: new_rec.toString(), date: event_date, event_name: event_name, event_time: event_time},
                            crossDomain: true,
                            dataType: "json",
                            cache: false,
                            success: function (data) {
                                redirecmetodashboard();
                            },
                            error: function (error) {
                                //console.log();
                                //alert(JSON.stringify(data));
                                config.Msg("An eror occured while email regarding decision", 'Hmm, something’s wrong', 'OK');
                            }
                        });
                    }
                }
            } else {
                alert('no1found');
                myApp.hideIndicator();
            }
            //if got player id of the requesting party
            myApp.hideIndicator();
        },
        error: function (error) {
            myApp.hideIndicator();
            config.Msg("That didn’t really go to plan. We’re not sure why. Please reload CarePool.", 'Hmm, something’s wrong', 'OK');
        }
    });





}

var redirect_count = 0;
function redirecmetodashboard() {
    redirect_count++;
    //  alert(redirect_count);
    if (redirect_count === 2) {
        window.location = 'dashboard.html';
    }

}
//update the user_offer record and save  my decisions
function update_me(id, status, rejected_after_acception) {
    //alert(event_time);return false;
    myApp.showIndicator();
    var GameScore = Parse.Object.extend("user_offers");
    var query = new GameScore();
    //config.Msg(savedate);
    //return false;
    query.save({
        objectId: id,
        status: status
    },
            {
                success: function (object) {
                    //send email if accpeted
                    if (status !== 'Declined') {

                        myApp.showIndicator();
                        var off_cat = Parse.Object.extend("subscription");
                        var off = new Parse.Query(off_cat);
                        off.equalTo("user_id", rec.toString());
                        off.count({
                            success: function (results) {

                                if (results === 0) {

                                    send_accept_email();
                                } else {
                                    redirecmetodashboard();
                                }

                                myApp.hideIndicator();
                            },
                            error: function (error) {
                                config.Msg("That didn’t really go to plan. We’re not sure why. Please reload CarePool.", 'Hmm, something’s wrong', 'OK');
                                myApp.hideIndicator();
                            }
                        });
                    } /*else {
                        redirecmetodashboard();
                    }*/


                    myApp.showIndicator();
                    //code for sending push notification to the requesting party
                    var off_cat = Parse.Object.extend("User");
                    var off = new Parse.Query(off_cat);
                    off.containedIn("username", rec);
                    off.notEqualTo("player_id", null);
                    off.equalTo("push_notes", 'Yes');
                    off.find({
                        success: function (results) {
                            standarresult = results;
                            if (results.length) {
                                for (var i = 0; i < results.length; i++) {
                                    var object = results[i];
                                    player_ids.push(object.get('player_id'));
                                    if (i === results.length - 1) {
                                        //alert(player_ids);
                                        if (player_ids.length > 0) {
                                            var titleforpush = '';
                                            var message = '';

                                            if (rejected_after_acception) {
                                                titleforpush = 'Sorry…!';
                                                message = Parse.User.current().get('full_name') + ' has had to cancel an event. These things happen sometimes.';

                                            } else {
                                                if (type === 'Offer') {
                                                    if (status === 'Accepted') {
                                                        titleforpush = 'Offer accepted!';
                                                        message = Parse.User.current().get('full_name') + ' has accepted your offer! Please refresh your Dashboard for details.';
                                                    } else {
                                                        titleforpush = 'No thanks!';
                                                        message = Parse.User.current().get('full_name') + ' does not need help on this occasion – but thanks you for your kind offer. Ask again another time! Please refresh your Dashboard for details.';
                                                    }
                                                }
                                                if (type !== 'Offer') {
                                                    if (status === 'Accepted') {
                                                        titleforpush = 'Yay!';
                                                        message = Parse.User.current().get('full_name') + ' can help you out! Please refresh your Dashboard.';
                                                    } else {
                                                        titleforpush = 'Sorry…';
                                                        message = Parse.User.current().get('full_name') + " can’t help this time, but ask again another time. Please refresh your Dashboard for details.";
                                                    }
                                                }
                                            }



                                            $.ajax({
                                                type: "post",
                                                url: 'https://onesignal.com/api/v1/notifications',
                                                data: {app_id: '8c1378a8-a783-400e-94da-e9505801a0c1',
                                                    include_player_ids: player_ids,
                                                    contents: {"en": message},
                                                    headings: {'en': titleforpush},
                                                    android_group: true,
                                                    android_group_message: {"en": "You have $[notif_count] new messages"},
                                                    ios_badgeType: 'Increase',
                                                    ios_badgeCount: 1},
                                                crossDomain: true,
                                                dataType: "json",
                                                cache: false,
                                                success: function (data) {
                                                    //alert(JSON.stringify(data));

                                                    //code for updating offer status 
                                                    if (status === 'Accepted') {

                                                        var GameScore = Parse.Object.extend("offers");
                                                        var query = new GameScore();
                                                        // alert(time1);alert(time2);return false;
                                                        query.set("objectId", getUrlVars()["id"]);
                                                        query.set("status", status);
                                                        query.set("accepted_by", Parse.User.current().id);
                                                        query.set("accepted_date", new Date());
                                                        query.set("creator_viewed", null);
                                                        query.save(null,
                                                                {
                                                                    success: function (object) {

                                                                        //send push to other members that were invloved with me in this event
                                                                        if (type === 'Request') {

                                                                            var others_push_receivers = [];
                                                                            myApp.showIndicator();
                                                                            var mainQuery = new Parse.Query("user_offers");
                                                                            mainQuery.equalTo("offer_id", getUrlVars()["id"]);
                                                                            mainQuery.notEqualTo("care_provider", Parse.User.current().get('username'));
                                                                            mainQuery.notEqualTo("status", 'Declined');
                                                                            mainQuery.find({
                                                                                success: function (results) {
                                                                                    if (results.length) {
                                                                                        for (var i = 0; i < results.length; i++) {
                                                                                            var object = results[i];
                                                                                            others_push_receivers.push(object.get('care_provider'));
                                                                                            //alert(others_push_receivers);
                                                                                            if (i === results.length - 1) {
                                                                                                //alert(others_push_receivers);

                                                                                                var off_cat = Parse.Object.extend("User");
                                                                                                var off = new Parse.Query(off_cat);
                                                                                                off.containedIn("username", others_push_receivers);
                                                                                                off.notEqualTo("player_id", null);
                                                                                                off.equalTo("push_notes", 'Yes');
                                                                                                off.find({
                                                                                                    success: function (results) {

                                                                                                        standarresult = results;
                                                                                                        if (results.length) {
                                                                                                            for (var i = 0; i < results.length; i++) {
                                                                                                                var object = results[i];
                                                                                                                myplayer_ids.push(object.get('player_id'));
                                                                                                                if (i === results.length - 1) {
                                                                                                                    //alert(player_ids);
                                                                                                                    if (myplayer_ids.length > 0) {
                                                                                                                        //alert(player_ids);

                                                                                                                        $.ajax({
                                                                                                                            type: "post",
                                                                                                                            url: 'https://onesignal.com/api/v1/notifications',
                                                                                                                            data: {app_id: '8c1378a8-a783-400e-94da-e9505801a0c1',
                                                                                                                                include_player_ids: myplayer_ids,
                                                                                                                                //include_player_ids: ['07e84db6-2f92-4bc3-ba31-9a20c66ad0e8', '943cdab1-364e-47a8-b28b-fcf614e8bad4'],
                                                                                                                                contents: {"en": rec_names.toString() + "'s request for help has been accepted by someone. No need to worry this time :)"},
                                                                                                                                headings: {'en': 'Sorted!'},
                                                                                                                                android_group: true,
                                                                                                                                android_group_message: {"en": "You have $[notif_count] new messages"},
                                                                                                                                ios_badgeType: 'Increase',
                                                                                                                                ios_badgeCount: 1},
                                                                                                                            crossDomain: true,
                                                                                                                            dataType: "json",
                                                                                                                            cache: false,
                                                                                                                            success: function (data) {
                                                                                                                                //alert(JSON.stringify(data));
                                                                                                                                myApp.hideIndicator();
                                                                                                                                //window.location = 'dashboard.html';
                                                                                                                                redirecmetodashboard();
                                                                                                                            },
                                                                                                                            error: function (error) {
                                                                                                                                //console.log();
                                                                                                                                //alert(JSON.stringify(data));
                                                                                                                                config.Msg("That didn’t really go to plan. We’re not sure why. Please reload CarePool.", 'Hmm, something’s wrong', 'OK');
                                                                                                                            }
                                                                                                                        });
                                                                                                                    } else {
                                                                                                                        myApp.hideIndicator();
                                                                                                                        redirecmetodashboard();
                                                                                                                        //window.location = 'dashboard.html';
                                                                                                                    }
                                                                                                                }

                                                                                                            }
                                                                                                        } else {
                                                                                                            myApp.hideIndicator();
                                                                                                            redirecmetodashboard();
                                                                                                        }

                                                                                                        //console.log(arr);

                                                                                                        myApp.hideIndicator();
                                                                                                    },
                                                                                                    error: function (error) {
                                                                                                        myApp.hideIndicator();
                                                                                                        config.Msg("That didn’t really go to plan. We’re not sure why. Please reload CarePool.", 'Hmm, something’s wrong', 'OK');
                                                                                                    }
                                                                                                });
                                                                                            }
                                                                                        }
                                                                                    } else {
                                                                                        myApp.hideIndicator();
                                                                                        redirecmetodashboard();
                                                                                        //window.location = 'dashboard.html';
                                                                                    }
                                                                                    myApp.hideIndicator();
                                                                                },
                                                                                error: function (error) {
                                                                                    config.Msg("That didn’t really go to plan. We’re not sure why. Please reload CarePool.", 'Hmm, something’s wrong', 'OK');
                                                                                    myApp.hideIndicator();
                                                                                }
                                                                            });
                                                                        } else {
                                                                            myApp.hideIndicator();
                                                                            redirecmetodashboard();
                                                                            //window.location = 'dashboard.html';
                                                                        }
                                                                        //offer extra code moved to offer details js


                                                                    },
                                                                    error: function (model, error)
                                                                    {
                                                                        myApp.hideIndicator();
                                                                        //$(".error").show();
                                                                        //config.Msg('error');
                                                                        config.Msg("That didn’t really go to plan. We’re not sure why. Please reload CarePool.", 'Hmm, something’s wrong', 'OK');
                                                                    }
                                                                });
                                                    } else {

                                                        myApp.showIndicator();
                                                        //now again update offer table to status='' and apprvod='';
                                                        var GameScore = Parse.Object.extend("offers");
                                                        var query = new GameScore();
                                                        // alert(time1);alert(time2);return false;
                                                        query.set("objectId", getUrlVars()["id"]);
                                                        query.set("status", null);
                                                        query.set("accepted_by", null);
                                                        query.set("accepted_date", null);
                                                        query.set("creator_viewed", null);
                                                        query.save(null,
                                                                {
                                                                    success: function (object) {
//                                                                        alert(getUrlVars()["id"]);
//                                                                        alert(577);
//                                                                        myApp.hideIndicator();
//                                                                        // window.location = 'dashboard.html';
//                                                                        redirecmetodashboard();


                                                                        //do code here to bring other recepients back to the request -starts
                                                                        if (type === 'Request') {

                                                                            var others_push_receivers = [];
                                                                            myApp.showIndicator();
                                                                            var mainQuery = new Parse.Query("user_offers");
                                                                            mainQuery.equalTo("offer_id", getUrlVars()["id"]);
                                                                            mainQuery.notEqualTo("care_provider", Parse.User.current().get('username'));
                                                                            mainQuery.notEqualTo("status", 'Declined');
                                                                            mainQuery.find({
                                                                                success: function (results) {
                                                                                    if (results.length) {
                                                                                        for (var i = 0; i < results.length; i++) {
                                                                                            var object = results[i];
                                                                                            others_push_receivers.push(object.get('care_provider'));
                                                                                            //alert(others_push_receivers);
                                                                                            if (i === results.length - 1) {
                                                                                                //alert(others_push_receivers);

                                                                                                var off_cat = Parse.Object.extend("User");
                                                                                                var off = new Parse.Query(off_cat);
                                                                                                off.containedIn("username", others_push_receivers);
                                                                                                off.notEqualTo("player_id", null);
                                                                                                off.equalTo("push_notes", 'Yes');
                                                                                                off.find({
                                                                                                    success: function (results) {

                                                                                                        standarresult = results;
                                                                                                        if (results.length) {
                                                                                                            for (var i = 0; i < results.length; i++) {
                                                                                                                var object = results[i];
                                                                                                                myplayer_ids.push(object.get('player_id'));
                                                                                                                if (i === results.length - 1) {
                                                                                                                    //alert(player_ids);
                                                                                                                    if (myplayer_ids.length > 0) {
                                                                                                                        //alert(player_ids);

                                                                                                                        $.ajax({
                                                                                                                            type: "post",
                                                                                                                            url: 'https://onesignal.com/api/v1/notifications',
                                                                                                                            data: {app_id: '8c1378a8-a783-400e-94da-e9505801a0c1',
                                                                                                                                include_player_ids: myplayer_ids,
                                                                                                                                //include_player_ids: ['07e84db6-2f92-4bc3-ba31-9a20c66ad0e8', '943cdab1-364e-47a8-b28b-fcf614e8bad4'],
                                                                                                                                contents: {"en": rec_names.toString() + "'s request for help has been rejected by someone after acceptance, so please if you can help him?"},
                                                                                                                                headings: {'en': 'Is there any chance you can help?'},
                                                                                                                                android_group: true,
                                                                                                                                android_group_message: {"en": "You have $[notif_count] new messages"},
                                                                                                                                ios_badgeType: 'Increase',
                                                                                                                                ios_badgeCount: 1},
                                                                                                                            crossDomain: true,
                                                                                                                            dataType: "json",
                                                                                                                            cache: false,
                                                                                                                            success: function (data) {
                                                                                                                                //alert(JSON.stringify(data));
                                                                                                                                myApp.hideIndicator();
                                                                                                                                //window.location = 'dashboard.html';
                                                                                                                                redirecmetodashboard();
                                                                                                                            },
                                                                                                                            error: function (error) {
                                                                                                                                //console.log();
                                                                                                                                //alert(JSON.stringify(data));
                                                                                                                                config.Msg("That didn’t really go to plan. We’re not sure why. Please reload CarePool.", 'Hmm, something’s wrong', 'OK');
                                                                                                                            }
                                                                                                                        });
                                                                                                                    } else {
                                                                                                                        myApp.hideIndicator();
                                                                                                                        redirecmetodashboard();
                                                                                                                        //window.location = 'dashboard.html';
                                                                                                                    }
                                                                                                                }

                                                                                                            }
                                                                                                        } else {
                                                                                                            myApp.hideIndicator();
                                                                                                            redirecmetodashboard();
                                                                                                        }

                                                                                                        //console.log(arr);

                                                                                                        myApp.hideIndicator();
                                                                                                    },
                                                                                                    error: function (error) {
                                                                                                        myApp.hideIndicator();
                                                                                                        config.Msg("That didn’t really go to plan. We’re not sure why. Please reload CarePool.", 'Hmm, something’s wrong', 'OK');
                                                                                                    }
                                                                                                });
                                                                                            }
                                                                                        }
                                                                                    } else {
                                                                                        myApp.hideIndicator();
                                                                                        redirecmetodashboard();
                                                                                        //window.location = 'dashboard.html';
                                                                                    }
                                                                                    myApp.hideIndicator();
                                                                                },
                                                                                error: function (error) {
                                                                                    config.Msg("That didn’t really go to plan. We’re not sure why. Please reload CarePool.", 'Hmm, something’s wrong', 'OK');
                                                                                    myApp.hideIndicator();
                                                                                }
                                                                            });
                                                                        } else {
                                                                            myApp.hideIndicator();
                                                                            redirecmetodashboard();
                                                                            //window.location = 'dashboard.html';
                                                                        }
                                                                        //do code here to bring other recepients back to the request -ends

                                                                    },
                                                                    error: function (model, error)
                                                                    {
                                                                        myApp.hideIndicator();
                                                                        //$(".error").show();
                                                                        //config.Msg('error');
                                                                        config.Msg("That didn’t really go to plan. We’re not sure why. Please reload CarePool.", 'Hmm, something’s wrong', 'OK');
                                                                    }
                                                                });
                                                    }

                                                },
                                                error: function (error) {
                                                    //console.log();
                                                    //alert(JSON.stringify(data));
                                                    config.Msg("That didn’t really go to plan. We’re not sure why. Please reload CarePool.", 'Hmm, something’s wrong', 'OK');
                                                }
                                            });
                                        } //if got player_id

                                        else {
                                            //alert(499);
                                            myApp.hideIndicator();
                                            //window.location = 'dashboard.html';
                                            redirecmetodashboard();
                                        }
                                    }

                                }//end of looping
                                //console.log(arr);
                            } else {
                                myApp.hideIndicator();
                                // window.location = 'dashboard.html';
                                redirecmetodashboard();
                            }
                            //if got player id of the requesting party
                            myApp.hideIndicator();
                        },
                        error: function (error) {
                            myApp.hideIndicator();
                            config.Msg("That didn’t really go to plan. We’re not sure why. Please reload CarePool.", 'Hmm, something’s wrong', 'OK');
                        }
                    });
                },
                error: function (model, error)
                {
                    myApp.hideIndicator();
                    //$(".error").show();
                    //config.Msg('error');
                    config.Msg("That didn’t really go to plan. We’re not sure why. Please reload CarePool.", 'Hmm, something’s wrong', 'OK');
                }
            });
}


function func_getcategory(id) {
//get offers categories
    myApp.showIndicator();
    var off_cat = Parse.Object.extend("offer_Category");
    var off = new Parse.Query(off_cat);
    off.equalTo("objectId", id);
    off.find({
        success: function (results) {
            // 
            for (var i = 0; i < results.length; i++) {
                var object = results[i];
                //console.log(object);
                $("#offer_image")[0].src = object.get('simple_image').url();
//alert(object.get('category'));

                if (object.get('category') === 'help') {
                    type = 'Request';
                    //change labels of the button
                    $('#c1').html('Yes - I can help!');
                    $('#c2').html('Sorry - I can\'t this time');
                } else {

                    type = 'Offer';
                    $('#c1').html('Gratefully accept');
                    $('#c2').html('Sorry but not this time');
                }
                // alert(getUrlVars()["type"]);
                if (getUrlVars()["type"] == 'past') {
                    $('.c5').hide();
                    $('#signin_btn').hide();
                    $('#c3').hide();
                }

            }
            myApp.hideIndicator();
        },
        error: function (error) {
            config.Msg("That didn’t really go to plan. We’re not sure why. Please reload CarePool.", 'Hmm, something’s wrong', 'OK');
            myApp.hideIndicator();
        }
    });
}
function yessuredelete(answer) {
    switch (answer) {
        case 1:
            //alert('no');

            break;
        case 2:
            myApp.showIndicator();
            // var id = getUrlVars()["id"];
            //update all user offer against this offer

            var MLife = Parse.Object.extend("user_offers");
            var lifeArray = [];
            // create a few objects, with a random state 0 or 1.
            for (var i = 0; i < user_offers_ids.length; i++) {
                var newLife = new MLife();
                newLife.set("objectId", user_offers_ids[i]);
                newLife.set("viewed", null);
                // alert(user_offers_ids[i]);
                lifeArray.push(newLife);
            }

            // save all the newly created objects
            Parse.Object.saveAll(lifeArray, {
                success: function (objs) {
                    //alert('success');
                },
                error: function (error) {
                    // an error occurred...
                    config.Msg("That didn’t really go to plan. We’re not sure why. Please reload CarePool.", 'Hmm, something’s wrong', 'OK');
                    return false;
                }
            });

            //set delete flag in offer as well

//            alert(710);
//            return false;

            //delete flag in the offer
            var GameScore = Parse.Object.extend("offers");
            var query = new GameScore();
            // alert(time1);alert(time2);return false;
            query.set("objectId", getUrlVars()["id"]);
            query.set("deleted", 1);
            query.set("deleted_date", new Date());
            query.save(null,
                    {
                        success: function (object) {

                            //dont send email to those who already declined it...
                            var mycheck;
                            var count_my_length = rec.length;
                            for (mycheck = 0; mycheck < count_my_length; mycheck++) {

                                if (rec_status[mycheck] === 'Declined') {
                                    var index = rec.indexOf(rec[mycheck]);
                                    rec.splice(index, 1);

                                    var index2 = rec_names.indexOf(rec_names[mycheck]);
                                    rec_names.splice(index2, 1);
                                }
                            }
                            //alert(rec);

                            //email send to the other party
                            if (rec.length > 0) {
                                myApp.showIndicator();
                                var new_rec = []; // requester/offerer email address array
                                var rec_names = [];
                                var off_cat = Parse.Object.extend("User");
                                var off = new Parse.Query(off_cat);
                                off.containedIn("username", rec);
                                off.find({
                                    success: function (results) {

//            standarresult = results;
                                        if (results.length) {

                                            for (var i = 0; i < results.length; i++) {
                                                var object = results[i];

                                                if (object.get('email') !== undefined && object.get('email') !== '') {
                                                    new_rec.push(object.get('email'));
                                                    rec_names.push(object.get('full_name'));
                                                }
                                                if (i === results.length - 1) {
                                                    // send_email(new_rec,rec_names);
                                                    //code here to sendemail
                                                    //ahsan
                                                    $.ajax({
                                                        type: "post",
                                                        url: 'http://www.carepool.co/carepool_emails/event_cancel.php',
                                                        data: {name: Parse.User.current().get('full_name'), email_list: new_rec, date: event_date, event_name: event_name, name_list: rec_names},
                                                        crossDomain: true,
                                                        dataType: "json",
                                                        cache: false,
                                                        success: function (data) {

                                                        },
                                                        error: function (error) {
                                                            // alert(750);
                                                            //console.log();
                                                            //alert(JSON.stringify(data));
                                                            config.Msg("That didn’t really go to plan. We’re not sure why. Please reload CarePool.", 'Hmm, something’s wrong', 'OK');
                                                        }
                                                    });
                                                    //ali
                                                }
                                            }
                                        } else {

                                            myApp.hideIndicator();
                                        }
                                        //if got player id of the requesting party
                                        myApp.hideIndicator();
                                    },
                                    error: function (error) {
                                        myApp.hideIndicator();
                                        config.Msg("That didn’t really go to plan. We’re not sure why. Please reload CarePool.", 'Hmm, something’s wrong', 'OK');
                                    }
                                });

                                setTimeout(function () {
                                    var off_cat = Parse.Object.extend("User");
                                    var off = new Parse.Query(off_cat);
                                    off.containedIn("username", rec);
                                    off.notEqualTo("player_id", null);
                                    off.equalTo("push_notes", 'Yes');
                                    off.find({
                                        success: function (results) {
                                            //alert(results.length);
                                            standarresult = results;
                                            if (results.length > 0) {
                                                for (var i = 0; i < results.length; i++) {
                                                    var object = results[i];
                                                    player_ids.push(object.get('player_id'));
                                                    if (i === results.length - 1) {
                                                        //alert(player_ids);
                                                        if (player_ids.length > 0) {

                                                            $.ajax({
                                                                type: "post",
                                                                url: 'https://onesignal.com/api/v1/notifications',
                                                                data: {app_id: '8c1378a8-a783-400e-94da-e9505801a0c1',
                                                                    include_player_ids: player_ids,
                                                                    contents: {"en": Parse.User.current().get('full_name') + ' has had to cancel an event. These things happen sometimes.'},
                                                                    headings: {'en': 'Sorry…'},
                                                                    android_group: true,
                                                                    android_group_message: {"en": "You have $[notif_count] new messages"},
                                                                    ios_badgeType: 'Increase',
                                                                    ios_badgeCount: 1},
                                                                crossDomain: true,
                                                                dataType: "json",
                                                                cache: false,
                                                                success: function (data) {
                                                                    //alert(JSON.stringify(data));
                                                                    myApp.hideIndicator();
                                                                    window.location = 'dashboard.html';
                                                                },
                                                                error: function (error) {
                                                                    //alert(790);
                                                                    //console.log();
                                                                    //alert(JSON.stringify(data));
                                                                    config.Msg("That didn’t really go to plan. We’re not sure why. Please reload CarePool.", 'Hmm, something’s wrong', 'OK');
                                                                }
                                                            });
                                                        } else {
                                                            //alert(499);
                                                            myApp.hideIndicator();
                                                            window.location = 'dashboard.html';
                                                        }
                                                    }

                                                }
                                            } else {
                                                myApp.hideIndicator();
                                                window.location = 'dashboard.html';
                                            }

                                            //console.log(arr);

                                            myApp.hideIndicator();
                                        },
                                        error: function (error) {
                                            //alert(815);
                                            myApp.hideIndicator();
                                            config.Msg("That didn’t really go to plan. We’re not sure why. Please reload CarePool.", 'Hmm, something’s wrong', 'OK');
                                        }
                                    });
                                }, 2000);
                            } else {
                                myApp.hideIndicator();
                                window.location = 'dashboard.html';
                            }
                            // The object was deleted from the Parse Cloud.
                        },
                        error: function (model, error)
                        {
                            //alert(828);
                            myApp.hideIndicator();
                            //$(".error").show();
                            //config.Msg(error);
                            config.Msg("That didn’t really go to plan. We’re not sure why. Please reload CarePool.", 'Hmm, something’s wrong', 'OK');
                        }
                    });
//            var yourClass = Parse.Object.extend("offers");
//            var query = new Parse.Query(yourClass);
//            query.get(id, {
//                success: function (yourObj) {
//
//                    // The object was retrieved successfully.
//                    yourObj.destroy({
//                        success: function (yourObj) {
//
//                        },
//                        error: function (yourObj, error) {
//                            myApp.hideIndicator();
//                            //config.Msg('unable to cancel');
//                            config.Msg("That didn’t really go to plan. We’re not sure why. Please reload CarePool.", 'Hmm, something’s wrong', 'OK');
//                            // The delete failed.
//                            // error is a Parse.Error with an error code and message.
//                        }
//                    });
//
//                    //getoffers(yourClass);
//
//                    //
//                },
//                error: function (object, error) {
//                    // The object was not retrieved successfully.
//                    // error is a Parse.Error with an error code and description.
//                }
//            });
            break;
    }
}
function deleteoffer() {

        config.Confirm(
                "Are you sure you want to cancel this event?", // message
                yessuredelete, // callback
                'Double check', // title
                'No,Yes' // buttonName
                );
}
