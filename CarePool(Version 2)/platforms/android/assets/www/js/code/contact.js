var Life = Parse.Object.extend("user_friends");
$(function () {
    ttlheight = $(window).height() - 180;
    myApp.showIndicator();
    $('.page-content').css('height', ttlheight);


    var mylife = new Parse.Query(Life);
    mylife.equalTo("user_id", Parse.User.current().id);
    mylife.find({
        success: function (results) {
            if (results.length) {
                $('#contactlist').empty();
                for (var i = 0; i < results.length; i++) {
                    var object = results[i];
                    //myfriends.push(object.get('full_name'));

                    $('#contactlist').append('<li class="swipeout ' + object.get('full_name').slice(0, 1).toLowerCase() + '" id="' + object.id + '"><div class="item-content" style="text-align:left;"><div class="item-media">\n\
<img src="img/profile.png" width="30" height="30"></div><div class="item-inner"><div class="item-title">' + object.get('full_name') + '<br/>Phone Number: ' + object.get('email') + '</div></div>\n\
<div class="item-media"><a href="#" style="margin-right:10px;color:red;" onclick="deleteme(' + "'" + object.id + "'" + ')">X</a></div></div></li>');
                    //'+'"'+object.id+'"'+'
                }
            } else {
                $('#contactlist').empty();
                if (Math.floor(Math.random()*100) % 2 == 0)
                $('#contactlist').append('<li><div class="item-content" style="text-align:left;"><div class="">\n\
<div class="item-media"><div class=""><div class="default_font" style="color:black;">Add people to your CarePool to be able to send them offers and requests. Click on \'Add More\' below to get started.</div></div></div></div></li>');
                else
                $('#contactlist').append('<li><div class="item-content" style="text-align:left;"><div class="">\n\
                <div class="item-media"><div class=""><div class="default_font" style="color:black;">Easily invite your friends.</div></div></div></div></li>');
//                (NB: your device contacts must have an email address)
            }

            myApp.hideIndicator();

        },
        error: function (error) {
            myApp.hideIndicator();
            config.Msg("That didn’t really go to plan. We’re not sure why. Please reload CarePool.",'Hmm, something’s wrong','OK');
        }
    })


});
function sendletter(elem) {
    var container = $('div.page-content'),
            scrollTo = $('.' + elem+':first');
    container.animate({
        scrollTop: scrollTo.offset().top - container.offset().top + container.scrollTop()
    });
}
function deleteme(id) {

    config.Confirm(
            "Are you sure you want to delete this person from your CarePool? ", // message
            function (buttonIndex) {
                logoutconfrim(buttonIndex, id);
            }, // callback
            'Double check', // title
            'No,Yes' // buttonName
            );
}
function logoutconfrim(answer, id) {
    switch (answer) {
        case 1:
            //alert('no');

            break;
        case 2:
            //alert(id);
            var query = new Parse.Query(Life);
            query.get(id, {
                success: function (yourObj) {
                    // The object was retrieved successfully.
                    yourObj.destroy({});

                    $('#' + id).hide();
                },
                error: function (object, error) {
                    // The object was not retrieved successfully.
                    // error is a Parse.Error with an error code and description.
                }
            });
            break;
    }
}