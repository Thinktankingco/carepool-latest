var counter = 0;
$(function () {
    Parse.User.logOut();
    $('#signup').on('submit', function () {
        myApp.showIndicator();
//        var dimensions = {
//            gender: 'm',
//            source: 'web',
//            dayType: 'weekend'
//        };
//        Parse.Analytics.track('signup', dimensions);
//        alert('done');
//        return false;
        var user = new Parse.User();
        user.set("username", $('#email').val());
        user.set("password", $('#password').val());
        user.set("full_name", $('#full_name').val());
        user.set("player_id", localStorage.getItem('player_id'));
        user.set("push_notes", 'Yes');
        user.set("type", 'Client');

        user.signUp(null, {
            success: function (user) {

                $.ajax({
                    type: "post",
                    url: 'http://www.carepool.co/carepool_emails/welcome.php',
                    data: {email: $('#email').val(), name: $('#full_name').val()},
                    crossDomain: true,
                    dataType: "json",
                    cache: false,
                    success: function (data) {
                        // console.log();
                        if (data == 'sent') {

                            myApp.showIndicator();
                            //change name in user friends 
                            var Life = Parse.Object.extend("user_friends");
                            var query = new Parse.Query(Life);

                            //updaintg user friends table
                            query.equalTo("email", $('#email').val());
                            //query.equalTo("email", 'ahsan.dev.drc@gmail.com');
                            query.find({
                                success: function (results) {
                                    myApp.showIndicator();
                                    //console.log(results);
                                    if (results.length) {
                                        var lifeArray = [];
                                        for (var i = 0; i < results.length; i++) {
                                            var update_name = new Life();
                                            var object = results[i];
                                            update_name.set("objectId", object.id);
                                            update_name.set("full_name", $('#full_name').val());
                                            lifeArray.push(update_name);
                                        }
                                        Parse.Object.saveAll(lifeArray, {
                                            success: function (objs) {
                                                // objects have been saved...
                                                // config.Msg('success');
                                                //alert('changed all the names');
                                                //myApp.hideIndicator();
                                                sum();
                                            },
                                            error: function (error) {
                                                // an error occurred...

                                                config.Msg("Please type your number again without spaces.", 'Sans Spaces', 'OK');
                                                myApp.hideIndicator();
                                                return false;
                                            }
                                        });

                                    } else {
                                        sum();
                                    }
                                    // results is an array of AgentReleases
                                    myApp.hideIndicator();
                                },
                                error: function (error) {
                                    alert("Error: " + error.code + " " + error.message);
                                    myApp.hideIndicator();
                                }
                            });
                            //updating user_offers providers name first
                            myApp.showIndicator();
                            var Provider = Parse.Object.extend("user_offers");
                            var query = new Parse.Query(Provider);
                            query.equalTo("care_provider", $('#email').val());
                            query.find({
                                success: function (results) {
                                    myApp.showIndicator();
                                    //console.log(results);
                                    if (results.length) {
                                        var lifeArray = [];
                                        for (var i = 0; i < results.length; i++) {
                                            var update_name = new Provider();
                                            var object = results[i];
                                            update_name.set("objectId", object.id);
                                            update_name.set("care_provider_name", $('#full_name').val());
                                            lifeArray.push(update_name);
                                        }
                                        Parse.Object.saveAll(lifeArray, {
                                            success: function (objs) {
                                                // objects have been saved...
                                                // config.Msg('success');
                                                //alert('changed all the receivers');
                                                sum();
                                                //myApp.hideIndicator();
                                            },
                                            error: function (error) {
                                                // an error occurred...
                                                //config.Msg(error);
                                                config.Msg("That didn’t really go to plan. We’re not sure why. Please reload CarePool.", 'Hmm, something’s wrong', 'OK');
                                                myApp.hideIndicator();
                                                return false;
                                            }
                                        });
                                        //console.log(results);
                                    } else {
                                        sum();
                                    }
                                    // results is an array of AgentReleases
                                    myApp.hideIndicator();
                                },
                                error: function (error) {
                                    alert("Error: " + error.code + " " + error.message);
                                }
                            });
                            myApp.showIndicator();
                            //updating user_offers receivers name first
                            query.equalTo("care_receiver", $('#email').val());
                            query.find({
                                success: function (results) {
                                    //console.log(results);
                                    myApp.showIndicator();
                                    if (results.length) {
                                        var lifeArray = [];
                                        for (var i = 0; i < results.length; i++) {
                                            var update_name = new Provider();
                                            var object = results[i];
                                            update_name.set("objectId", object.id);
                                            update_name.set("care_receiver_name", $('#full_name').val());
                                            lifeArray.push(update_name);
                                        }
                                        Parse.Object.saveAll(lifeArray, {
                                            success: function (objs) {
                                                // objects have been saved...
                                                // config.Msg('success');
                                                //alert('changed all the providers');
                                                sum();
                                                //myApp.hideIndicator();
                                            },
                                            error: function (error) {
                                                // an error occurred...

                                                config.Msg("That didn’t really go to plan. We’re not sure why. Please reload CarePool.", 'Hmm, something’s wrong', 'OK');
                                                myApp.hideIndicator();
                                                return false;
                                            }
                                        });
                                        // console.log(results);
                                    } else {
                                        sum();
                                    }
                                    // results is an array of AgentReleases
                                    myApp.hideIndicator();
                                },
                                error: function (error) {
                                    alert("Error: " + error.code + " " + error.message);
                                    myApp.hideIndicator();
                                }
                            });

                        } else {
                            myApp.hideIndicator();

                            config.Msg("That didn’t really go to plan. We’re not sure why. Please reload CarePool.", 'Hmm, something’s wrong', 'OK');
                        }
                    },
                    error: function () {
                        //console.log();
                        config.Msg("That didn’t really go to plan. We’re not sure why. Please reload CarePool.", 'Hmm, something’s wrong', 'OK');
                    }

                });

            },
            error: function (user, error) {
                //alert('190-'+error.code);
                if (error.code !== 202) {
                    //alert(192);
                    // myApp.hideIndicator();
                    // Show the error message somewhere and let the user try again.
                    myApp.hideIndicator();
                    config.Msg("Please Fill all the required fields.", 'Hmm, something’s wrong', 'OK');
                } else {
                    //alert(194);
                    myApp.hideIndicator();
                    config.Msg("Ooops, that email address is already connected to a CarePool account. \n\
Recover your password on the Sign In screen or use another email address", 'CarePool', 'OK');
                }

            }
        });

        return false;
    });
    //api_num();
});

//function api_num() {
//    alert(1);
//    
//    var url = "http://api.clickatell.com/http/sendmsg?user=bilalkirmani&password=IGFEbcPfePSEYC&api_id=3620448&to=923028005003&text=Message";
//    
//    $.post(url, function(output){
//        alert(output);
//    });
//}

function sum() {
    ++counter;
    //alert(counter);
    if (counter === 3) {
        myApp.hideIndicator();
        window.location = 'dashboard.html';
    }
}