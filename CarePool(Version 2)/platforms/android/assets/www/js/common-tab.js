function getUrlVars() {
    var vars = {};
    var parts = window.location.href.replace(
            /[?&]+([^=&]+)=([^&]*)/gi, function (m, key, value) {
                vars[key] = value;
            });
    return vars;
}

function mymenu() {

    var url = window.location.pathname;
    var filename = url.substring(url.lastIndexOf('/') + 1);

	
	
	
    var home, offer, ask, profile, more, fontcolor1, fontcolor2, fontcolor3, fontcolor4, fontcolor5;
    if (window.location.href.split("/").pop() === 'dashboard.html'  || window.location.href.split("/").pop() === 'dashboard-Welcome.html') {
        home = 'style="background-color:#EC008C;color:#fff;width:20%;"';
        offer = 'style="width:20%;"';
        ask = 'style="width:20%;"';
        profile = 'style="width:20%;"';
        more = 'style="width:20%;"';
        fontcolor1 = 'style="color:#FFF;"';
        fontcolor2 = 'style="color:grey"';
        fontcolor3 = 'style="color:grey"';
        fontcolor4 = 'style="color:grey"';
        fontcolor5 = 'style="color:grey"';
    } else if (window.location.href.split("/").pop() === 'offer.html' || getUrlVars()["cat"]=='offer') {
        home = 'style="width:20%;"';
        offer = 'style="background-color:#EC008C;color:#FFF;width:20%;"';
        ask = 'style="width:20%;"';
        profile = 'style="width:20%;"';
        more = 'style="width:20%;"';
        fontcolor2 = 'style="color:#FFF;"';
        fontcolor1 = 'style="color:grey"';
        fontcolor3 = 'style="color:grey"';
        fontcolor4 = 'style="color:grey"';
        fontcolor5 = 'style="color:grey"';
    } else if (window.location.href.split("/").pop() === 'askforhelp.html' || getUrlVars()["cat"]=='ask') {
        home = 'style="width:20%;"';
        offer = 'style="width:20%;"';
        ask = 'style="background-color:#EC008C;color:#FFF;width:20%;"';
        profile = 'style="width:20%;"';
        more = 'style="width:20%;"';
        fontcolor3 = 'style="color:#FFF;"';
        fontcolor2 = 'style="color:grey"';
        fontcolor1 = 'style="color:grey"';
        fontcolor4 = 'style="color:grey"';
        fontcolor5 = 'style="color:grey"';
    } else if (window.location.href.split("/").pop() === 'profile.html') {
        home = 'style="width:20%;"';
        offer = 'style="width:20%;"';
        ask = 'style="width:20%;"';
        profile = 'style="background-color:#EC008C;color:#FFF;width:20%;"';
        more = 'style="width:20%;"';
        fontcolor4 = 'style="color:#FFF;"';
        fontcolor2 = 'style="color:grey"';
        fontcolor3 = 'style="color:grey"';
        fontcolor1 = 'style="color:grey"';
        fontcolor5 = 'style="color:grey"';
    } else if (window.location.href.split("/").pop() === 'more.html' || window.location.href.split("/").pop() === 'settings.html' || window.location.href.split("/").pop() === 'help.html' || window.location.href.split("/").pop() === 'help.html') {
        home = 'style="width:20%;"';
        offer = 'style="width:20%;"';
        ask = 'style="width:20%;"';
        profile = 'style="width:20%;"';
        more = 'style="background-color:#EC008C;color:#FFF;width:20%;"';
        fontcolor5 = 'style="color:#FFF;"';
        fontcolor2 = 'style="color:grey"';
        fontcolor3 = 'style="color:grey"';
        fontcolor4 = 'style="color:grey"';
        fontcolor1 = 'style="color:grey"';
    }


    document.write('<div class="toolbar tabbar tabbar-labels" style="background-color:#FFF;"><div class="toolbar-inner">\n\
<a href="dashboard.html" class="tab-link active external" ' + home + ' ><span class="tabbar-label default_font" '+fontcolor1+'>Dashboard</span>\n\
<img src="img/dash1.png" width="30" height="25"></a><a href="offer.html" class="tab-link external" ' + offer + '><span class="tabbar-label default_font" '+fontcolor2+'>Offer</span>\n\
<img src="img/offer.png" width="30" height="30"></a><a href="askforhelp.html" class="tab-link external" ' + ask + '><span class="tabbar-label default_font" '+fontcolor3+'>Ask</span>\n\
<img src="img/sun3.png" width="30" height="30"></a><a href="profile.html" class="tab-link external" ' + profile + '><span class="tabbar-label default_font" '+fontcolor4+'>Profile</span>\n\
<img src="img/profile.png" width="30" height="30"></a><a href="more.html" class="tab-link external" ' + more + '>\n\
<span class="tabbar-label default_font" '+fontcolor5+'>More</span><img src="img/more2.png" width="30" height="30"></a></div></div>');
}
