/*
       Licensed to the Apache Software Foundation (ASF) under one
       or more contributor license agreements.  See the NOTICE file
       distributed with this work for additional information
       regarding copyright ownership.  The ASF licenses this file
       to you under the Apache License, Version 2.0 (the
       "License"); you may not use this file except in compliance
       with the License.  You may obtain a copy of the License at

         http://www.apache.org/licenses/LICENSE-2.0

       Unless required by applicable law or agreed to in writing,
       software distributed under the License is distributed on an
       "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
       KIND, either express or implied.  See the License for the
       specific language governing permissions and limitations
       under the License.
 */

package com.carepool.carepool;

import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.JavascriptInterface;
import android.webkit.WebView;
import android.widget.ScrollView;
import android.widget.Toast;

import org.apache.cordova.CordovaActivity;

//import com.onesignal.OneSignal;

public class MainActivity extends CordovaActivity {
    WebView webView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //OneSignal.startInit(this).init();
        // Set by <content src="index.html" /> in config.xml
//        webView = new WebView(this);
//        webView.getSettings().setJavaScriptEnabled(true);
//        webView.loadUrl("file:///android_asset/script_page.html");

        loadUrl(launchUrl);
        webView = (WebView) appView.getEngine().getView();
        webView.addJavascriptInterface(this, "MainActivity");
    }

    @JavascriptInterface
    void customMethod() {
     //  customMethodf();

    }


//
//    @Override
//    public void onDestroy() {
//        super.onDestroy();
//        webView.loadUrl("javascript:execute()");
//        Log.e("keyword", "nazar aaye");
//    }
}
