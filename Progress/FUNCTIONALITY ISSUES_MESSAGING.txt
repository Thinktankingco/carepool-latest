1. FUNCTIONALITY_MESSAGING FOR EVENTS THAT MOVE TO PAST: Users cannot see or access the Messaging screen once an event has moved to past. When a user declines, they write a message, but the recipient cannot access it as the Message function is not available. Users should be able to access the messaging function once an event has moved to Past (whether complete, declined or cancelled). This is important to allow users to see the reason an event has been declined or cancelled, or to thank a user. Messaging should be available for 24 hours after the end time, or indefinitely if the time limit is a problem (see Workflow).
	I fix it!!!
	www/js/code/offer_detail.js;
	Remove Line 315, 344;if (new Date(check_date + " " + end_time) > new Date(utc + " " + newtime)) {

2. FUNCTIONALITY_MESSAGING: when CANCELLING AN EVENT - canceller should be able to GIVE A REASON by Message. When user clicks on 'Cancel this event', the FIRST MESSAGE that appears in messaging should be 'So sorry, I have to cancel this event', and the user should be taken to MESSAGE screen in order to give a reason. Recipient should be able to see this message. See WORKFLOW
	I fix it!!!!!!!!!

3. FUNCTIONALITY_MESSAGING: MULTI-RECIPIENT EVENTS - Currently, there is only one message bubble per event. The sender of an event can see all messages, but the recipients can only see the messages from the sender. This is confusing so please make all messaging GROUP MESSAGING (i.e. all parties to an event can see all messages).
	I fix it;
	www/js/code/offer_detail.js changed:
		Line 329 ~ 342

4. SUGGESTION RE MESSAGING: Change so that users can message in relation to an event that is pending. This will solve numerous problems, including in multirecipient request, if first responded accepts then cancels, the event moves back into Pending, so the sender cannot see why the person cancelled. No message is received.
	Maybe!

5. MESSAGES should be able to be accessed and sent for PENDING and PAST events as well. All messaging should be GROUP MESSAGING - all parties to the event will be included in messaging, even if they decline or cancel. This will resolve a number of issues.
	I fix it!!
	www/js/code/offer_detail.js Changed!
		Remove Line 315, 344